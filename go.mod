module framagit.org/comboros/combalou

go 1.12

require (
	github.com/DATA-DOG/go-txdb v0.1.3
	github.com/go-kit/kit v0.9.0
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/golang-migrate/migrate/v4 v4.7.0
	github.com/google/go-cmp v0.2.0
	github.com/gorilla/mux v1.7.3
	github.com/lib/pq v1.0.0
	github.com/pelletier/go-toml v1.9.0 // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	gopkg.in/dealancer/validate.v2 v2.1.0
)
