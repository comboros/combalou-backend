package helper

import (
	"testing"

	"framagit.org/comboros/combalou/data"
	"framagit.org/comboros/combalou/data/csv"
	"framagit.org/comboros/combalou/internal/projectpath"
	"framagit.org/comboros/combalou/ticket"
	"framagit.org/comboros/combalou/visitor"
	"framagit.org/comboros/combalou/workshop"
)

var (
	_tickets     []ticket.Ticket
	_workshops   []workshop.Workshop
	_initialized = false // true when _tickets and _workshops are populated
)

var (
	_pathTickets   = projectpath.Root + "/data/tickets.csv"
	_pathWorkshops = projectpath.Root + "/data/workshops.csv"
)

// Init loads test data from data files.
func Init(t *testing.T) {
	t.Helper()

	// Load tickets & workshops from csv files
	files, err := data.Load(_pathTickets, _pathWorkshops)
	if err != nil {
		t.Fatalf("Opening test data: %v", err)
	}
	_tickets, err = csv.LoadTickets(files.Tickets)
	if err != nil {
		t.Fatalf("Loading tickets: %v", err)
	}
	_workshops, err = csv.LoadWorkshops(files.Workshops)
	if err != nil {
		t.Fatalf("Loading workshops: %v", err)
	}

	// Give IDs
	for i := range _tickets {
		_tickets[i].ID = ticket.ID(i)
	}
	for i := range _workshops {
		_workshops[i].ID = workshop.ID(i)
	}

	_initialized = true
}

func Tickets(t *testing.T) []ticket.Ticket {
	t.Helper()
	if !_initialized {
		Init(t)
	}
	return _tickets
}

func TicketsByID(t *testing.T) map[ticket.ID]ticket.Ticket {
	t.Helper()
	result := make(map[ticket.ID]ticket.Ticket)
	for _, t := range Tickets(t) {
		result[t.ID] = t
	}
	return result
}

func Workshops(t *testing.T) []workshop.Workshop {
	t.Helper()
	if !_initialized {
		Init(t)
	}
	return _workshops
}

func WorkshopsByID(t *testing.T) map[workshop.ID]workshop.Workshop {
	t.Helper()
	result := make(map[workshop.ID]workshop.Workshop)
	for _, w := range Workshops(t) {
		result[w.ID] = w
	}
	return result
}

func Purchases(t *testing.T, args ...interface{}) map[ticket.ID]int {
	t.Helper()
	var (
		purchases = map[ticket.ID]int{}
		id        ticket.ID
	)
	for _, arg := range args {
		switch arg := arg.(type) {
		case ticket.ID:
			id = arg
		case int:
			purchases[id] = arg
		default:
			t.Fatalf("Invalid parameter %v", arg)
		}
	}
	return purchases
}

func Bookings(t *testing.T, args ...workshop.ID) map[workshop.ID]bool {
	t.Helper()
	workshops := WorkshopsByID(t)
	bookings := make(visitor.Bookings, len(args))
	for _, id := range args {
		if _, ok := workshops[id]; !ok {
			t.Fatalf("Unknown workshop id: %v", id)
		}
		bookings[id] = true
	}
	return bookings
}
