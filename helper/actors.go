package helper

import (
	"context"
	"testing"

	"framagit.org/comboros/combalou/ticket"
	"framagit.org/comboros/combalou/visitor"
)

// Actor represents a typical test subject that we reuse regularly.
type Actor struct {
	Visitor visitor.Visitor
	Basket  visitor.Basket
}

func (a *Actor) SaveVisitor(t *testing.T, visitors visitor.Repository) {
	if err := visitors.Save(context.Background(), &a.Visitor); err != nil {
		t.Fatalf("Saving %s’s visitor: %v", a.Visitor.Surname, err)
	}
	a.Basket.VisitorID = a.Visitor.ID
}

func (a *Actor) SaveBasket(t *testing.T, baskets visitor.BasketRepository) {
	if err := baskets.Save(context.Background(), &a.Basket); err != nil {
		t.Fatalf("Saving %s’s basket: %v", a.Visitor.Surname, err)
	}
}

// Alice is a visitor that decided to come the entire festival.
// She took the complete formula, and booked one workshop per half-day.
func Alice(t *testing.T) *Actor {
	return &Actor{
		Visitor: visitor.Visitor{
			Surname: "Alice",
			Name:    "Actor",
			Phone:   "418 123 4567",
			Email:   "alice@example.com",
			Address: visitor.Address{
				Line1:      "1234 av. Alice",
				City:       "Quebec",
				PostalCode: "G1J 000",
				Country:    "Canada",
			},
			Bookings: Bookings(t, 0, 5, 12, 16, 22, 29, 35),
		},
		Basket: visitor.Basket{
			Purchases: Purchases(t, ticket.ID(0), 1),
		},
	}
}

// Bob is a visitor that decided to come the entire festival, but does not want
// to spend all his time in workshops. He took the mixt formula, and booked for
// 5 workshops.
func Bob(t *testing.T) *Actor {
	return &Actor{
		Visitor: visitor.Visitor{
			Surname: "Bob",
			Name:    "Actor",
			Phone:   "418 987 5432",
			Email:   "bob@example.com",
			Address: visitor.Address{
				Line1:      "4321 av. Bob",
				Line2:      "Appt. 5",
				City:       "Montreal",
				PostalCode: "H3W 000",
				Country:    "Canada",
			},
			Bookings: Bookings(t, 3, 6, 14, 19, 35),
		},
		Basket: visitor.Basket{
			Purchases: Purchases(t, ticket.ID(1), 1, ticket.ID(5), 1),
		},
	}
}

// Martine decided to come a few days, but hasn’t booked anything yet.
func Martine(t *testing.T) *Actor {
	return &Actor{
		Visitor: visitor.Visitor{
			Surname: "Martine",
			Name:    "Actor",
			Phone:   "08 36 12 34 56",
			Email:   "martine@example.com",
			Address: visitor.Address{
				Line1:      "36 rue Martine",
				City:       "Villeneuve d’Ascq",
				PostalCode: "59600",
				Country:    "France",
			},
		},
		Basket: visitor.Basket{
			Purchases: Purchases(t, ticket.ID(2), 1),
		},
	}
}
