package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"framagit.org/comboros/combalou/workshop"
	"github.com/lib/pq"
)

type workshopRepository struct {
	db *sql.DB
}

func NewWorkshopRepository(db *sql.DB) workshop.Repository {
	return &workshopRepository{db}
}

func (r *workshopRepository) All(ctx context.Context) (_ map[workshop.ID]workshop.Workshop, err error) {
	const q = `
SELECT
	id,
	name,
	band,
	start,
	EXTRACT(EPOCH FROM duration),
	category,
	gauge
FROM
	workshops
`
	rows, err := r.db.QueryContext(ctx, q)
	if err != nil {
		return nil, fmt.Errorf("workshops: %v", err)
	}
	defer func() {
		if closeErr := rows.Close(); closeErr != nil {
			if err != nil {
				return // favor the original error
			}
			err = fmt.Errorf("workshops: %v", closeErr)
		}
	}()
	workshops := make(map[workshop.ID]workshop.Workshop)
	for rows.Next() {
		w, err := scanWorkshop(rows)
		if err != nil {
			return nil, fmt.Errorf("workshops: %v", err)
		}
		workshops[w.ID] = w
	}
	if err = rows.Err(); err != nil {
		return nil, fmt.Errorf("workshops: %v", err)
	}
	return workshops, nil
}

func (r *workshopRepository) AllByID(ctx context.Context, ids []workshop.ID) (_ map[workshop.ID]workshop.Workshop, err error) {
	const q = `
SELECT
	id,
	name,
	band,
	start,
	EXTRACT(EPOCH FROM duration),
	category,
	gauge
FROM
	workshops
WHERE
	id = ANY($1)
`
	rows, err := r.db.QueryContext(ctx, q, pq.Array(ids))
	if err != nil {
		return nil, fmt.Errorf("workshops by id: %v", err)
	}
	defer func() {
		if closeErr := rows.Close(); closeErr != nil {
			if err != nil {
				return // favor the original error
			}
			err = fmt.Errorf("workshops by id: %v", err)
		}
	}()
	workshops := make(map[workshop.ID]workshop.Workshop)
	for rows.Next() {
		w, err := scanWorkshop(rows)
		if err != nil {
			return nil, fmt.Errorf("workshops by id: %v", err)
		}
		workshops[w.ID] = w
	}
	if err = rows.Err(); err != nil {
		return nil, fmt.Errorf("workshops by id: %v", err)
	}
	return workshops, nil
}

func scanWorkshop(rows *sql.Rows) (workshop.Workshop, error) {
	var w workshop.Workshop
	var c string
	err := rows.Scan(&w.ID, &w.Name, &w.Band, &w.Start, &w.Duration, &c, &w.Gauge)
	if err != nil {
		return workshop.Workshop{}, err
	}
	w.Category, err = workshop.StringToCategory(c)
	if err != nil {
		return workshop.Workshop{}, err
	}
	return w, nil
}
