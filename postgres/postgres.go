package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"io"

	"framagit.org/comboros/combalou/data/csv"
	"framagit.org/comboros/combalou/internal/projectpath"
	"framagit.org/comboros/combalou/ticket"
	"github.com/golang-migrate/migrate/v4"
	pgmigrate "github.com/golang-migrate/migrate/v4/database/postgres"
)

const DSN = "postgres://comboros:comboros@localhost:5432/combalou?sslmode=disable"

type DB struct {
	*sql.DB
}

func (db DB) ReadOnly(ctx context.Context, f func(*sql.Tx) error) error {
	return db.withTx(ctx, &sql.TxOptions{ReadOnly: true}, f)
}

func (db DB) Update(ctx context.Context, f func(*sql.Tx) error) error {
	return db.withTx(ctx, nil, f)
}

func (db DB) withTx(ctx context.Context, opts *sql.TxOptions, f func(*sql.Tx) error) error {
	tx, err := db.BeginTx(ctx, opts)
	if err != nil {
		return err
	}

	if err = f(tx); err != nil {
		_ = tx.Rollback()
		return err
	}

	return tx.Commit()
}

// Start runs the PostgreSQL migrations to ensure the database is properly created and populated.
func Start(tickets io.Reader, workshops io.Reader) (db *sql.DB, err error) {
	// TODO: enable SSL
	db, err = sql.Open("postgres", DSN)
	if err != nil {
		return nil, fmt.Errorf("unable to open database: %v", err)
	}
	driver, err := pgmigrate.WithInstance(db, &pgmigrate.Config{})
	if err != nil {
		return nil, fmt.Errorf("unable to obtain driver: %v", err)
	}
	m, err := migrate.NewWithDatabaseInstance("file://"+projectpath.Root+"/postgres/migrations", "postgres", driver)
	if err != nil {
		return nil, fmt.Errorf("unable to open migrations: %v", err)
	}
	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		return nil, fmt.Errorf("unable to run migrations: %v", err)
	}
	if err := loadTickets(db, tickets); err != nil {
		return nil, fmt.Errorf("load tickets: %s", err)
	}
	if err := loadWorkshops(db, workshops); err != nil {
		return nil, fmt.Errorf("load workshops: %s", err)
	}
	return db, nil
}

func loadTickets(db *sql.DB, in io.Reader) (err error) {
	const q = `
INSERT INTO tickets (
	id,
	name,
	description,
	category,
	workshop_count,
	price_full,
	price_reduced)
VALUES ($1, $2, $3, $4, $5, $6, $7)
ON CONFLICT DO NOTHING;
`
	tickets, err := csv.LoadTickets(in)
	if err != nil {
		return err
	}
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
		} else {
			tx.Rollback()
		}
	}()
	for i, t := range tickets {
		_, err := tx.Exec(q, i, t.Name, t.Description, t.Category.String(),
			t.WorkshopCount, t.Prices[ticket.Full], t.Prices[ticket.Reduced])
		if err != nil {
			return fmt.Errorf("line %d: %s", i, err)
		}
	}
	return nil
}

func loadWorkshops(db *sql.DB, in io.Reader) (err error) {
	const q = `
INSERT INTO workshops (
	id,
	name,
	band,
	"start",
	duration,
	category,
	gauge)
VALUES ($1, $2, $3, $4, $5, $6, $7)
ON CONFLICT DO NOTHING;
`
	workshops, err := csv.LoadWorkshops(in)
	if err != nil {
		return err
	}
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
		} else {
			tx.Rollback()
		}
	}()
	for i, w := range workshops {
		_, err = tx.Exec(q, i, w.Name, w.Band, w.Start, w.Duration.Seconds(), w.Category.String(), w.Gauge)
		if err != nil {
			return fmt.Errorf("line %d: %s", i, err)
		}
	}
	return nil
}

type querier interface {
	QueryContext(context.Context, string, ...interface{}) (*sql.Rows, error)
	QueryRowContext(context.Context, string, ...interface{}) *sql.Row
}

type execer interface {
	ExecContext(context.Context, string, ...interface{}) (sql.Result, error)
}
