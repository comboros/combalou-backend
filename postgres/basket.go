package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"framagit.org/comboros/combalou/ticket"
	"framagit.org/comboros/combalou/visitor"
)

type basketDAO struct{}

func (dao *basketDAO) Insert(ctx context.Context, db querier, bask visitor.Basket) (visitor.BasketID, error) {
	const q = `
INSERT INTO baskets (
	visitor_id,
	rate)
VALUES ($1, $2)
RETURNING id`
	var id int
	err := db.QueryRowContext(ctx, q, bask.VisitorID, bask.Rate.String()).Scan(&id)
	if err != nil {
		return 0, err
	}
	return visitor.BasketID(id), nil
}

func (dao *basketDAO) Update(ctx context.Context, db execer, bask visitor.Basket) error {
	const q = `
UPDATE baskets SET
	visitor_id = $2,
	rate = $3
WHERE id = $1;
`
	res, err := db.ExecContext(ctx, q, bask.ID, bask.VisitorID, bask.Rate.String())
	if err != nil {
		return err
	}
	if n, err := res.RowsAffected(); err == nil && n == 0 {
		return errors.New("no rows affected")
	}
	return nil
}

func (dao *basketDAO) FindByVisitor(ctx context.Context, db querier, vid visitor.ID) (*visitor.Basket, error) {
	const q = `
SELECT
	b.id,
	b.visitor_id,
	b.rate
FROM
	baskets b
WHERE
	b.visitor_id = $1
ORDER BY
	b.created DESC
LIMIT 1
`
	var (
		basket visitor.Basket
		rate   string
	)
	err := db.QueryRowContext(ctx, q, vid).Scan(&basket.ID, &basket.VisitorID, &rate)
	if errors.Is(err, sql.ErrNoRows) {
		return nil, visitor.ErrNotFound
	} else if err != nil {
		return nil, fmt.Errorf("basket by visitor: %v", err)
	}
	basket.Rate, err = ticket.StringToRate(rate)
	if err != nil {
		return nil, err
	}
	return &basket, nil
}

func (dao *basketDAO) Delete(ctx context.Context, db execer, id visitor.BasketID) error {
	const q = `
DELETE FROM baskets
WHERE id = $1;
`
	res, err := db.ExecContext(ctx, q, id)
	if err != nil {
		return fmt.Errorf("delete basket: %v", err)
	}
	if n, err := res.RowsAffected(); err == nil && n == 0 {
		return visitor.ErrNotFound
	}
	return nil
}

type purchaseDAO struct{}

func (dao *purchaseDAO) FindByBasket(ctx context.Context, db querier, id visitor.BasketID) (map[ticket.ID]int, error) {
	const q = `
SELECT
	p.ticket_id,
	p.quantity
FROM
	purchases AS p
WHERE
	p.basket_id = $1
`
	rows, err := db.QueryContext(ctx, q, id)
	if err != nil {
		return nil, fmt.Errorf("purchases by basket: %v", err)
	}
	defer rows.Close()

	purchases := map[ticket.ID]int{}
	for rows.Next() {
		var (
			tid ticket.ID
			qt  int
		)
		err := rows.Scan(&tid, &qt)
		if err != nil {
			return nil, fmt.Errorf("purchases by basket: %v", err)
		}
		purchases[tid] = qt
	}
	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("purchases by basket: %v", err)
	}
	return purchases, nil
}

func (dao *purchaseDAO) Insert(ctx context.Context, db execer, bid visitor.BasketID, tid ticket.ID, qt int) error {
	const q = `
INSERT INTO purchases (
	basket_id,
	ticket_id,
	quantity)
VALUES ($1, $2, $3)
`
	_, err := db.ExecContext(ctx, q, bid, tid, qt)
	if err != nil {
		return fmt.Errorf("insert purchase %d: %v", tid, err)
	}

	return nil
}

func (dao *purchaseDAO) Update(ctx context.Context, db execer, bid visitor.BasketID, tid ticket.ID, qt int) error {
	const q = `
UPDATE
	purchases
SET
	quantity = $1
WHERE
	basket_id = $2
	AND ticket_id = $3;
`
	result, err := db.ExecContext(ctx, q, qt, bid, tid)
	if err != nil {
		return fmt.Errorf("update purchase %d: %v", tid, err)
	}
	if n, err := result.RowsAffected(); err == nil && n == 0 {
		return fmt.Errorf("update purchase %d: %v", tid, visitor.ErrNotFound)
	}
	return nil
}

func (dao *purchaseDAO) Delete(ctx context.Context, db execer, bid visitor.BasketID, tid ticket.ID) error {
	const q = `
DELETE FROM
	purchases
WHERE
	basket_id = $1
	AND ticket_id = $2;
`
	result, err := db.ExecContext(ctx, q, bid, tid)
	if err != nil {
		return fmt.Errorf("delete purchase %d: %v", tid, err)
	}
	if n, err := result.RowsAffected(); err == nil && n == 0 {
		return fmt.Errorf("delete purchase %d: %v", tid, visitor.ErrNotFound)
	}
	return nil
}

func (dao *purchaseDAO) DeleteByBasketID(ctx context.Context, db execer, bid visitor.BasketID) error {
	const q = `
DELETE FROM
	purchases
WHERE
	basket_id = $1;
`
	_, err := db.ExecContext(ctx, q, bid)
	if err != nil {
		return fmt.Errorf("delete purchases: %v", err)
	}
	return nil
}

type basketRepository struct {
	db        *sql.DB
	baskets   basketDAO
	purchases purchaseDAO
	tickets   ticket.Repository
}

func NewBasketRepository(db *sql.DB, tickets ticket.Repository) visitor.BasketRepository {
	return &basketRepository{
		db:        db,
		baskets:   basketDAO{},
		purchases: purchaseDAO{},
		tickets:   tickets,
	}
}

func (r *basketRepository) FindByVisitor(ctx context.Context, id visitor.ID) (*visitor.Basket, error) {
	tx, err := r.db.BeginTx(ctx, &sql.TxOptions{ReadOnly: true})
	if err != nil {
		return nil, fmt.Errorf("basket by visitor: %v", err)
	}

	basket, err := r.baskets.FindByVisitor(ctx, tx, id)
	if err != nil {
		return nil, fmt.Errorf("basket by visitor: %w", err)
	}
	basket.Purchases, err = r.purchases.FindByBasket(ctx, tx, basket.ID)
	if err != nil {
		return nil, fmt.Errorf("basket by visitor: %w", err)
	}

	return basket, nil
}

func (r *basketRepository) Save(ctx context.Context, bask *visitor.Basket) (err error) {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("tx: %v", err)
	}
	defer func() {
		_ = tx.Rollback()
	}()

	// If the basket has no surrogate identity, then it is the first time
	// it is persisted. Insert it in the database. Otherwise, update it.
	if bask.ID == 0 {
		id, err := r.baskets.Insert(ctx, tx, *bask)
		if err != nil {
			return fmt.Errorf("insert basket: %v", err)
		}

		bask.ID = id
	} else {
		err := r.baskets.Update(ctx, tx, *bask)
		if err != nil {
			return fmt.Errorf("update basket: %v", err)
		}
	}

	// replace the purchases collection
	if err := r.purchases.DeleteByBasketID(ctx, tx, bask.ID); err != nil {
		return fmt.Errorf("basket %v: %w", bask.ID, err)
	}
	for tid, qt := range bask.Purchases {
		err := r.purchases.Insert(ctx, tx, bask.ID, tid, qt)
		if err != nil {
			return fmt.Errorf("insert purchase: %w", err)
		}
	}

	if err := tx.Commit(); err != nil {
		return fmt.Errorf("commit tx: %v", err)
	}

	return nil
}

func (r *basketRepository) Delete(ctx context.Context, bask *visitor.Basket) error {
	if err := r.baskets.Delete(ctx, r.db, bask.ID); err != nil {
		return err
	}
	bask.ID = 0
	return nil
}
