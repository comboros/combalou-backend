package postgres

import (
	"database/sql/driver"
	"fmt"
	"strconv"
	"time"
)

type interval time.Duration

func (dst *interval) Scan(src interface{}) error {
	switch src := src.(type) {
	case int64:
		*dst = interval(src)
	case []byte:
		val, err := strconv.ParseInt(string(src), 10, 64)
		if err != nil {
			return fmt.Errorf("scan interval: %v", err)
		}
		*dst = interval(val)
	case nil:
		*dst = 0
	default:
		return fmt.Errorf("scan interval: cannot convert from %T", src)
	}
	return nil
}

func (src interval) Value() (driver.Value, error) {
	return time.Duration(src).Seconds(), nil
}
