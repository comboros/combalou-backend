// +build it

package postgres

import (
	"context"
	"database/sql"
	"testing"

	"framagit.org/comboros/combalou/helper"
	"framagit.org/comboros/combalou/ticket"
	"framagit.org/comboros/combalou/visitor"
	"framagit.org/comboros/combalou/workshop"
	"github.com/google/go-cmp/cmp"
)

func TestSaveBasket(t *testing.T) {
	bob := helper.Bob(t)

	tests := map[string]struct {
		query string
		count int
	}{
		"insert basket": {
			query: "SELECT COUNT(*) FROM baskets WHERE id = $1",
			count: 1,
		},
		"insert each purchase": {
			query: "SELECT COUNT(*) FROM purchases p WHERE p.basket_id = $1",
			count: len(bob.Basket.Purchases),
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			WithDB(t, "save basket", func(db *sql.DB) {
				deps := dependencies(db)
				ctx := context.Background()

				// to save a basket, we need its visitor to exist
				v := bob.Visitor
				if err := deps.visitors.Save(ctx, &v); err != nil {
					t.Fatalf("Saving bob: %v", err)
				}

				give := bob.Basket
				give.VisitorID = v.ID
				if err := deps.baskets.Save(ctx, &give); err != nil {
					t.Fatalf("Saving bob’s basket: %v", err)
				}

				var n int
				if err := db.QueryRow(tt.query, give.ID).Scan(&n); err != nil {
					t.Fatalf("Querying: %v\n\t%q", err, tt.query)
				}
				if n != tt.count {
					t.Errorf("Got count %d (expected %d)\n\t%q", n, tt.count, tt.query)
				}
			})
		})
	}
}

func TestSaveExistingBasket(t *testing.T) {
	bob := helper.Bob(t)

	WithDB(t, "save existing basket", func(db *sql.DB) {
		deps := dependencies(db)
		ctx := context.Background()

		tickets, err := deps.tickets.All(ctx)
		if err != nil {
			t.Fatalf("tickets.All(): %v", err)
		}

		// Let’s save bob.
		if err := deps.visitors.Save(ctx, &bob.Visitor); err != nil {
			t.Fatalf("visitors.Save(): %v", err)
		}
		bob.Basket.VisitorID = bob.Visitor.ID
		if err := deps.baskets.Save(ctx, &bob.Basket); err != nil {
			t.Fatalf("baskets.Save(): %v", err)
		}

		// Change its basket from the mixt pass to the complete pass.
		if err := bob.Basket.CancelPurchase(tickets[1]); err != nil {
			t.Fatalf("Basket.CancelPurchase(tickets[1]): %v", err)
		}
		if err := bob.Basket.CancelPurchase(tickets[5]); err != nil {
			t.Fatalf("Basket.CancelPurchase(tickets[5]): %v", err)
		}
		if err := bob.Basket.AddPurchase(tickets[0], 1); err != nil {
			t.Fatalf("Basket.AddPurchase(tickets[0]): %v", err)
		}

		// Save the resulting basket.
		if err := deps.baskets.Save(ctx, &bob.Basket); err != nil {
			t.Fatalf("baskets.Save(): %v", err)
		}

		// Check.
		got, err := deps.baskets.FindByVisitor(ctx, bob.Visitor.ID)
		if err != nil {
			t.Fatalf("baskets.FindByVisitor(%v): %v", bob.Visitor.ID, err)
		}
		if diff := cmp.Diff(&bob.Basket, got); diff != "" {
			t.Errorf("Unexpected basket retrieved:\n%s", diff)
		}
	})
}

func TestDeleteBasket(t *testing.T) {
	bob := helper.Bob(t)

	tests := map[string]struct {
		query string
	}{
		"delete basket": {
			query: "SELECT COUNT(*) FROM baskets WHERE id = $1",
		},
		"delete each purchase": {
			query: "SELECT COUNT(*) FROM purchases p WHERE p.basket_id = $1",
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			WithDB(t, "delete basket", func(db *sql.DB) {
				deps := dependencies(db)
				ctx := context.Background()

				// to save a basket, we need its visitor to exist
				v := bob.Visitor
				if err := deps.visitors.Save(ctx, &v); err != nil {
					t.Fatalf("Saving bob: %v", err)
				}

				give := bob.Basket
				give.VisitorID = v.ID
				if err := deps.baskets.Save(ctx, &give); err != nil {
					t.Fatalf("Saving bob’s basket: %v", err)
				}

				if err := deps.baskets.Delete(ctx, &give); err != nil {
					t.Fatalf("Deleting bob’s basket: %v", err)
				}

				var n int
				if err := db.QueryRow(tt.query, give.ID).Scan(&n); err != nil {
					t.Fatalf("Querying: %v\n\t%q", err, tt.query)
				}
				if n != 0 {
					t.Errorf("Got count %d (expected 0)\n\t%q", n, tt.query)
				}
			})
		})
	}

}

type repositories struct {
	workshops workshop.Repository
	tickets   ticket.Repository
	bookings  visitor.BookingRepository
	visitors  visitor.Repository
	baskets   visitor.BasketRepository
	payments  visitor.PaymentRepository
}

func dependencies(db *sql.DB) repositories {
	repos := repositories{
		workshops: NewWorkshopRepository(db),
		tickets:   NewTicketRepository(db),
	}
	repos.bookings = NewBookingRepository(db, repos.workshops)
	repos.visitors = NewVisitorRepository(db, repos.bookings)
	repos.baskets = NewBasketRepository(db, repos.tickets)
	repos.payments = NewPaymentRepository(db)
	return repos
}
