package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"framagit.org/comboros/combalou/visitor"
)

type paymentDAO struct{}

func (dao paymentDAO) Insert(ctx context.Context, tx *sql.Tx, pmt visitor.Payment) (visitor.PaymentID, error) {
	const q = `
INSERT INTO payments (
	visitor_id,
	amount,
	mode)
VALUES ($1, $2, $3)
RETURNING id;
`
	var id visitor.PaymentID
	err := tx.QueryRowContext(ctx, q, pmt.VisitorID, pmt.Amount, pmt.Mode.String()).Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (dao paymentDAO) Update(ctx context.Context, tx *sql.Tx, pmt visitor.Payment) error {
	const q = `
UPDATE payments SET
	visitor_id = $1,
	amount = $2,
	mode = $3
WHERE id = $4;
`
	res, err := tx.ExecContext(ctx, q, pmt.VisitorID, pmt.Amount, pmt.Mode.String(), pmt.ID)
	if err != nil {
		return err
	}

	if n, err := res.RowsAffected(); err == nil && n == 0 {
		return errors.New("no rows affected")
	}

	return nil
}

func (dao paymentDAO) DeleteByVisitorID(ctx context.Context, tx *sql.Tx, vid visitor.ID) error {
	const q = `
DELETE FROM payments
WHERE visitor_id = $1;
`
	_, err := tx.ExecContext(ctx, q, vid)
	return err
}

func (dao paymentDAO) FindByVisitorID(ctx context.Context, tx *sql.Tx, vid visitor.ID) (*visitor.Payment, error) {
	const q = `
SELECT
	id,
	visitor_id,
	amount,
	mode
FROM
	payments
WHERE
	visitor_id = $1;
`
	var pmt visitor.Payment
	var mode string
	err := tx.QueryRowContext(ctx, q, vid).
		Scan(&pmt.ID, &pmt.VisitorID, &pmt.Amount, &mode)
	if errors.Is(err, sql.ErrNoRows) {
		return nil, visitor.ErrNotFound
	} else if err != nil {
		return nil, err
	}

	pmt.Mode, err = visitor.StringToPaymentMode(mode)
	if err != nil {
		return nil, err
	}

	return &pmt, nil
}

type paymentRepository struct {
	db       DB
	payments paymentDAO
}

func NewPaymentRepository(db *sql.DB) visitor.PaymentRepository {
	return paymentRepository{
		db:       DB{db},
		payments: paymentDAO{},
	}
}

func (r paymentRepository) Save(ctx context.Context, pmt *visitor.Payment) error {
	return r.db.Update(ctx, func(tx *sql.Tx) error {
		id, err := r.payments.Insert(ctx, tx, *pmt)
		if err != nil {
			return fmt.Errorf("insert: %v", err)
		}
		pmt.ID = id

		return nil
	})
}

func (r paymentRepository) DeleteByVisitor(ctx context.Context, vid visitor.ID) error {
	return r.db.Update(ctx, func(tx *sql.Tx) error {
		if err := r.payments.DeleteByVisitorID(ctx, tx, vid); err != nil {
			return fmt.Errorf("delete: %v", err)
		}
		return nil
	})
}

func (r paymentRepository) FindByVisitor(ctx context.Context, vid visitor.ID) (*visitor.Payment, error) {
	var pmt *visitor.Payment

	err := r.db.ReadOnly(ctx, func(tx *sql.Tx) (err error) {
		pmt, err = r.payments.FindByVisitorID(ctx, tx, vid)
		return err
	})

	return pmt, err
}
