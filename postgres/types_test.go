package postgres

import (
	"testing"
)

func TestIntervalScan(t *testing.T) {
	tests := map[string]struct {
		give    interface{}
		want    interval
		wantErr bool
	}{
		"nil":            {give: nil, want: 0},
		"int64":          {give: int64(42), want: 42},
		"valid []byte":   {give: []byte("98"), want: 98},
		"invalid []byte": {give: []byte("foo"), wantErr: true},
		"string":         {give: "hello", wantErr: true},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			var i interval
			err := i.Scan(tt.give)
			if err != nil && !tt.wantErr {
				t.Errorf("Unexpected error: %v", err)
			} else if err != nil {
				return
			}
			if i != tt.want {
				t.Errorf("Scanned %d – expected %d", i, tt.want)
			}
		})
	}
}
