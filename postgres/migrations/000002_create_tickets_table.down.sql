BEGIN;
DROP TRIGGER IF EXISTS tickets_refresh_modified ON tickets;
DROP TABLE IF EXISTS tickets;
COMMIT;
