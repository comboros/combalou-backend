BEGIN;

CREATE TABLE tickets (
        id		serial		PRIMARY KEY,
        name		text		NOT NULL,
        description	text		NOT NULL,
        category	text		NOT NULL,
        workshop_count	integer		NOT NULL,
        price_full	numeric(13, 2)	NOT NULL CONSTRAINT positive_price_full		CHECK (price_full >= 0),
        price_reduced	numeric(13, 2)	NOT NULL CONSTRAINT positive_price_reduced	CHECK (price_reduced >= 0),

        created		timestamp	DEFAULT CURRENT_TIMESTAMP,
        modified	timestamp	DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER tickets_refresh_modified
BEFORE UPDATE ON tickets
FOR EACH ROW EXECUTE PROCEDURE refresh_modified();

COMMIT;
