BEGIN;
DROP TRIGGER IF EXISTS purchases_refresh_modified ON purchases;
DROP TABLE IF EXISTS purchases;
COMMIT;
