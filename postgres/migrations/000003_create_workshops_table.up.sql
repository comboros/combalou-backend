BEGIN;

CREATE TABLE workshops (
        id		serial			PRIMARY KEY,
        name		text			NOT NULL,
        band		text			NOT NULL,
        "start"		timestamptz		NOT NULL,
        duration	interval second(0)	NOT NULL CONSTRAINT positive_duration CHECK (duration > '0 second'),
        category	text			NOT NULL,
        gauge		text			NOT NULL,

        created		timestamp		DEFAULT CURRENT_TIMESTAMP,
        modified	timestamp		DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER workshops_refresh_modified
BEFORE UPDATE ON workshops
FOR EACH ROW EXECUTE PROCEDURE refresh_modified();

COMMIT;
