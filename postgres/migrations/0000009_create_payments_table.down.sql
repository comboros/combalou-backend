BEGIN;
DROP TRIGGER IF EXISTS payments_refresh_modified ON payments;
DROP TABLE IF EXISTS payments;
COMMIT;
