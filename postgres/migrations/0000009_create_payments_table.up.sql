BEGIN;

CREATE TABLE payments (
        id		serial		PRIMARY KEY,
        visitor_id	integer		NOT NULL REFERENCES visitors (id),
        amount		numeric(13, 2)	NOT NULL,
        "mode"		text		NOT NULL,

        created		timestamp	DEFAULT CURRENT_TIMESTAMP,
        modified	timestamp	DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER payments_refresh_modified
BEFORE UPDATE ON payments
FOR EACH ROW EXECUTE PROCEDURE refresh_modified();

COMMIT;
