CREATE OR REPLACE FUNCTION refresh_modified()
RETURNS TRIGGER AS $$
BEGIN
        NEW.modified = CURRENT_TIMESTAMP;
        RETURN NEW;
END;
$$ language 'plpgsql';

