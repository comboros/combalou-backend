BEGIN;

CREATE TABLE addresses (
        id		serial		PRIMARY KEY,
        visitor_id	integer		NOT NULL REFERENCES visitors (id) ON DELETE CASCADE,
        line_1		text		NOT NULL,
        line_2		text		NOT NULL,
        postal_code	text		NOT NULL,
        city		text		NOT NULL,
        country		text		NOT NULL,

        created		timestamp	DEFAULT CURRENT_TIMESTAMP,
        modified	timestamp	DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER addresses_refresh_modified
BEFORE UPDATE ON addresses
FOR EACH ROW EXECUTE PROCEDURE refresh_modified();

COMMIT;
