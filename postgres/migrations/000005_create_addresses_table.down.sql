BEGIN;
DROP TRIGGER IF EXISTS addresses_refresh_modified ON addresses;
DROP TABLE IF EXISTS addresses;
COMMIT;
