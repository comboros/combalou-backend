BEGIN;

CREATE TABLE visitors (
        id			serial		PRIMARY KEY,
        name			text		NOT NULL,
        surname			text		NOT NULL,
        phone			text		NOT NULL,
        email			text		NOT NULL,
        responsible_adult	text,		-- nullable
        payment_mode		text		NOT NULL,
        billed			timestamp,	-- nullable

        created			timestamp	DEFAULT CURRENT_TIMESTAMP,
        modified		timestamp	DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER visitors_refresh_modified
BEFORE UPDATE ON visitors
FOR EACH ROW EXECUTE PROCEDURE refresh_modified();

COMMIT;
