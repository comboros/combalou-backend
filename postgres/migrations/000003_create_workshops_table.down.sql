BEGIN;
DROP TRIGGER IF EXISTS workshops_refresh_modified ON workshops;
DROP TABLE IF EXISTS workshops;
COMMIT;
