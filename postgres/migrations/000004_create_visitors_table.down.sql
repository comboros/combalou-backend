BEGIN;
DROP TRIGGER IF EXISTS visitors_refresh_modified ON visitors;
DROP TABLE IF EXISTS visitors;
COMMIT;
