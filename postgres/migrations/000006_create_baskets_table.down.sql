BEGIN;
DROP TRIGGER IF EXISTS baskets_refresh_modified ON baskets;
DROP TABLE IF EXISTS baskets;
COMMIT;
