CREATE TABLE bookings (
        visitor_id	integer		REFERENCES visitors (id) ON DELETE CASCADE,
        workshop_id	integer		REFERENCES workshops (id),

        created		timestamp	DEFAULT CURRENT_TIMESTAMP
);
