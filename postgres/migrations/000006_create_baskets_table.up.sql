BEGIN;

CREATE TABLE baskets (
        id			serial		PRIMARY KEY,
        visitor_id		integer		REFERENCES visitors (id),
        rate			text		NOT NULL,

        created			timestamp	DEFAULT CURRENT_TIMESTAMP,
        modified		timestamp	DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER baskets_refresh_modified
BEFORE UPDATE ON baskets
FOR EACH ROW EXECUTE PROCEDURE refresh_modified();

COMMIT;
