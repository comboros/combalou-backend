BEGIN;

CREATE TABLE purchases (
        basket_id	integer		NOT NULL REFERENCES baskets (id) ON DELETE CASCADE,
        ticket_id	integer		NOT NULL REFERENCES tickets (id),
        quantity	integer		NOT NULL CONSTRAINT positive_quantity CHECK (quantity > 0),

        created		timestamp	DEFAULT CURRENT_TIMESTAMP,
        modified	timestamp	DEFAULT CURRENT_TIMESTAMP,

        PRIMARY KEY (basket_id, ticket_id)
);

CREATE TRIGGER purchases_refresh_modified
BEFORE UPDATE ON purchases
FOR EACH ROW EXECUTE PROCEDURE refresh_modified();

COMMIT;
