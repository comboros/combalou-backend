package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"framagit.org/comboros/combalou/ticket"
)

type ticketRepository struct {
	db *sql.DB
}

func NewTicketRepository(db *sql.DB) ticket.Repository {
	return &ticketRepository{db}
}

func (r *ticketRepository) All(ctx context.Context) (_ map[ticket.ID]ticket.Ticket, err error) {
	const q = `
SELECT
	id,
	name,
	description,
	price_full,
	price_reduced,
	category,
	workshop_count
FROM
	tickets
`
	rows, err := r.db.QueryContext(ctx, q)
	if err != nil {
		return nil, fmt.Errorf("tickets: %v", err)
	}
	defer func() {
		if closeErr := rows.Close(); closeErr != nil {
			if err != nil {
				return // favor the original error
			}
			err = closeErr
		}
	}()
	tickets := make(map[ticket.ID]ticket.Ticket)
	for rows.Next() {
		t, err := scanTicket(rows)
		if err != nil {
			return nil, fmt.Errorf("tickets: %v", err)
		}
		tickets[t.ID] = *t
	}
	if err = rows.Err(); err != nil {
		return nil, fmt.Errorf("tickets: %v", err)
	}
	return tickets, nil
}

func scanTicket(rows *sql.Rows) (*ticket.Ticket, error) {
	var t ticket.Ticket
	var c string
	var p struct{ full, reduced float64 }
	err := rows.Scan(&t.ID, &t.Name, &t.Description, &p.full, &p.reduced, &c, &t.WorkshopCount)
	if err != nil {
		return nil, err
	}
	t.Category, err = ticket.StringToCategory(c)
	if err != nil {
		return nil, err
	}
	t.Prices = map[ticket.Rate]float64{
		ticket.Full:    p.full,
		ticket.Reduced: p.reduced,
	}
	return &t, nil
}
