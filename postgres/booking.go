package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"framagit.org/comboros/combalou/visitor"
	"framagit.org/comboros/combalou/workshop"
)

type bookingDAO struct{}

func (r *bookingDAO) All(ctx context.Context, tx *sql.Tx) (map[visitor.ID]visitor.Bookings, error) {
	const q = `
SELECT
	visitor_id,
	workshop_id
FROM bookings;
`
	rows, err := tx.QueryContext(ctx, q)
	if err != nil {
		return nil, fmt.Errorf("select: %v", err)
	}
	defer rows.Close()

	all := make(map[visitor.ID]visitor.Bookings)
	for rows.Next() {
		var (
			v visitor.ID
			w workshop.ID
		)
		err = rows.Scan(&v, &w)
		if err != nil {
			return nil, fmt.Errorf("scan: %v", err)
		}
		if all[v] == nil {
			all[v] = make(visitor.Bookings)
		}
		all[v][w] = true
	}
	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("rows: %v", err)
	}

	return all, nil
}

type bookingRepository struct {
	db        *sql.DB
	workshops workshop.Repository
}

func NewBookingRepository(db *sql.DB, workshops workshop.Repository) visitor.BookingRepository {
	return &bookingRepository{
		db:        db,
		workshops: workshops,
	}
}

func (r *bookingRepository) CountByWorkshop(ctx context.Context) (_ map[workshop.ID]int, err error) {
	const q = `
SELECT
	workshop_id,
	COUNT(visitor_id)
FROM bookings
GROUP BY workshop_id
`
	rows, err := r.db.QueryContext(ctx, q)
	if err != nil {
		return nil, fmt.Errorf("count bookings: %v", err)
	}
	defer func() {
		if closeErr := rows.Close(); closeErr != nil {
			if err != nil {
				return // favor the original error
			}
			err = fmt.Errorf("count bookings: %v", err)
		}
	}()

	result := make(map[workshop.ID]int)
	for rows.Next() {
		var id workshop.ID
		var n int
		if err = rows.Scan(&id, &n); err != nil {
			return nil, fmt.Errorf("count bookings: %v", err)
		}
		result[id] = n
	}
	if err = rows.Err(); err != nil {
		return nil, fmt.Errorf("count bookings: %v", err)
	}
	return result, nil
}

func (r *bookingRepository) FindByVisitor(ctx context.Context, id visitor.ID) (visitor.Bookings, error) {
	const q = `
SELECT
	workshop_id
FROM bookings
WHERE visitor_id = $1
`
	rows, err := r.db.QueryContext(ctx, q, id)
	if err != nil {
		return nil, fmt.Errorf("visitor bookings: %v", err)
	}
	defer rows.Close()

	books := make(visitor.Bookings)
	for rows.Next() {
		var wid workshop.ID
		err = rows.Scan(&wid)
		if err != nil {
			return nil, fmt.Errorf("visitor bookings: %v", err)
		}
		books[wid] = true
	}
	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("visitor bookings: %v", err)
	}

	return books, nil
}

func (r *bookingRepository) Add(ctx context.Context, visID visitor.ID, workID workshop.ID) error {
	const q = `
INSERT INTO bookings (
	visitor_id,
	workshop_id)
VALUES ($1, $2);
`
	_, err := r.db.ExecContext(ctx, q, visID, workID)
	if err != nil {
		return fmt.Errorf("insert booking: %v", err)
	}
	return nil
}

func (r *bookingRepository) Remove(ctx context.Context, visID visitor.ID, workID workshop.ID) error {
	const q = `
DELETE FROM bookings
WHERE visitor_id = $1
AND workshop_id = $2;
`
	res, err := r.db.ExecContext(ctx, q, visID, workID)
	if err != nil {
		return fmt.Errorf("delete booking: %v", err)
	}
	if n, err := res.RowsAffected(); err == nil && n == 0 {
		return visitor.ErrNotFound
	}
	return nil
}
