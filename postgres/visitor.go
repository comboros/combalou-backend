package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"framagit.org/comboros/combalou/visitor"
)

type visitorRepository struct {
	db         DB
	bookings   visitor.BookingRepository // TODO: replace by DAO (strangle)
	bookingDAO *bookingDAO
	visitors   *visitorDAO
	addresses  *addressDAO
}

func NewVisitorRepository(db *sql.DB, bookings visitor.BookingRepository) visitor.Repository {
	return &visitorRepository{
		db:         DB{db},
		bookings:   bookings,
		bookingDAO: &bookingDAO{},
		visitors:   &visitorDAO{},
		addresses:  &addressDAO{},
	}
}

// Save persists a visitor, its basket and its bookings.
func (r *visitorRepository) Save(ctx context.Context, v *visitor.Visitor) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("save visitor: %v", err)
	}
	// TODO: defer tx.Rollback()

	if err := saveVisitor(ctx, tx, v); err != nil {
		return fmt.Errorf("save visitor: %w", err)
	}
	if err := saveAddress(ctx, tx, v.ID, &v.Address); err != nil {
		return fmt.Errorf("save visitor: %w", err)
	}
	if err := saveBookings(ctx, tx, v.ID, v.Bookings); err != nil {
		return fmt.Errorf("save visitor: %w", err)
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("save visitor: %v", err)
	}
	return nil
}

func (r *visitorRepository) Update(ctx context.Context, v visitor.Visitor) (err error) {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("update visitor: %v", err)
	}
	defer func() {
		if err == nil {
			tx.Commit()
		} else {
			tx.Rollback()
		}
	}()
	if err := r.visitors.Update(ctx, tx, v); err != nil {
		return fmt.Errorf("update visitor: %w", err)
	}
	if err := r.addresses.UpdateByVisitorID(ctx, tx, v.Address, v.ID); err != nil {
		return fmt.Errorf("update visitor: %w", err)
	}
	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("update visitor: %v", err)
	}
	return nil
}

func (r *visitorRepository) Delete(ctx context.Context, id visitor.ID) error {
	const q = `DELETE FROM visitors WHERE id = $1;`
	res, err := r.db.ExecContext(ctx, q, id)
	if err != nil {
		return fmt.Errorf("delete visitor: %v", err)
	}
	if n, err := res.RowsAffected(); err == nil && n == 0 {
		return visitor.ErrNotFound
	}
	return nil
}

func (r *visitorRepository) All(ctx context.Context) (visitors map[visitor.ID]visitor.Visitor, err error) {
	r.db.ReadOnly(ctx, func(tx *sql.Tx) error {
		visitors, err = r.visitors.All(ctx, tx)
		if err != nil {
			return fmt.Errorf("all visitors: %v", err)
		}

		bookings, err := r.bookingDAO.All(ctx, tx)
		if err != nil {
			return fmt.Errorf("all bookings: %v", err)
		}

		addresses, err := r.addresses.All(ctx, tx)
		if err != nil {
			return fmt.Errorf("all addresses: %v", err)
		}

		for id, v := range visitors {
			v.Bookings = bookings[id]
			v.Address = addresses[id]
			visitors[id] = v
		}

		return nil
	})
	return visitors, err
}

func (r *visitorRepository) Find(ctx context.Context, id visitor.ID) (*visitor.Visitor, error) {
	v, err := r.findVisitor(ctx, id)
	if err != nil {
		return nil, fmt.Errorf("find visitor: %w", err)
	}

	addr, err := r.findAddress(ctx, v.ID)
	if err != nil {
		return nil, fmt.Errorf("find visitor: %w", err)
	}
	v.Address = *addr

	if v.Bookings, err = r.bookings.FindByVisitor(ctx, id); err != nil {
		return nil, fmt.Errorf("find visitor: %w", err)
	}

	return v, nil
}

func (r *visitorRepository) findAddress(ctx context.Context, vid visitor.ID) (*visitor.Address, error) {
	const q = `
SELECT
	id,
	line_1,
	line_2,
	postal_code,
	city,
	country
FROM addresses
WHERE visitor_id = $1
`
	var addr visitor.Address
	err := r.db.QueryRowContext(ctx, q, vid).
		Scan(&addr.ID, &addr.Line1, &addr.Line2, &addr.PostalCode,
			&addr.City, &addr.Country)
	if err == sql.ErrNoRows {
		return nil, visitor.ErrNotFound
	} else if err != nil {
		return nil, fmt.Errorf("query: %v", err)
	}
	return &addr, nil
}

func (r *visitorRepository) findVisitor(ctx context.Context, id visitor.ID) (*visitor.Visitor, error) {
	const q = `
SELECT
	id,
	name,
	surname,
	phone,
	email,
	responsible_adult,
	payment_mode,
	billed
FROM visitors
WHERE id = $1
`
	var v visitor.Visitor
	var pm string
	var billed sql.NullTime
	err := r.db.QueryRowContext(ctx, q, id).
		Scan(&v.ID, &v.Name, &v.Surname, &v.Phone, &v.Email,
			&v.Responsible, &pm, &billed)
	if err == sql.ErrNoRows {
		return nil, visitor.ErrNotFound
	} else if err != nil {
		return nil, fmt.Errorf("query: %v", err)
	}
	v.PaymentMode, err = visitor.StringToPaymentMode(pm)
	if err != nil {
		return nil, err
	}
	if billed.Valid {
		v.Billed = billed.Time
	}
	return &v, err
}

func saveVisitor(ctx context.Context, tx *sql.Tx, v *visitor.Visitor) error {
	const q = `
INSERT INTO visitors (
	name,
	surname,
	phone,
	email,
	responsible_adult,
	payment_mode,
	billed)
VALUES ($1, $2, $3, $4, $5, $6, $7)
RETURNING id
`
	var id visitor.ID
	err := tx.QueryRowContext(ctx, q, v.Name, v.Surname, v.Phone,
		v.Email, v.Responsible, v.PaymentMode.String(), v.Billed).
		Scan(&id)
	if err != nil {
		return fmt.Errorf("save visitor: %v", err)
	}
	v.ID = id
	return nil
}

func saveAddress(ctx context.Context, tx *sql.Tx, vid visitor.ID, addr *visitor.Address) error {
	const q = `
INSERT INTO addresses (
	visitor_id,
	line_1,
	line_2,
	postal_code,
	city,
	country)
VALUES ($1, $2, $3, $4, $5, $6)
RETURNING id
`
	var id int
	err := tx.QueryRowContext(ctx, q, vid, addr.Line1, addr.Line2, addr.PostalCode,
		addr.City, addr.Country).
		Scan(&id)
	if err != nil {
		return fmt.Errorf("save address: %v", err)
	}
	addr.ID = id
	return nil
}

func saveBookings(ctx context.Context, tx *sql.Tx, visitorID visitor.ID, bks visitor.Bookings) error {
	const q = `
INSERT INTO bookings (
	visitor_id,
	workshop_id)
VALUES ($1, $2)
`
	for id := range bks {
		_, err := tx.ExecContext(ctx, q, visitorID, id)
		if err != nil {
			return fmt.Errorf("save booking: %v", err)
		}
	}
	return nil
}

type visitorDAO struct{}

func (r *visitorDAO) All(ctx context.Context, tx *sql.Tx) (map[visitor.ID]visitor.Visitor, error) {
	const q = `
SELECT
	id,
	surname,
	name,
	phone,
	email,
	responsible_adult,
	payment_mode,
	billed
FROM visitors
`
	rows, err := tx.QueryContext(ctx, q)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	visitors := map[visitor.ID]visitor.Visitor{}
	for rows.Next() {
		var v visitor.Visitor
		var pm string
		var billed sql.NullTime
		err := rows.Scan(&v.ID, &v.Surname, &v.Name, &v.Phone,
			&v.Email, &v.Responsible, &pm, &billed)
		if err != nil {
			return nil, err
		}
		v.PaymentMode, err = visitor.StringToPaymentMode(pm)
		if err != nil {
			return nil, err
		}
		if billed.Valid {
			v.Billed = billed.Time
		}
		visitors[v.ID] = v
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return visitors, nil
}
func (dao *visitorDAO) Update(ctx context.Context, db execer, v visitor.Visitor) error {
	const q = `
UPDATE visitors
SET
	surname = $2,
	name = $3,
	phone = $4,
	email = $5,
	responsible_adult = $6,
	payment_mode = $7
WHERE id = $1;
`
	res, err := db.ExecContext(ctx, q, v.ID,
		v.Surname,
		v.Name,
		v.Phone,
		v.Email,
		v.Responsible,
		v.PaymentMode.String(),
	)
	if err != nil {
		return fmt.Errorf("update visitor: %v", err)
	}
	if n, err := res.RowsAffected(); err == nil && n == 0 {
		return visitor.ErrNotFound
	}
	return nil
}

type addressDAO struct{}

func (dao *addressDAO) All(ctx context.Context, tx *sql.Tx) (map[visitor.ID]visitor.Address, error) {
	const q = `
SELECT
	visitor_id,
	id,
	line_1,
	line_2,
	postal_code,
	city,
	country
FROM addresses;
`
	rows, err := tx.QueryContext(ctx, q)
	if err != nil {
		return nil, fmt.Errorf("select: %v", err)
	}
	all := make(map[visitor.ID]visitor.Address)
	for rows.Next() {
		var (
			id   visitor.ID
			addr visitor.Address
		)
		err := rows.Scan(&id, &addr.ID, &addr.Line1, &addr.Line2,
			&addr.PostalCode, &addr.City, &addr.Country)
		if err != nil {
			return nil, fmt.Errorf("scan: %v", err)
		}
		all[id] = addr
	}
	return all, nil
}

func (dao *addressDAO) UpdateByVisitorID(ctx context.Context, db execer, addr visitor.Address, id visitor.ID) error {
	const q = `
UPDATE
	addresses
SET
	line_1 = $2,
	line_2 = $3,
	postal_code = $4,
	city = $5,
	country = $6
WHERE
	visitor_id = $1;
`
	res, err := db.ExecContext(ctx, q, id,
		addr.Line1,
		addr.Line2,
		addr.PostalCode,
		addr.City,
		addr.Country,
	)
	if err != nil {
		return fmt.Errorf("update address: %v", err)
	}
	if n, err := res.RowsAffected(); err == nil && n == 0 {
		return visitor.ErrNotFound
	}
	return nil
}
