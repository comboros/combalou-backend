// +build it

package postgres

import (
	"context"
	"database/sql"
	"errors"
	"testing"

	"framagit.org/comboros/combalou/helper"
	"framagit.org/comboros/combalou/visitor"
	"github.com/google/go-cmp/cmp"
)

func TestSaveNewPayment(t *testing.T) {
	WithDB(t, "save new payment", func(db *sql.DB) {
		deps := dependencies(db)
		ctx := context.Background()

		bob := helper.Bob(t).Visitor
		if err := deps.visitors.Save(ctx, &bob); err != nil {
			t.Fatalf("Saving Bob: %v", err)
		}

		pmt := visitor.Payment{
			VisitorID: bob.ID,
			Amount:    125.19,
			Mode:      visitor.Check,
		}
		if err := deps.payments.Save(ctx, &pmt); err != nil {
			t.Fatalf("Saving Bob’s payment: %v", err)
		}

		if pmt.ID == 0 {
			t.Error("Expected Bob’s payment to have a valid ID; got 0")
		}

		var n int
		err := db.QueryRow("SELECT COUNT(*) FROM payments WHERE visitor_id = $1 AND id = $2", bob.ID, pmt.ID).Scan(&n)
		if err != nil {
			t.Fatalf("Counting Bob’s payments: %v", err)
		}
		if n != 1 {
			t.Fatalf("Counted %d payments (expected 1)", n)
		}
	})
}

func TestSaveExistingPayment(t *testing.T) {
	WithDB(t, "save existing payment", func(db *sql.DB) {
		deps := dependencies(db)
		ctx := context.Background()

		bob := helper.Bob(t).Visitor
		if err := deps.visitors.Save(ctx, &bob); err != nil {
			t.Fatalf("Saving Bob: %v", err)
		}

		pmt := visitor.Payment{
			VisitorID: bob.ID,
			Amount:    10.92,
			Mode:      visitor.Transfer,
		}
		if err := deps.payments.Save(ctx, &pmt); err != nil {
			t.Fatalf("Saving Bob’s payment: %v", err)
		}

		pmt.Amount = 19.20
		if err := deps.payments.Save(ctx, &pmt); err != nil {
			t.Fatalf("Saving Bob’s payment again: %v", err)
		}

		var amount float64
		err := db.QueryRow("SELECT amount FROM payments WHERE visitor_id = $1 AND id = $2", bob.ID, pmt.ID).Scan(&amount)
		if err != nil {
			t.Fatalf("Querying amount of Bob’s payment: %v", err)
		}
		if float64(amount) != pmt.Amount {
			t.Errorf("Bob’s payment amounts %f (expected %f)", amount, pmt.Amount)
		}
	})
}

func TestDeletePaymentByVisitor(t *testing.T) {
	WithDB(t, "delete payment by visitor", func(db *sql.DB) {
		deps := dependencies(db)
		ctx := context.Background()

		alice := helper.Alice(t)
		alice.SaveVisitor(t, deps.visitors)

		pmt := &visitor.Payment{
			VisitorID: alice.Visitor.ID,
			Amount:    32.20,
			Mode:      visitor.Transfer,
		}
		if err := deps.payments.Save(ctx, pmt); err != nil {
			t.Fatalf("Saving Alice’s payment: %v", err)
		}

		if err := deps.payments.DeleteByVisitor(ctx, alice.Visitor.ID); err != nil {
			t.Fatalf("Deleting Alice’s payment: %v", err)
		}

		if _, err := deps.payments.FindByVisitor(ctx, alice.Visitor.ID); err == nil {
			t.Error("Alice’s payment still exists")
		} else if !errors.Is(err, visitor.ErrNotFound) {
			t.Fatalf("Finding Alice’s payment: %v", err)
		}
	})
}

func TestFindPaymentByVisitor(t *testing.T) {
	WithDB(t, "find payment by visitor", func(db *sql.DB) {
		deps := dependencies(db)
		ctx := context.Background()

		alice := helper.Alice(t)
		alice.SaveVisitor(t, deps.visitors)

		pmt := &visitor.Payment{
			VisitorID: alice.Visitor.ID,
			Amount:    90.9,
			Mode:      visitor.Check,
		}
		if err := deps.payments.Save(ctx, pmt); err != nil {
			t.Fatalf("Saving Alice’s payment: %v", err)
		}

		got, err := deps.payments.FindByVisitor(ctx, alice.Visitor.ID)
		if err != nil {
			t.Fatalf("Finding Alice’s payment: %v", err)
		}

		if diff := cmp.Diff(pmt, got); diff != "" {
			t.Errorf("Alice’s payment mismatch (-want +got):\n%s", diff)
		}
	})
}
