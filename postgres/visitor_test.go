// +build it

package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"os"
	"testing"

	"framagit.org/comboros/combalou/data"
	"framagit.org/comboros/combalou/helper"
	"framagit.org/comboros/combalou/visitor"

	"github.com/DATA-DOG/go-txdb"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/google/go-cmp/cmp"
	_ "github.com/lib/pq"
)

func TestMain(m *testing.M) {
	// Load tickets & workshops in the database
	files, err := data.Load("testdata/tickets.csv", "testdata/workshops.csv")
	if err != nil {
		fmt.Printf("Unable to load data files: %v", err)
		os.Exit(1)
	}
	db, err := Start(files.Tickets, files.Workshops)
	if err != nil {
		fmt.Printf("Unable to initialise PostgreSQL connection: %v", err)
		os.Exit(1)
	}
	db.Close()

	// Register txdb, that will be used for all upcoming DB connections
	txdb.Register("txdb", "postgres", DSN)

	// Run tests
	os.Exit(m.Run())
}

func WithDB(t *testing.T, name string, body func(*sql.DB)) {
	t.Helper()

	db, err := sql.Open("txdb", name)
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	body(db)
}

func TestSave(t *testing.T) {
	WithDB(t, "save visitor", func(db *sql.DB) {
		v := visitor.Visitor{
			Surname: "Bob",
			Name:    "Sponge",
			Phone:   "418 123 4567",
			Email:   "spongebob@example.com",
			Address: visitor.Address{
				Line1:      "123 Baker st.",
				Line2:      "Appt. 12",
				PostalCode: "123 ABC",
				City:       "Chicago",
				Country:    "Under the see",
			},
			Bookings: visitor.Bookings{
				1: true,
				2: true,
			},
			Responsible: "Patrick",
			PaymentMode: visitor.Transfer,
		}

		workshops := NewWorkshopRepository(db)
		bookings := NewBookingRepository(db, workshops)
		visitors := NewVisitorRepository(db, bookings)
		if err := visitors.Save(context.Background(), &v); err != nil {
			t.Fatalf("Unexpected error: %v", err)
		}

		tests := map[string]struct {
			query string
			count int
		}{
			"visitors":  {"SELECT COUNT(*) FROM visitors WHERE id = $1", 1},
			"addresses": {"SELECT COUNT(*) FROM addresses a JOIN visitors v ON v.id = a.visitor_id WHERE v.id = $1", 1},
			"bookings":  {"SELECT COUNT(*) FROM bookings WHERE visitor_id = $1", 2},
		}

		for name, tt := range tests {
			t.Run(name, func(t *testing.T) {
				var n int
				err := db.QueryRow(tt.query, v.ID).Scan(&n)
				if err != nil {
					t.Fatalf("Could not query database: %v", err)
				}
				if n != tt.count {
					t.Errorf("%d rows in table %q; expected %d", n, name, tt.count)
				}
			})
		}
	})
}

func TestUpdate(t *testing.T) {
	WithDB(t, "update visitor", func(db *sql.DB) {
		roger := visitor.Visitor{
			Surname: "Roger",
			Name:    "Devlamenque",
			Phone:   "418 018 2890",
			Email:   "rodevl@example.com",
			Address: visitor.Address{
				Line1:      "13 rue Lavoisier",
				Line2:      "Appt. 2",
				PostalCode: "59800",
				City:       "Lille",
				Country:    "France",
			},
			Bookings: visitor.Bookings{
				1: true,
			},
			PaymentMode: visitor.Transfer,
		}
		workshops := NewWorkshopRepository(db)
		bookings := NewBookingRepository(db, workshops)
		visitors := NewVisitorRepository(db, bookings)
		if err := visitors.Save(context.Background(), &roger); err != nil {
			t.Fatalf("Unexpected error: %v", err)
		}

		changes := visitor.Visitor{
			ID:          roger.ID,
			Surname:     "Albert",
			Name:        "Deconninck",
			Responsible: "Roger Devlamenque",
			Phone:       "418 018 2890",
			Email:       "albert.deco@example.com",
			Address: visitor.Address{
				Line1:      "12 rue Lavoisier",
				PostalCode: "59800",
				City:       "Lille",
				Country:    "France",
			},
			PaymentMode: visitor.Transfer,
		}
		err := visitors.Update(context.Background(), changes)
		if err != nil {
			t.Fatalf("Unexpected error: %v", err)
		}

		want := visitor.Visitor{
			ID:          roger.ID,
			Surname:     "Albert",
			Name:        "Deconninck",
			Responsible: "Roger Devlamenque",
			Phone:       "418 018 2890",
			Email:       "albert.deco@example.com",
			Address: visitor.Address{
				ID:         roger.Address.ID,
				Line1:      "12 rue Lavoisier",
				PostalCode: "59800",
				City:       "Lille",
				Country:    "France",
			},
			Bookings: visitor.Bookings{
				1: true,
			},
			PaymentMode: visitor.Transfer,
		}
		got, err := visitors.Find(context.Background(), roger.ID)
		if err != nil {
			t.Fatalf("Unexpected error: %v", err)
		}
		if diff := cmp.Diff(&want, got); diff != "" {
			t.Errorf("visitor.Repository.Update() mismatch (-want +got):\n%s", diff)
		}
	})
}

func TestDelete(t *testing.T) {
	WithDB(t, "delete visitor", func(db *sql.DB) {
		bob := visitor.Visitor{
			Surname: "Bob",
			Name:    "L’éponge carrée",
			Phone:   "512 512 512",
			Email:   "bob.squarefish@example.com",
			Address: visitor.Address{
				Line1:      "122, rue des Conques",
				PostalCode: "123 456",
				City:       "Bikini Bottom",
				Country:    "Ocean",
			},
			Bookings: visitor.Bookings{
				1: true,
			},
			PaymentMode: visitor.Check,
		}
		workshops := NewWorkshopRepository(db)
		bookings := NewBookingRepository(db, workshops)
		visitors := NewVisitorRepository(db, bookings)
		if err := visitors.Save(context.Background(), &bob); err != nil {
			t.Fatalf("Unexpected error: %v", err)
		}

		// find bob
		if _, err := visitors.Find(context.Background(), bob.ID); err != nil {
			t.Fatalf("Find visitor after save: %v (expected nil)", err)
		}

		// delete it
		if err := visitors.Delete(context.Background(), bob.ID); err != nil {
			t.Fatalf("Delete visitor: %v (expected nil)", err)
		}

		// find it again
		if _, err := visitors.Find(context.Background(), bob.ID); !errors.Is(err, visitor.ErrNotFound) {
			t.Fatalf("Find visitor after delete: %#v (expected ErrNotFound)", err)
		}

		b, err := bookings.FindByVisitor(context.Background(), bob.ID)
		if err != nil {
			t.Fatalf("Find bookings after delete: %v (expected nil)", err)
		}
		if len(b) > 0 {
			t.Fatalf("Find bookings after delete: got %d bookings (expected 0)", len(b))
		}

		// TODO: check addresses
	})
}

func TestAll(t *testing.T) {
	WithDB(t, "all visitors", func(db *sql.DB) {
		deps := dependencies(db)
		ctx := context.Background()

		all := []*helper.Actor{helper.Alice(t), helper.Bob(t), helper.Martine(t)}
		want := make(map[visitor.ID]visitor.Visitor, len(all))
		for i := range all {
			all[i].SaveVisitor(t, deps.visitors)
			all[i].SaveBasket(t, deps.baskets)
			v := all[i].Visitor
			want[v.ID] = v
		}

		got, err := deps.visitors.All(ctx)
		if err != nil {
			t.Fatalf("Unexpected error: %v", err)
		}

		if diff := cmp.Diff(want, got); diff != "" {
			t.Errorf("visitor.Repository.All() mismatch (-want +got):\n%s", diff)
		}
	})
}
