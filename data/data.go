package data

import (
	"fmt"
	"os"
)

type Files struct {
	Tickets   *os.File
	Workshops *os.File
}

func Load(ticketpath string, workshoppath string) (*Files, error) {
	var files Files
	var err error
	files.Tickets, err = os.Open(ticketpath)
	if err != nil {
		return nil, fmt.Errorf("load files: %v", err)
	}
	files.Workshops, err = os.Open(workshoppath)
	if err != nil {
		return nil, fmt.Errorf("load files: %v", err)
	}
	return &files, nil
}
