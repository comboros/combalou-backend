package csv

import (
	"encoding/csv"
	"fmt"
	"io"
	"strconv"
	"time"

	"framagit.org/comboros/combalou/ticket"
	"framagit.org/comboros/combalou/workshop"
)

func LoadTickets(in io.Reader) ([]ticket.Ticket, error) {
	records, err := readcsv(in)
	if err != nil {
		return nil, fmt.Errorf("invalid format: %v", err)
	}
	tickets := make([]ticket.Ticket, 0, len(records))
	for i, r := range records {
		t, err := convertTicket(i, r)
		if err != nil {
			return nil, fmt.Errorf("invalid format (line %d): %v", i, err)
		}
		tickets = append(tickets, *t)
	}
	return tickets, nil
}

func LoadWorkshops(in io.Reader) ([]workshop.Workshop, error) {
	records, err := readcsv(in)
	if err != nil {
		return nil, fmt.Errorf("invalid format: %s", err.Error())
	}
	workshops := make([]workshop.Workshop, 0, len(records))
	for i, r := range records {
		w, err := convertWorkshop(i, r)
		if err != nil {
			return nil, fmt.Errorf("invalid format (line %d): %v", i, err)
		}
		workshops = append(workshops, *w)
	}
	return workshops, nil
}

func readcsv(in io.Reader) ([][]string, error) {
	r := csv.NewReader(in)
	r.Comment = '#'
	return r.ReadAll()
}

func convertTicket(lineno int, v []string) (*ticket.Ticket, error) {
	cat, err := ticket.StringToCategory(v[0])
	if err != nil {
		return nil, err
	}

	prices := make(map[ticket.Rate]float64)
	p, err := strconv.ParseFloat(v[2], 32)
	if err != nil {
		return nil, fmt.Errorf("full price is not a float: %v", err)
	}
	prices[ticket.Full] = float64(p)

	p, err = strconv.ParseFloat(v[3], 32)
	if err != nil {
		return nil, fmt.Errorf("reduced price is not a float: %v", err)
	}
	prices[ticket.Reduced] = float64(p)

	count, err := strconv.ParseInt(v[5], 10, 32)
	if err != nil {
		return nil, fmt.Errorf("workshop count is not an int: %v", err)

	}
	return &ticket.Ticket{
		ID:            ticket.ID(lineno),
		Category:      cat,
		Name:          v[1],
		Prices:        prices,
		Description:   v[4],
		WorkshopCount: int(count),
	}, nil
}

func convertWorkshop(lineno int, v []string) (*workshop.Workshop, error) {
	cat, err := workshop.StringToCategory(v[2])
	if err != nil {
		return nil, err
	}
	start, err := time.Parse("2006-01-02 15:04", v[3])
	if err != nil {
		return nil, fmt.Errorf("wrong time format: %v", err)
	}
	duration, err := time.ParseDuration(v[4])
	if err != nil {
		return nil, fmt.Errorf("wrong duration format: %v", err)
	}
	gauge, err := strconv.ParseInt(v[5], 10, 32)
	if err != nil {
		return nil, fmt.Errorf("gauge is not an int: %v", err)
	}
	return &workshop.Workshop{
		ID:       workshop.ID(lineno),
		Name:     v[0],
		Band:     v[1],
		Category: cat,
		Start:    start,
		Duration: duration,
		Gauge:    int(gauge),
	}, nil
}
