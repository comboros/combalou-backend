// +build acc

package acceptance

import (
	"io"
	"net/http"
	"strconv"
	"testing"
)

type Requester interface {
	Tickets(t *testing.T) *http.Request
	Workshops(t *testing.T) *http.Request
	Register(t *testing.T, in io.Reader) *http.Request
	Find(t *testing.T, id int) *http.Request
	Suggest(t *testing.T, term string) *http.Request
	UpdateVisitor(t *testing.T, id int, in io.Reader) *http.Request
	UpdateBasket(t *testing.T, id int, in io.Reader) *http.Request
	UpdateBooking(t *testing.T, id int, in io.Reader) *http.Request
	CreateMainPayment(t *testing.T, id int) *http.Request
	Invoice(t *testing.T, id int) *http.Request
	Delete(t *testing.T, id int) *http.Request
}

type phpRequester struct{}

const (
	_phpHost  = "http://localhost:8000"
	_restHost = "http://localhost:8080"
)

func (r phpRequester) Tickets(t *testing.T) *http.Request {
	t.Helper()
	req, err := http.NewRequest(http.MethodGet, _phpHost+"/catalog.php", nil)
	if err != nil {
		t.Fatalf("NewRequest(GET /catalog.php): %v", err)
	}
	return req
}

func (r phpRequester) Workshops(t *testing.T) *http.Request {
	t.Helper()
	req, err := http.NewRequest(http.MethodGet, _phpHost+"/workshops.php", nil)
	if err != nil {
		t.Fatalf("NewRequest(GET /workshops.php): %v", err)
	}
	return req
}

func (r phpRequester) Register(t *testing.T, in io.Reader) *http.Request {
	t.Helper()
	req, err := http.NewRequest(http.MethodPost, _phpHost+"/initial-registration.php", in)
	if err != nil {
		t.Fatalf("NewRequest(POST /initial-registration.php): %v", err)
	}
	req.Header.Set("Content-Type", "application/json")
	return req
}

func (r phpRequester) Find(t *testing.T, id int) *http.Request {
	t.Helper()
	path := "/info.php?id=" + strconv.Itoa(id)
	req, err := http.NewRequest(http.MethodGet, _phpHost+path, nil)
	if err != nil {
		t.Fatalf("NewRequest(GET %s): %v", path, err)
	}
	return req
}

func (r phpRequester) Suggest(t *testing.T, term string) *http.Request {
	t.Helper()
	req, err := http.NewRequest(http.MethodGet, _phpHost+"/suggest.php", nil)
	if err != nil {
		t.Fatalf("NewRequest(GET /suggest.php): %v", err)
	}
	q := req.URL.Query()
	q.Set("term", term)
	req.URL.RawQuery = q.Encode()
	return req
}

func (r phpRequester) UpdateVisitor(t *testing.T, id int, in io.Reader) *http.Request {
	t.Helper()
	path := "/update-personal-info.php?id=" + strconv.Itoa(id)
	req, err := http.NewRequest(http.MethodPost, _phpHost+path, in)
	if err != nil {
		t.Fatalf("NewRequest(POST %s): %v", path, err)
	}
	req.Header.Set("Content-Type", "application/json")
	return req
}

func (r phpRequester) UpdateBasket(t *testing.T, id int, in io.Reader) *http.Request {
	t.Helper()
	path := "/update-basket.php?id=" + strconv.Itoa(id)
	req, err := http.NewRequest(http.MethodPost, _phpHost+path, in)
	if err != nil {
		t.Fatalf("NewRequest(POST %s): %v", path, err)
	}
	req.Header.Set("Content-Type", "application/json")
	return req
}

func (r phpRequester) UpdateBooking(t *testing.T, id int, in io.Reader) *http.Request {
	t.Helper()
	path := "/update-booking.php?id=" + strconv.Itoa(id)
	req, err := http.NewRequest(http.MethodPost, _phpHost+path, in)
	if err != nil {
		t.Fatalf("NewRequest(POST %s): %v", path, err)
	}
	req.Header.Set("Content-Type", "application/json")
	return req
}

func (r phpRequester) CreateMainPayment(t *testing.T, id int) *http.Request {
	t.Helper()
	path := "/validate-payment.php?id=" + strconv.Itoa(id)
	req, err := http.NewRequest(http.MethodPost, _phpHost+path, nil)
	if err != nil {
		t.Fatalf("NewRequest(POST %s): %v", path, err)
	}
	return req
}

func (r phpRequester) Invoice(t *testing.T, id int) *http.Request {
	t.Helper()
	path := "/send-invoice.php?id=" + strconv.Itoa(id)
	req, err := http.NewRequest(http.MethodPost, _phpHost+path, nil)
	if err != nil {
		t.Fatalf("NewRequest(POST %s): %v", path, err)
	}
	return req
}

func (r phpRequester) Delete(t *testing.T, id int) *http.Request {
	t.Helper()
	path := "/delete-registration.php?id=" + strconv.Itoa(id)
	req, err := http.NewRequest(http.MethodPost, _phpHost+path, nil)
	if err != nil {
		t.Fatalf("NewRequest(POST %s): %v", path, err)
	}
	return req
}

type restRequester struct{}

func (r restRequester) Tickets(t *testing.T) *http.Request {
	t.Helper()
	req, err := http.NewRequest(http.MethodGet, _restHost+"/tickets", nil)
	if err != nil {
		t.Fatalf("NewRequest(GET /tickets): %v", err)
	}
	return req
}

func (r restRequester) Workshops(t *testing.T) *http.Request {
	t.Helper()
	req, err := http.NewRequest(http.MethodGet, _restHost+"/workshops", nil)
	if err != nil {
		t.Fatalf("NewRequest(GET /workshops): %v", err)
	}
	return req
}

func (r restRequester) Register(t *testing.T, in io.Reader) *http.Request {
	t.Helper()
	req, err := http.NewRequest(http.MethodPost, _restHost+"/registrations", in)
	if err != nil {
		t.Fatalf("NewRequest(POST /registrations): %v", err)
	}
	req.Header.Set("Content-Type", "application/json")
	return req
}

func (r restRequester) Find(t *testing.T, id int) *http.Request {
	t.Helper()
	path := "/registration/" + strconv.Itoa(id)
	req, err := http.NewRequest(http.MethodGet, _restHost+path, nil)
	if err != nil {
		t.Fatalf("NewRequest(GET %s): %v", path, err)
	}
	return req
}

func (r restRequester) Suggest(t *testing.T, term string) *http.Request {
	t.Helper()
	req, err := http.NewRequest(http.MethodGet, _restHost+"/registrations", nil)
	if err != nil {
		t.Fatalf("NewRequest(GET /registrations): %v", err)
	}
	q := req.URL.Query()
	q.Set("term", term)
	req.URL.RawQuery = q.Encode()
	return req
}

func (r restRequester) UpdateVisitor(t *testing.T, id int, in io.Reader) *http.Request {
	t.Helper()
	path := "/visitor/" + strconv.Itoa(id)
	req, err := http.NewRequest(http.MethodPut, _restHost+path, in)
	if err != nil {
		t.Fatalf("NewRequest(PUT %s): %v", path, err)
	}
	req.Header.Set("Content-Type", "application/json")
	return req
}

func (r restRequester) UpdateBasket(t *testing.T, id int, in io.Reader) *http.Request {
	t.Helper()
	path := "/visitor/" + strconv.Itoa(id) + "/basket"
	req, err := http.NewRequest(http.MethodPut, _restHost+path, in)
	if err != nil {
		t.Fatalf("NewRequest(PUT %s): %v", path, err)
	}
	req.Header.Set("Content-Type", "application/json")
	return req
}

func (r restRequester) UpdateBooking(t *testing.T, id int, in io.Reader) *http.Request {
	t.Helper()
	path := "/visitor/" + strconv.Itoa(id) + "/booking"
	req, err := http.NewRequest(http.MethodPut, _restHost+path, in)
	if err != nil {
		t.Fatalf("NewRequest(PUT %s): %v", path, err)
	}
	req.Header.Set("Content-Type", "application/json")
	return req
}

func (r restRequester) CreateMainPayment(t *testing.T, id int) *http.Request {
	t.Helper()
	path := "/visitor/" + strconv.Itoa(id) + "/payment"
	req, err := http.NewRequest(http.MethodPost, _restHost+path, nil)
	if err != nil {
		t.Fatalf("NewRequest(POST %s): %v", path, err)
	}
	return req
}

func (r restRequester) Invoice(t *testing.T, id int) *http.Request {
	t.Helper()
	path := "/visitor/" + strconv.Itoa(id) + "/invoice"
	req, err := http.NewRequest(http.MethodPost, _restHost+path, nil)
	if err != nil {
		t.Fatalf("NewRequest(POST %s): %v", path, err)
	}
	return req
}

func (r restRequester) Delete(t *testing.T, id int) *http.Request {
	t.Helper()
	path := "/registration/" + strconv.Itoa(id)
	req, err := http.NewRequest(http.MethodDelete, _restHost+path, nil)
	if err != nil {
		t.Fatalf("NewRequest(DELETE %s): %v", path, err)
	}
	return req
}
