// +build acc

package acceptance

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"
)

type Client struct {
	http.Client
	req Requester
}

func (c Client) Tickets(t *testing.T) []Ticket {
	t.Helper()
	res, err := c.Do(c.req.Tickets(t))
	if err != nil {
		t.Fatalf("HTTP request: %v", err)
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		t.Fatalf("Return code: %v (expected 200)", res.StatusCode)
	}

	var tickets []Ticket
	err = json.NewDecoder(res.Body).Decode(&tickets)
	if err != nil {
		t.Fatalf("Invalid response body: %v", err)
	}

	return tickets
}

func (c Client) Workshops(t *testing.T) []Workshop {
	t.Helper()
	res, err := c.Do(c.req.Workshops(t))
	if err != nil {
		t.Fatalf("HTTP request: %v", err)
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		t.Fatalf("Return code: %v (expected 200)", res.StatusCode)
	}

	var workshops []Workshop
	err = json.NewDecoder(res.Body).Decode(&workshops)
	if err != nil {
		t.Fatalf("Invalid response body: %v", err)
	}

	return workshops
}

func (c Client) Register(t *testing.T, r Registration) int {
	t.Helper()
	payload, err := json.Marshal(r)
	if err != nil {
		t.Fatalf("Registration cannot be marshalled: %v", err)
	}
	res, err := c.Do(c.req.Register(t, bytes.NewReader(payload)))
	if err != nil {
		t.Fatalf("HTTP request: %v", err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		t.Fatalf("Return code: %v (expected 200)", res.StatusCode)
	}

	var id int
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatalf("Error reading body: %v", err)
	}
	err = json.Unmarshal(body, &id)
	if err != nil {
		t.Errorf("Wrong result format: %v", err)
		print(string(body))
	}
	return id
}

func (c Client) Find(t *testing.T, id int) RegistrationInfo {
	t.Helper()
	res, err := c.Do(c.req.Find(t, id))
	if err != nil {
		t.Fatalf("HTTP request: %v", err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		t.Fatalf("Wrong status code: %v (expected 200)", res.StatusCode)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatalf("Error reading body: %v", err)
	}
	var r RegistrationInfo
	err = json.Unmarshal(body, &r)
	if err != nil {
		t.Errorf("Wrong result format: %v", err)
		fmt.Printf("Body: %s", string(body))
	}
	return r
}

func (c Client) Suggest(t *testing.T, term string) []SearchResult {
	t.Helper()
	res, err := c.Do(c.req.Suggest(t, term))
	if err != nil {
		t.Fatalf("HTTP request: %v", err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		t.Fatalf("Wrong status code: %v (expected 200)", res.StatusCode)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatalf("Error reading body: %v", err)
	}
	var results []SearchResult
	err = json.Unmarshal(body, &results)
	if err != nil {
		t.Errorf("Wrong result format: %v", err)
		fmt.Printf("Body: %s", string(body))
	}
	return results
}

func (c Client) UpdateVisitor(t *testing.T, id int, dto updateVisitorRequest) {
	t.Helper()
	payload, err := json.Marshal(dto)
	if err != nil {
		t.Fatalf("Registration is not marshallable: %v", err)
	}

	res, err := c.Do(c.req.UpdateVisitor(t, id, bytes.NewReader(payload)))
	if err != nil {
		t.Fatalf("HTTP request: %v", err)
	}
	defer res.Body.Close()
	if res.StatusCode != 204 {
		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			t.Fatalf("Error reading body: %v", err)
		}
		fmt.Printf("Body: %s\n", string(body))
		t.Fatalf("Wrong status code: %v (expected 204)", res.StatusCode)
	}
}

func (c Client) UpdateBasket(t *testing.T, id int, basket []BasketEntry) {
	t.Helper()
	payload, err := json.Marshal(basket)
	if err != nil {
		t.Fatalf("Basket is not marshallable: %v", err)
	}
	res, err := c.Do(c.req.UpdateBasket(t, id, bytes.NewReader(payload)))
	if err != nil {
		t.Fatalf("HTTP request: %v", err)
	}
	defer res.Body.Close()
	if res.StatusCode != 204 {
		t.Fatalf("Wrong status code: %v (expected 204)", res.StatusCode)
	}
}

func (c Client) UpdateBooking(t *testing.T, id int, bookings []int) {
	t.Helper()
	payload, err := json.Marshal(bookings)
	if err != nil {
		t.Fatalf("Bookings aren’t marshallable: %v", err)
	}
	res, err := c.Do(c.req.UpdateBooking(t, id, bytes.NewReader(payload)))
	if err != nil {
		t.Fatalf("HTTP request: %v", err)
	}
	defer res.Body.Close()
	if res.StatusCode != 204 {
		t.Fatalf("Wrong status code: %v (expected 204)", res.StatusCode)
	}
}

func (c Client) CreateMainPayment(t *testing.T, id int) {
	t.Helper()
	c.do(t, c.req.CreateMainPayment(t, id), http.StatusNoContent)
}

func (c Client) Invoice(t *testing.T, id int) {
	t.Helper()
	c.do(t, c.req.Invoice(t, id), http.StatusNoContent)
}

func (c Client) Delete(t *testing.T, id int) {
	t.Helper()
	c.do(t, c.req.Delete(t, id), http.StatusNoContent)
}

func (c Client) do(t *testing.T, req *http.Request, expected int) {
	t.Helper()
	res, err := c.Do(req)
	if err != nil {
		t.Fatalf("HTTP request: %v", err)
	}
	defer res.Body.Close()
	if res.StatusCode != expected {
		t.Fatalf("Wrong status code: %v (expected %d)", res.StatusCode, expected)
	}
}

type Ticket struct {
	ID           int
	Name         string
	Description  string
	Price        float64
	ReducedPrice float64
	Span         string
	Workshops    int
}

func (t Ticket) Valid() bool {
	return t.Name != "" && t.Description != "" && t.Span != ""
}

type Workshop struct {
	ID        int
	Name      string
	Band      string
	Category  string
	Day       string
	Moment    string
	Remainder int
}

func (w *Workshop) Valid() bool {
	return w.Name != "" && w.Band != "" &&
		w.Category != "" && w.Day != "" && w.Moment != "" &&
		w.Remainder != 0
}

type Registration struct {
	ID          int           `json:"id,omitempty"`
	Info        personalInfo  `json:"personal-info"`
	Contact     contact       `json:"contact"`
	Basket      []BasketEntry `json:"basket"`
	Workshops   []int         `json:"workshops"` // a list of workshop id
	PaymentMode string        `json:"payment-mode"`
	PriceType   string        `json:"price-type"`
}

type RegistrationInfo struct {
	ID          int           `json:"id,omitempty"`
	Info        personalInfo  `json:"personalInfo"`
	Contact     contact       `json:"contact"`
	Basket      []BasketEntry `json:"basket"`
	Workshops   []Workshop    `json:"workshops"`
	PaymentMode string        `json:"paymentMode"`
	PriceType   string        `json:"priceType"`
	LastBilling int           `json:"lastBilling"` // timestamp
	State       string        `json:"state"`
}

type BasketEntry struct {
	Ticket   int `json:"ticket"`
	Quantity int `json:"quantity"`
}

type personalInfo struct {
	Surname     string `json:"surname"`
	Name        string `json:"name"`
	Responsible string `json:"responsible"`
}

type contact struct {
	Phone   string  `json:"phone"`
	Email   string  `json:"email"`
	Address address `json:"address"`
}

type address struct {
	Line1      string `json:"line1"`
	Line2      string `json:"line2"`
	PostalCode string `json:"postalCode"`
	City       string `json:"city"`
	Country    string `json:"country"`
}

type SearchResult struct {
	ID          int
	PriceType   string
	PaymentType string
	State       string
	Surname     string
	Name        string
	Responsible string
	Line1       string
	Line2       string
	PostalCode  string
	City        string
	Country     string
	Phone       string
	Email       string
	LastBilling int
}

type updateVisitorRequest struct {
	Name         string `json:"name"`
	Surname      string `json:"surname"`
	Responsible  string `json:"responsible"`
	AddressLine1 string `json:"addressLine1"`
	AddressLine2 string `json:"addressLine2"`
	PostalCode   string `json:"postalCode"`
	City         string `json:"city"`
	Country      string `json:"country"`
	Phone        string `json:"phone"`
	Email        string `json:"email"`
	PaymentMode  string `json:"paymentType"`
	PriceType    string `json:"priceType"`
	State        string `json:"state"`
}
