// +build acc

// Compatibility/regression tests for the V1 API.
package acceptance

import (
	"flag"
	"net/http"
	"os"
	"sort"
	"testing"
	"time"
)

var _apiVersion string

func TestMain(m *testing.M) {
	flag.StringVar(&_apiVersion, "api", "php", "either php or rest")
	flag.Parse()

	os.Exit(m.Run())
}

func TestTickets(t *testing.T) {
	client := newClient(t)
	tickets := client.Tickets(t)

	for _, ticket := range tickets {
		if !ticket.Valid() {
			t.Fatalf("Wrong ticket format: some fields were left empty: %+v", ticket)
		}
	}

	// tickets must be sorted by price decreasing
	sorted := sort.SliceIsSorted(tickets, func(i, j int) bool {
		return tickets[i].Price > tickets[j].Price
	})
	if !sorted {
		t.Fatalf("Tickets aren’t sorted by price:\n%#v", tickets)
	}
}

func TestWorkshops(t *testing.T) {
	client := newClient(t)
	workshops := client.Workshops(t)

	for _, w := range workshops {
		if !w.Valid() {
			t.Fatalf("Wrong workshop format: some fields were left empty: %+v", w)
		}
	}

	// TODO: we should test that workshops are sorted by start time and
	// name, but right now, it would imply defining a specific order on
	// weekdays, and that would make the test complex and brittle.
}

// TestRegistration is a complete registration scenario, from initial
// registration to payment confirmation.
// TODO: payment confirmation!
func TestRegistration(t *testing.T) {
	var (
		visitorID int
		client    = newClient(t)
	)
	t.Run("Initial registration", func(t *testing.T) {
		r := Registration{
			Info: personalInfo{"Roger", "Devlamenque", ""},
			Contact: contact{"01 02 03 04 05", "rodevl@example.com",
				address{"12 rue du Foin", "", "59800", "Lille", "France"},
			},
			Basket: []BasketEntry{
				{5, 2}, {1, 1},
			},
			Workshops:   []int{2, 7, 14, 16, 22, 31},
			PaymentMode: "transfer",
			PriceType:   "full",
		}
		visitorID = client.Register(t, r)
	})
	t.Run("Suggest", func(t *testing.T) {
		results := client.Suggest(t, "devlamenque")
		if len(results) == 0 {
			t.Errorf("Suggest(devlamenque) returned no result")
		}

		for _, res := range results {
			if res.ID == visitorID && res.State != "unpaid" {
				t.Errorf("Suggest(devlamenque) returned state %q (\"unpaid\" expected)",
					res.State)
				break
			}
		}
	})
	t.Run("Find", func(t *testing.T) {
		info := client.Find(t, visitorID)
		if info.State != "unpaid" {
			t.Errorf("State: %q (expected \"unpaid\")", info.State)
		}
	})
	t.Run("Update visitor", func(t *testing.T) {
		// Changes the civil state and the address, keeps the rest as is.
		dto := updateVisitorRequest{
			Surname:      "Roger",
			Name:         "Devlamenque-Deconnincq",
			Phone:        "01 02 03 04 05",
			Email:        "rodevl@example.com",
			AddressLine1: "29 avenue de la Grange",
			AddressLine2: "Appt. 3",
			PostalCode:   "63000",
			City:         "Clermont-Ferrand",
			Country:      "France",
			PaymentMode:  "transfer",
			PriceType:    "reduced",
			State:        "unpaid",
		}
		client.UpdateVisitor(t, visitorID, dto)
	})
	t.Run("Change one booking", func(t *testing.T) {
		// Replace workshop 16 by workshop 19 (same day, same hour)
		bookings := []int{2, 7, 14, 19, 22, 31}
		client.UpdateBooking(t, visitorID, bookings)
	})
	t.Run("Update basket", func(t *testing.T) {
		// Old basket was 1 “passe mixte” (gives 4 workshops) and 2 unitary
		// workshops, giving a total of 6 workshops; new basket is 1 “passe
		// programmation” that gives no workshop, and 3 unitary workshops.
		// That way, we add one purchase, remove one purchase and change one
		// purchase.
		basket := []BasketEntry{{2, 1}, {5, 3}}
		client.UpdateBasket(t, visitorID, basket)
	})
	t.Run("Update booking accordingly", func(t *testing.T) {
		bookings := []int{19, 22, 31}
		client.UpdateBooking(t, visitorID, bookings)
	})
	t.Run("Re-send invoice", func(t *testing.T) {
		client.Invoice(t, visitorID)
	})
	t.Run("Create main payment", func(t *testing.T) {
		client.CreateMainPayment(t, visitorID)
	})
	// We find again the registration, in case one of our modifications
	// would have rendered it non fetchable.
	// TODO: check state is “paid”
	t.Run("Find #2", func(t *testing.T) {
		info := client.Find(t, visitorID)
		if info.State != "paid" {
			t.Errorf("State: %q (expected \"paid\")", info.State)
		}
	})
	t.Run("Delete", func(t *testing.T) {
		client.Delete(t, visitorID)
	})
}

func newClient(t *testing.T) Client {
	t.Helper()
	httpClient := http.Client{Timeout: 3 * time.Second}

	switch _apiVersion {
	case "php":
		return Client{httpClient, phpRequester{}}
	case "rest":
		return Client{httpClient, restRequester{}}
	default:
		t.Fatalf("Unknown API version: %s", _apiVersion)
	}
	// Unreachable
	return Client{}
}
