package main

import (
	"database/sql"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"framagit.org/comboros/combalou/data"
	"framagit.org/comboros/combalou/data/csv"
	"framagit.org/comboros/combalou/email"
	"framagit.org/comboros/combalou/inmem"
	"framagit.org/comboros/combalou/postgres"
	"framagit.org/comboros/combalou/registration"
	"framagit.org/comboros/combalou/templates"
	"framagit.org/comboros/combalou/ticket"
	"framagit.org/comboros/combalou/visitor"
	"framagit.org/comboros/combalou/workshop"

	"github.com/go-kit/kit/log"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
)

func main() {
	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr))
		logger = log.With(logger, "time", log.DefaultTimestampUTC)
		logger = log.With(logger, "caller", log.DefaultCaller)
	}

	cfg := LoadConfig()

	logger.Log("level", "info", "msg", "Loading data from CSV files")
	files, err := data.Load(cfg.paths.tickets, cfg.paths.workshops)
	if err != nil {
		logger.Log("level", "error", "msg", "Unable to load data files", "err", err)
		os.Exit(1)
	}

	var db *sql.DB
	if cfg.persistence.postgres {
		logger.Log("level", "info", "msg", "PostgreSQL-based persistence: opening connection")
		var err error
		if db, err = postgres.Start(files.Tickets, files.Workshops); err != nil {
			logger.Log("level", "error", "msg", "Unable to initialise PostgreSQL connection", "err", err)
			os.Exit(1)
		}
		defer db.Close()
	}

	var registrations registration.Service
	{
		var tickets ticket.Repository
		var workshops workshop.Repository
		var visitors visitor.Repository
		var baskets visitor.BasketRepository
		var payments visitor.PaymentRepository
		var bookings visitor.BookingRepository

		if cfg.persistence.postgres {
			tickets = postgres.NewTicketRepository(db)
			workshops = postgres.NewWorkshopRepository(db)
			payments = postgres.NewPaymentRepository(db)
			baskets = postgres.NewBasketRepository(db, tickets)
			bookings = postgres.NewBookingRepository(db, workshops)
			visitors = postgres.NewVisitorRepository(db, bookings)
		} else {
			allTickets, err := csv.LoadTickets(files.Tickets)
			if err != nil {
				logger.Log("level", "error", "msg", "Could not load tickets", "err", err)
				os.Exit(1)
			}
			allWorkshops, err := csv.LoadWorkshops(files.Workshops)
			if err != nil {
				logger.Log("level", "error", "msg", "Could not load workshops", "err", err)
				os.Exit(1)
			}
			tickets = inmem.NewTicketRepository(allTickets)
			workshops = inmem.NewWorkshopRepository(allWorkshops)
			visitors = inmem.NewVisitorRepository()
			baskets = inmem.NewBasketRepository(visitors, tickets)
			bookings = inmem.NewBookingRepository(visitors, workshops)
			payments = inmem.NewPaymentRepository()
		}

		sender := email.Sender{
			From: cfg.smtp.from,
			Transport: email.SMTPTransport{
				Host:     cfg.smtp.host,
				Port:     cfg.smtp.port,
				User:     cfg.smtp.user,
				Password: cfg.smtp.password,
			},
			Tmpl: templates.New(cfg.smtp.from),
		}
		if cfg.env == "local" {
			sender.Transport = email.StdoutTransport{}
		}

		registrations = registration.NewService(visitors, tickets, workshops, baskets, bookings, payments, sender)
		registrations = registration.NewLoggingService(logger, registrations)
	}

	mux := http.NewServeMux()
	mux.Handle("/", registration.MakeHandler(registrations, logger))

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGINT)
		errs <- fmt.Errorf("%s", <-c)
	}()
	go func() {
		logger.Log("transport", "HTTP", "msg", "Started")
		errs <- http.ListenAndServe(":"+cfg.server.port, mux)
	}()

	logger.Log("terminated", <-errs)
}
