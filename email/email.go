package email

import (
	"fmt"
	"net/smtp"
	"strings"

	"framagit.org/comboros/combalou/templates"
	"framagit.org/comboros/combalou/ticket"
	"framagit.org/comboros/combalou/visitor"
	"framagit.org/comboros/combalou/workshop"
)

type Transport interface {
	Send(from, to, msg string) error
}

type Sender struct {
	From      string
	Transport Transport
	Tmpl      templates.Tmpl
}

func (s Sender) SendInvoice(v visitor.Visitor, basket visitor.Basket,
	tickets map[ticket.ID]ticket.Ticket, workshops map[workshop.ID]workshop.Workshop) error {
	body, err := s.Tmpl.ConfirmRegistration(v, basket, tickets, workshops)
	if err != nil {
		return err
	}
	if err := s.Transport.Send(s.From, v.Email, body); err != nil {
		return fmt.Errorf("send: %v", err)
	}
	return nil
}

func (s Sender) SendPaymentConfirmation(v visitor.Visitor) error {
	body, err := s.Tmpl.ConfirmPayment(v)
	if err != nil {
		return err
	}
	if err := s.Transport.Send(s.From, v.Email, body); err != nil {
		return fmt.Errorf("send: %v", err)
	}
	return nil
}

type SMTPTransport struct {
	Host     string // Host of the SMTP server (ex: smtp.example.com)
	Port     string // Port of the SMTP server (ex: 25)
	User     string
	Password string
}

func (s SMTPTransport) Send(from, to, msg string) error {
	body := strings.ReplaceAll(msg, "\n", "\r\n")
	return smtp.SendMail(s.Host+":"+s.Port, s.auth(), from, []string{to}, []byte(body))
}

func (s SMTPTransport) auth() smtp.Auth {
	return smtp.PlainAuth("", s.User, s.Password, s.Host)
}

type StdoutTransport struct{}

func (s StdoutTransport) Send(from, to, msg string) error {
	fmt.Println("---------- sending email ----------")
	fmt.Print(msg)
	fmt.Println("-----------------------------------")
	return nil
}

type NulTransport struct{}

func (s NulTransport) Send(_, _, _ string) error {
	return nil
}
