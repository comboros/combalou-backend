package visitor

import (
	"context"
	"fmt"
)

type PaymentID int

type Payment struct {
	ID        PaymentID
	VisitorID ID
	Mode      PaymentMode
	Amount    float64
}

type PaymentRepository interface {
	Save(context.Context, *Payment) error
	DeleteByVisitor(context.Context, ID) error

	FindByVisitor(context.Context, ID) (*Payment, error)
}

type PaymentMode int

const (
	Check PaymentMode = iota
	Transfer
	Credit
)

func StringToPaymentMode(s string) (PaymentMode, error) {
	switch s {
	case "check":
		return Check, nil
	case "transfer":
		return Transfer, nil
	case "credit":
		return Credit, nil
	default:
		return 0, fmt.Errorf("invalid payment mode %v", s)
	}
}

func (t PaymentMode) String() string {
	switch t {
	case Transfer:
		return "transfer"
	case Credit:
		return "credit"
	default:
		return "check"
	}
}
