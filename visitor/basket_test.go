package visitor_test

import (
	"errors"
	"testing"

	"framagit.org/comboros/combalou/helper"
	"framagit.org/comboros/combalou/ticket"
	"framagit.org/comboros/combalou/visitor"
	"github.com/google/go-cmp/cmp"
)

func TestAddPurchase(t *testing.T) {
	tickets := helper.TicketsByID(t)

	tests := map[string]struct {
		givePurchases map[ticket.ID]int
		giveTicket    ticket.Ticket
		giveQuantity  int
		want          map[ticket.ID]int
		wantErr       error
	}{
		"empty basket": {
			givePurchases: map[ticket.ID]int{},
			giveTicket:    tickets[0],
			giveQuantity:  1,
			want: map[ticket.ID]int{
				0: 1,
			},
		},
		"article not yet purchased": {
			givePurchases: map[ticket.ID]int{
				0: 3,
				1: 2,
				3: 1,
			},
			giveTicket:   tickets[2],
			giveQuantity: 1,
			want: map[ticket.ID]int{
				0: 3,
				1: 2,
				2: 1,
				3: 1,
			},
		},
		"zero quantity": {
			givePurchases: map[ticket.ID]int{0: 1},
			giveTicket:    tickets[2],
			giveQuantity:  0,
			want: map[ticket.ID]int{
				0: 1,
			},
		},
		"article already purchased in the same quantity": {
			givePurchases: map[ticket.ID]int{
				0: 2,
				2: 1,
				3: 2,
			},
			giveTicket:   tickets[2],
			giveQuantity: 1,
			want: map[ticket.ID]int{
				0: 2,
				2: 1,
				3: 2,
			},
		},
		"article already purchased in different quantity": {
			givePurchases: map[ticket.ID]int{
				0: 2,
				2: 1,
				3: 2,
			},
			giveTicket:   tickets[2],
			giveQuantity: 2,
			wantErr: visitor.ErrAlreadyPurchased{
				Article:          tickets[2],
				CurrentQuantity:  1,
				DemandedQuantity: 2,
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			b := visitor.Basket{
				Purchases: tt.givePurchases,
			}

			err := b.AddPurchase(tt.giveTicket, tt.giveQuantity)

			if err != nil {
				if tt.wantErr == nil {
					t.Fatalf("Basket.AddPurchase(): %v", err)
				} else if diff := cmp.Diff(tt.wantErr, err); diff != "" {
					t.Errorf("Basket.AddPurchase() failed improperly:\n%s", diff)
				}
				return
			}
			if tt.wantErr != nil {
				t.Fatal("Basket.AddPurchase() was supposed to fail but did not")
			}

			if diff := cmp.Diff(tt.want, b.Purchases); diff != "" {
				t.Errorf("Basket.AddPurchase() set Basket to an unexpected state:\n%s", diff)
			}
		})
	}
}

func TestCancelPurchase(t *testing.T) {
	tickets := helper.Tickets(t)

	tests := map[string]struct {
		givePurchases map[ticket.ID]int
		giveTicket    ticket.Ticket
		want          map[ticket.ID]int
		wantErr       error
	}{
		"empty basket": {
			givePurchases: map[ticket.ID]int{},
			giveTicket:    tickets[2],
			wantErr:       visitor.ErrNotFound,
		},
		"article not purchased": {
			givePurchases: map[ticket.ID]int{
				0: 3,
				1: 2,
				3: 1,
			},
			giveTicket: tickets[2],
			wantErr:    visitor.ErrNotFound,
		},
		"article purchased": {
			givePurchases: map[ticket.ID]int{
				0: 3,
				1: 2,
				2: 1,
				3: 1,
			},
			giveTicket: tickets[2],
			want: map[ticket.ID]int{
				0: 3,
				1: 2,
				3: 1,
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			b := visitor.Basket{
				Purchases: tt.givePurchases,
			}

			err := b.CancelPurchase(tt.giveTicket)

			if err != nil {
				if tt.wantErr == nil {
					t.Fatalf("Basket.CancelPurchase(): %v", err)
				} else if !errors.Is(err, tt.wantErr) {
					t.Errorf("Basket.CancelPurchase() failed improperly:\n\tgot %+v ; expected %+v", err, tt.wantErr)
				}
				return
			}
			if tt.wantErr != nil {
				t.Fatalf("Basket.CancelPurchase() was expected to fail, but did not")
			}

			if diff := cmp.Diff(tt.want, b.Purchases); diff != "" {
				t.Errorf("Basket.CancelPurchase() set Basket to an unexpected state:\n%s", diff)
			}
		})
	}
}
