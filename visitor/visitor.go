package visitor

import (
	"context"
	"strings"
	"time"
)

type Visitor struct {
	ID            ID
	Surname, Name string
	Phone, Email  string
	Address       Address
	Bookings      Bookings
	Billed        time.Time
	// Deprecated fields
	Responsible string      // name of the responsible adult
	PaymentMode PaymentMode // preselected payment mode
}

func (v Visitor) Matches(term string) bool {
	candidates := []string{
		v.Name,
		v.Surname,
		v.Responsible,
	}
	t := strings.ToLower(term)
	for _, str := range candidates {
		if strings.Contains(strings.ToLower(str), t) {
			return true
		}
	}
	return false
}

type ID int

type Address struct {
	ID                                      int
	Line1, Line2, PostalCode, City, Country string
}

type Repository interface {
	// Save persists a visitor, and updates its ID accordingly.
	Save(context.Context, *Visitor) error

	// Find returns a visitor if it exists, ErrNotFound otherwise.
	Find(context.Context, ID) (*Visitor, error)

	// All produces the set of all visitors.
	All(context.Context) (map[ID]Visitor, error)

	Update(context.Context, Visitor) error

	Delete(context.Context, ID) error
}
