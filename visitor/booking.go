package visitor

import (
	"context"
	"fmt"
	"sort"
	"time"

	"framagit.org/comboros/combalou/workshop"
)

type BookingRepository interface {
	CountByWorkshop(context.Context) (map[workshop.ID]int, error)
	FindByVisitor(context.Context, ID) (Bookings, error)

	Add(context.Context, ID, workshop.ID) error
	Remove(context.Context, ID, workshop.ID) error
}

type Bookings map[workshop.ID]bool

func (b Bookings) WorkshopsBooked() int {
	return len(b)
}

func (b Bookings) Conflicts(workshops map[workshop.ID]workshop.Workshop) error {
	// Collect the booked workshops
	booked := make([]workshop.Workshop, 0, len(b))
	for id := range b {
		booked = append(booked, workshops[id])
	}

	// Sort them, to ensure the same input always gives the same results
	sort.Slice(booked, func(i, j int) bool {
		return booked[i].ID < booked[j].ID
	})

	// For each booked workshops, check if there’s another booked workshop
	// that conflicts with it.
	for n, w := range booked {
		for i := 0; i < n; i++ {
			if booked[i].Conflicts(w) {
				return WorkshopConflict{booked[i], w}
			}
		}
	}

	return nil
}

type WorkshopConflict struct {
	X workshop.Workshop
	Y workshop.Workshop
}

func (err WorkshopConflict) Error() string {
	return fmt.Sprintf("workshops %v – %s (%s) and %v – %s (%s) conflicts",
		err.X.ID, err.X.Name, err.X.Start.Format(time.RFC3339),
		err.Y.ID, err.Y.Name, err.Y.Start.Format(time.RFC3339))
}
