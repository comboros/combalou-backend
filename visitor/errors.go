package visitor

import (
	"errors"
	"fmt"

	"framagit.org/comboros/combalou/ticket"
)

var ErrNotFound = errors.New("not found")

type ErrAlreadyPurchased struct {
	Article          ticket.Ticket
	CurrentQuantity  int
	DemandedQuantity int
}

func (e ErrAlreadyPurchased) Error() string {
	return fmt.Sprintf("basket: ticket %v has already been purchased", e.Article.ID)
}
