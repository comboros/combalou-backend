package visitor

import (
	"context"
	"fmt"

	"framagit.org/comboros/combalou/ticket"
)

type BasketRepository interface {
	FindByVisitor(context.Context, ID) (*Basket, error)

	Save(context.Context, *Basket) error
	Delete(context.Context, *Basket) error
}

type BasketID int

type Basket struct {
	ID        BasketID
	VisitorID ID
	Purchases map[ticket.ID]int
	Rate      ticket.Rate
}

// AddPurchase adds an article to the basket in the requested quantity.
// If that article has already been purchased in a different quantity, an error
// of type ErrAlreadyPurchased is returned.
// If it has already been purchased in the same quantity, or if quantity is
// nul, do nothing.
func (b *Basket) AddPurchase(article ticket.Ticket, quantity int) error {
	if quantity == 0 {
		return nil
	}

	q, ok := b.Purchases[article.ID]
	if ok && q == quantity {
		return nil
	} else if ok {
		return ErrAlreadyPurchased{
			Article:          article,
			CurrentQuantity:  q,
			DemandedQuantity: quantity,
		}
	}

	b.Purchases[article.ID] = quantity

	return nil
}

// CancelPurchase removes an article from the basket.
// If that article has not been purchased, ErrNotFound is returned.
func (b *Basket) CancelPurchase(article ticket.Ticket) error {
	_, ok := b.Purchases[article.ID]
	if !ok {
		return fmt.Errorf("basket: %w", ErrNotFound)
	}

	delete(b.Purchases, article.ID)

	return nil
}

func (b Basket) WorkshopsBought(tickets map[ticket.ID]ticket.Ticket) int {
	var result int

	for tid, qt := range b.Purchases {
		result += tickets[tid].WorkshopCount * qt
	}

	return result
}

func (b Basket) Price(tickets map[ticket.ID]ticket.Ticket) float64 {
	var result float64

	for tid, qt := range b.Purchases {
		result += tickets[tid].Prices[b.Rate] * float64(qt)
	}

	return result
}
