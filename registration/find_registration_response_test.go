package registration

import (
	"testing"
	"time"

	"framagit.org/comboros/combalou/helper"
	"framagit.org/comboros/combalou/ticket"
	"framagit.org/comboros/combalou/visitor"
	"framagit.org/comboros/combalou/workshop"
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
)

func TestNewFindRegistrationResponse(t *testing.T) {
	var (
		monday       = time.Date(2019, 12, 16, 10, 00, 00, 00, time.UTC)
		tuesday      = time.Date(2019, 12, 17, 10, 00, 00, 00, time.UTC)
		wednesdayAft = time.Date(2019, 12, 18, 12, 00, 00, 00, time.UTC)
		now          = time.Now()
	)
	var (
		tickets   = helper.Tickets(t)
		workshops = map[workshop.ID]workshop.Workshop{
			1: workshop.Workshop{
				ID:       1,
				Start:    monday,
				Duration: 2 * time.Hour,
				Category: workshop.Danse,
			},
			3: workshop.Workshop{
				ID:       3,
				Start:    tuesday,
				Duration: 6 * time.Hour,
				Category: workshop.Music,
			},
			4: workshop.Workshop{
				ID:       4,
				Start:    wednesdayAft,
				Duration: 2 * time.Hour,
				Category: workshop.Craft,
			},
		}
	)
	giveVisitor := visitor.Visitor{
		ID:      visitor.ID(15),
		Surname: "Louise",
		Name:    "Michel",
		Phone:   "08 36 65 65 65",
		Email:   "louise.michel@example.com",
		Address: visitor.Address{
			Line1: "10, rue des Postes",
		},
		Bookings:    visitor.Bookings{1: true, 3: true, 4: true},
		Responsible: "Louise Michel", // she is responsible for herself anyway!
		PaymentMode: visitor.Check,
		Billed:      now,
	}
	giveBasket := visitor.Basket{
		Rate:      ticket.Reduced,
		Purchases: map[ticket.ID]int{1: 2, 3: 4},
	}
	want := findRegistrationResponse{
		ID:          15,
		Surname:     "Louise",
		Name:        "Michel",
		Responsible: "Louise Michel",
		Phone:       "08 36 65 65 65",
		Email:       "louise.michel@example.com",
		Line1:       "10, rue des Postes",
		Purchases: []purchaseDTO{
			{
				id:           1,
				Quantity:     2,
				Name:         tickets[1].Name,
				Price:        tickets[1].Prices[ticket.Full],
				ReducedPrice: tickets[1].Prices[ticket.Reduced],
			},
			{
				id:           3,
				Quantity:     4,
				Name:         tickets[3].Name,
				Price:        tickets[3].Prices[ticket.Full],
				ReducedPrice: tickets[3].Prices[ticket.Reduced],
			},
		},
		Workshops: []workshopDTO{
			{
				ID:       1,
				Category: "danse",
				Day:      "monday",
				Moment:   "morning",
			},
			{
				ID:       3,
				Category: "instrument",
				Day:      "tuesday",
				Moment:   "morning",
			},
			{
				ID:       4,
				Category: "autre",
				Day:      "wednesday",
				Moment:   "afternoon",
			},
		},
		PaymentMode: "check",
		Rate:        "reduced",
		LastBilling: now.Unix(),
		State:       stateUnpaid,
	}
	got := newFindRegistrationResponse(giveVisitor, giveBasket, stateUnpaid, helper.TicketsByID(t), workshops)
	opts := cmpopts.IgnoreUnexported(purchaseDTO{}) // ignore purchaseDTO.id
	if diff := cmp.Diff(want, got, opts); diff != "" {
		t.Errorf("Registration don’t match (-want +got):\n%s", diff)
	}
}
