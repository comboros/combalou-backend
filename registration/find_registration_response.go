package registration

import (
	"sort"

	"framagit.org/comboros/combalou/ticket"
	"framagit.org/comboros/combalou/visitor"
	"framagit.org/comboros/combalou/workshop"
)

type findRegistrationResponse struct {
	ID int `json:"id"`

	Surname     string `json:"surname"`
	Name        string `json:"name"`
	Responsible string `json:"responsible,omitempty"`

	Phone string `json:"phone"`
	Email string `json:"email"`

	Line1      string `json:"line1"`
	Line2      string `json:"line2"`
	PostalCode string `json:"postalCode"`
	City       string `json:"city"`
	Country    string `json:"country"`

	Purchases   []purchaseDTO `json:"basket"`
	Workshops   []workshopDTO `json:"workshops"`
	PaymentMode string        `json:"paymentType"`
	Rate        string        `json:"priceType"`
	LastBilling int64         `json:"lastBilling"` // timestamp
	State       string        `json:"state"`
}

const (
	stateUnpaid       = "unpaid"
	statePaid         = "paid"
	stateInconsistent = "inconsistent"
)

func newFindRegistrationResponse(v visitor.Visitor, basket visitor.Basket, state string,
	tickets map[ticket.ID]ticket.Ticket, workshops map[workshop.ID]workshop.Workshop) findRegistrationResponse {
	var r findRegistrationResponse

	r.ID = int(v.ID)

	// civil state
	r.Surname = v.Surname
	r.Name = v.Name
	r.Responsible = v.Responsible

	// contact
	r.Phone = v.Phone
	r.Email = v.Email

	// address
	r.Line1 = v.Address.Line1
	r.Line2 = v.Address.Line2
	r.PostalCode = v.Address.PostalCode
	r.City = v.Address.City
	r.Country = v.Address.Country

	// payment
	r.LastBilling = v.Billed.Unix()
	r.PaymentMode = v.PaymentMode.String()
	r.Rate = basket.Rate.String()
	r.State = state

	// basket
	r.Purchases = make([]purchaseDTO, 0, len(basket.Purchases))
	for tid, qt := range basket.Purchases {
		t := tickets[tid]
		r.Purchases = append(r.Purchases, purchaseDTO{
			id:           int(tid),
			Quantity:     qt,
			Name:         t.Name,
			Price:        t.Prices[ticket.Full],
			ReducedPrice: t.Prices[ticket.Reduced],
		})
	}
	sort.Slice(r.Purchases, func(i, j int) bool {
		return r.Purchases[i].id < r.Purchases[j].id
	})

	// bookings
	r.Workshops = make([]workshopDTO, 0, len(v.Bookings))
	for id := range v.Bookings {
		var dto workshopDTO
		dto.FromWorkshop(workshops[id])
		r.Workshops = append(r.Workshops, dto)
	}
	sort.Slice(r.Workshops, func(i, j int) bool {
		return r.Workshops[i].ID < r.Workshops[j].ID
	})

	return r
}

type purchaseDTO struct {
	id           int     // unexported, used to sort tickets
	Quantity     int     `json:"quantity"`
	Name         string  `json:"name"`
	Price        float64 `json:"price"`
	ReducedPrice float64 `json:"reducedPrice"`
}
