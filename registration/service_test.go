package registration

import (
	"context"
	"errors"
	"fmt"
	"math/rand"
	"os"
	"regexp"
	"testing"
	"time"

	"framagit.org/comboros/combalou/email"
	"framagit.org/comboros/combalou/helper"
	"framagit.org/comboros/combalou/inmem"
	"framagit.org/comboros/combalou/templates"
	"framagit.org/comboros/combalou/ticket"
	"framagit.org/comboros/combalou/visitor"
	"framagit.org/comboros/combalou/workshop"
	"github.com/google/go-cmp/cmp"
)

func TestMain(m *testing.M) {
	seed := time.Now().UnixNano()
	rand.Seed(seed)
	fmt.Printf("Seed: %d\n", seed)
	os.Exit(m.Run())
}

var reftickets = map[ticket.ID]ticket.Ticket{
	0: {ID: 0, WorkshopCount: 0},
	1: {ID: 1, WorkshopCount: 1},
	2: {ID: 2, WorkshopCount: 2},
}

var reftimes = map[time.Weekday]time.Time{
	time.Thursday: time.Date(2019, time.August, 1, 10, 0, 0, 0, time.UTC),
	time.Friday:   time.Date(2019, time.August, 2, 10, 0, 0, 0, time.UTC),
	time.Saturday: time.Date(2019, time.August, 3, 10, 0, 0, 0, time.UTC),
}

var twohours, _ = time.ParseDuration("2h")

var registerTests = map[string]struct {
	input     registerRequest
	workshops map[workshop.ID]workshop.Workshop
	bookings  map[workshop.ID]int // number of bookings per workshop

	// err covers at least three expectations:
	// * has the operation been successful?
	// * is the returned error of the right type?
	// * does the returned error’s message match the expected one?
	// The expected message is a treated as a regexp, so don’t forget to
	// quote meta characters (with regexp.QuoteMeta).
	err error
}{
	"Empty": {
		registerRequest{},
		nil,
		nil,

		ErrValidation{`Validation error in field "Surname" of type "string" using validator "empty=false"`},
	},
	"NoPurchases": {
		registration(
			[]purchase{},
			[]workshop.ID{},
		),
		nil,
		nil,

		ErrValidation{
			regexp.QuoteMeta(
				`Validation error in field "Purchases" of type "[]registration.purchase" using validator "empty=false"`,
			),
		},
	},
	"NoBookings": {
		registration(
			[]purchase{{0, 1}},
			[]workshop.ID{},
		),
		nil,
		nil,

		nil,
	},
	"UnknownTickets": {
		registration(
			[]purchase{{0, 1}, {42, 2}, {1, 1}},
			[]workshop.ID{},
		),
		nil,
		nil,

		ErrValidation{"ticket 42: not found"},
	},
	"UnknownWorkshops": {
		registration(
			[]purchase{{1, 3}},
			[]workshop.ID{0},
		),
		nil,
		nil,

		ErrValidation{"unknown workshop 0"},
	},
	"WorkshopThatIntersect": {
		registration(
			[]purchase{{1, 3}},
			[]workshop.ID{0, 1},
		),
		map[workshop.ID]workshop.Workshop{
			0: {Name: "foo", Start: reftimes[time.Friday], Duration: twohours, Gauge: 10},
			1: {Name: "bar", Start: reftimes[time.Friday], Duration: twohours, Gauge: 10},
		},
		nil,

		ErrValidation{"workshops [01] – [fobar]+ \\(2019-08-02T10:00:00Z\\) and [01] – [fobar]+ \\(2019-08-02T10:00:00Z\\) conflicts"},
	},
	"NoWorkshopsAllowed": {
		registration(
			[]purchase{{0, 1}},
			[]workshop.ID{0},
		),
		map[workshop.ID]workshop.Workshop{
			0: {Start: reftimes[time.Friday], Duration: twohours, Gauge: 10},
		},
		nil,

		ErrValidation{"wrong number of workshops booked: 1 \\(0 bought\\)"},
	},
	"NotEnoughWorkshops": {
		registration(
			[]purchase{{1, 2}, {2, 1}},
			[]workshop.ID{0, 1},
		),
		map[workshop.ID]workshop.Workshop{
			0: {Start: reftimes[time.Thursday], Duration: twohours, Gauge: 10},
			1: {Start: reftimes[time.Friday], Duration: twohours, Gauge: 10},
		},
		nil,

		ErrValidation{"wrong number of workshops booked: 2 \\(4 bought\\)"},
	},
	"TooMuchWorkshops": {
		registration(
			[]purchase{{2, 1}},
			[]workshop.ID{0, 1, 2},
		),
		map[workshop.ID]workshop.Workshop{
			0: {Start: reftimes[time.Thursday], Duration: twohours, Gauge: 10},
			1: {Start: reftimes[time.Friday], Duration: twohours, Gauge: 10},
			2: {Start: reftimes[time.Saturday], Duration: twohours, Gauge: 10},
		},
		nil,

		ErrValidation{"wrong number of workshops booked: 3 \\(2 bought\\)"},
	},
	"WorkshopWithoutGauge": {
		registration(
			[]purchase{{0, 1}},
			[]workshop.ID{0},
		),
		map[workshop.ID]workshop.Workshop{
			0: {Start: reftimes[time.Thursday], Duration: twohours, Gauge: 0},
		},
		nil,

		ErrValidation{"no room left in workshop 0"},
	},
	"WorkshopWithoutRoomLeft": {
		registration(
			[]purchase{{0, 1}},
			[]workshop.ID{0},
		),
		map[workshop.ID]workshop.Workshop{
			0: {Start: reftimes[time.Thursday], Duration: twohours, Gauge: 10},
		},
		map[workshop.ID]int{
			0: 10,
		},

		ErrValidation{"no room left in workshop 0"},
	},
	"AllGood": {
		registration(
			[]purchase{{2, 1}, {1, 1}, {0, 1}},
			[]workshop.ID{0, 1, 2},
		),
		map[workshop.ID]workshop.Workshop{
			0: {Start: reftimes[time.Thursday], Duration: twohours, Gauge: 10},
			1: {Start: reftimes[time.Friday], Duration: twohours, Gauge: 10},
			2: {Start: reftimes[time.Saturday], Duration: twohours, Gauge: 10},
		},
		nil,

		nil,
	},
}

func TestRegister(t *testing.T) {
	for tcname, tc := range registerTests {
		t.Run(tcname, func(t *testing.T) {
			tickets := &StubTicketRepository{}
			workshops := &StubWorkshopRepository{
				t: t,
				allByID: func(_ []workshop.ID) (map[workshop.ID]workshop.Workshop, error) {
					return tc.workshops, nil
				},
			}
			baskets := &StubBasketRepository{}
			visitors := &StubVisitorRepository{}
			bookings := &StubBookingRepository{bookings: tc.bookings}
			emailSender := &StubEmailSender{}
			payments := inmem.NewPaymentRepository()

			s := NewService(visitors, tickets, workshops, baskets, bookings, payments, emailSender)
			_, err := s.Register(context.Background(), tc.input)

			if tc.err == nil { // success expected
				if err != nil {
					t.Errorf("No errors were expected, but got `%v`", err)
				}
				if visitors.calls != 1 {
					t.Errorf("%d visitors saved; expected 1", visitors.calls)
				}
				if emailSender.calls != 1 {
					t.Errorf("%d invoices sent; expected 1", emailSender.calls)
				}
			} else { // error expected
				pattern := regexp.MustCompile(tc.err.Error())
				if !pattern.MatchString(err.Error()) {
					t.Errorf("Got error `%v`; expected it to match `%s`", err, tc.err)
				}
			}
		})
	}
}

// Find was too complicated to test with a table-driven test

func TestFindUnknownVisitor(t *testing.T) {
	deps := makeDependencies(t)
	ctx := context.Background()

	_, err := deps.service.Find(ctx, visitor.ID(42))

	if !errors.Is(err, visitor.ErrNotFound) {
		t.Errorf("Find Alice: %v (expected \"not found\")", err)
	}
}

func TestFindVisitorWithoutBasket(t *testing.T) {
	deps := makeDependencies(t)
	ctx := context.Background()

	alice := helper.Alice(t)
	alice.SaveVisitor(t, deps.visitors)

	_, err := deps.service.Find(ctx, alice.Visitor.ID)

	if !errors.Is(err, visitor.ErrNotFound) {
		t.Errorf("Find Alice: %v (expected \"not found\")", err)
	}
}

func TestFindVisitorWithoutBookings(t *testing.T) {
	deps := makeDependencies(t)
	ctx := context.Background()

	// martine has no bookings
	martine := helper.Martine(t)
	martine.SaveVisitor(t, deps.visitors)
	martine.SaveBasket(t, deps.baskets)

	got, err := deps.service.Find(ctx, martine.Visitor.ID)
	if err != nil {
		t.Fatalf("Find Martine: %v", err)
	}
	if len(got.Workshops) != 0 {
		t.Errorf("Got %d workshops (expected 0):\n%#v", len(got.Workshops), got.Workshops)
	}
}

func TestFindVisitorWithoutPayment(t *testing.T) {
	deps := makeDependencies(t)
	ctx := context.Background()

	alice := helper.Alice(t)
	alice.SaveVisitor(t, deps.visitors)
	alice.SaveBasket(t, deps.baskets)

	got, err := deps.service.Find(ctx, alice.Visitor.ID)
	if err != nil {
		t.Fatalf("Find Alice: %v", err)
	}
	if got.State != "unpaid" {
		t.Errorf("Found Alice with state %q (expected \"unpaid\")", got.State)
	}
}

func TestFindVisitorWithPayment(t *testing.T) {
	deps := makeDependencies(t)
	ctx := context.Background()

	alice := helper.Alice(t)
	alice.SaveVisitor(t, deps.visitors)
	alice.SaveBasket(t, deps.baskets)
	price := alice.Basket.Price(helper.TicketsByID(t))
	pmt := visitor.Payment{
		VisitorID: alice.Visitor.ID,
		Amount:    price,
	}
	if err := deps.payments.Save(ctx, &pmt); err != nil {
		t.Fatalf("Saving Alice’s payment: %v", err)
	}

	got, err := deps.service.Find(ctx, alice.Visitor.ID)
	if err != nil {
		t.Fatalf("Find Alice: %v", err)
	}
	if got.State != "paid" {
		t.Errorf("Found Alice with state %q (expected \"paid\")", got.State)
	}
}

func TestFindVisitorWithTooMuchBookings(t *testing.T) {
	deps := makeDependencies(t)
	ctx := context.Background()

	martine := helper.Martine(t)
	martine.Visitor.Bookings = visitor.Bookings{1: true}
	martine.SaveVisitor(t, deps.visitors)
	martine.SaveBasket(t, deps.baskets)

	got, err := deps.service.Find(ctx, martine.Visitor.ID)
	if err != nil {
		t.Fatalf("Find Martine: %v", err)
	}
	if got.State != "inconsistent" {
		t.Errorf("Found Martine with state %q (expected \"inconsistent\")", got.State)
	}
}

func TestFindVisitorWithNotEnoughBookings(t *testing.T) {
	deps := makeDependencies(t)
	ctx := context.Background()

	alice := helper.Alice(t)
	delete(alice.Visitor.Bookings, 5)
	alice.SaveVisitor(t, deps.visitors)
	alice.SaveBasket(t, deps.baskets)

	got, err := deps.service.Find(ctx, alice.Visitor.ID)
	if err != nil {
		t.Fatalf("Find Alice: %v", err)
	}
	if got.State != "inconsistent" {
		t.Errorf("Found Alice with state %q (expected \"inconsistent\")", got.State)
	}
}

func TestUpdateVisitor(t *testing.T) {
	tests := map[string]struct {
		giveChange func(*visitor.Visitor)
		giveRate   ticket.Rate
		giveState  string
	}{
		"update phone": {
			giveChange: func(alice *visitor.Visitor) {
				alice.Phone = "12345"
			},
		},
		"update postal code": {
			giveChange: func(alice *visitor.Visitor) {
				alice.Address.PostalCode = "54321"
			},
		},
		"update rate": {
			giveRate: ticket.Reduced,
		},
		"update state": {
			giveState: statePaid,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			deps := makeDependencies(t)
			ctx := context.Background()

			alice := helper.Alice(t)
			alice.SaveVisitor(t, deps.visitors)
			alice.SaveBasket(t, deps.baskets)

			if tt.giveChange != nil {
				tt.giveChange(&alice.Visitor)
			}
			if tt.giveState == "" {
				tt.giveState = stateUnpaid
			}

			err := deps.service.UpdateVisitor(ctx, alice.Visitor, tt.giveRate, tt.giveState)
			if err != nil {
				t.Fatalf("Updating Alice: %v", err)
			}

			got, err := deps.visitors.Find(ctx, alice.Visitor.ID)
			if err != nil {
				t.Fatalf("Finding Alice: %v", err)
			}
			if diff := cmp.Diff(alice.Visitor, *got); err != nil {
				t.Errorf("Alice in DB is different than expected: (-want +got)\n%s", diff)
			}
		})
	}
}

func TestUpdateBasket(t *testing.T) {
	// Original basket: ticket.ID(1)×1 + ticket.ID(5)×2
	tests := map[string]struct {
		give    map[ticket.ID]int
		want    map[ticket.ID]int
		wantErr error
	}{
		"No change": {
			give: helper.Purchases(t, ticket.ID(1), 1, ticket.ID(5), 1),
		},
		"Change quantity": {
			give: helper.Purchases(t, ticket.ID(1), 1, ticket.ID(5), 2),
		},
		"Quantity zero": {
			give: helper.Purchases(t, ticket.ID(1), 1, ticket.ID(5), 0),
			want: helper.Purchases(t, ticket.ID(1), 1),
		},
		/*TODO: "Add ticket": {
			give: helper.Purchases(t, ticket.ID(1), 1, ticket.ID(5), 2),
		},*/
		"Remove ticket": {
			give: helper.Purchases(t, ticket.ID(1), 1),
		},
		"Remove all": {
			give:    helper.Purchases(t),
			wantErr: ErrEmptyBasket,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			deps := makeDependencies(t)
			ctx := context.Background()

			// Save Bob & its basket
			bob := helper.Bob(t)
			deps.visitors.Save(ctx, &bob.Visitor)
			bob.Basket.VisitorID = bob.Visitor.ID
			deps.baskets.Save(ctx, &bob.Basket)

			newBasket := visitor.Basket{
				VisitorID: bob.Visitor.ID,
				Purchases: tt.give,
			}
			err := deps.service.UpdateBasket(context.Background(), newBasket)
			if tt.wantErr != nil && errors.Is(err, tt.wantErr) {
				return
			} else if tt.wantErr != nil {
				t.Fatalf("UpdateBasket(bob) failed with %v (expected %q)", err, tt.wantErr)
			} else if err != nil {
				t.Fatalf("UpdateBasket(bob) failed: %v", err)
			}

			want := tt.want
			if want == nil {
				want = newBasket.Purchases
			}
			gotBasket, err := deps.baskets.FindByVisitor(context.Background(), bob.Visitor.ID)
			if err != nil {
				t.Fatalf("baskets.FindByVisitor(bob) failed: %v", err)
			}
			if diff := cmp.Diff(want, gotBasket.Purchases); diff != "" {
				t.Errorf("Basket in DB is different than expected: (-want, +got)\n%s", diff)
			}
		})
	}
}

func TestUpdateBookings(t *testing.T) {
	tests := map[string]struct {
		give    visitor.Bookings
		wantErr error
	}{
		"No change": {
			give: helper.Bookings(t, 0, 5, 12, 16, 22, 29, 35),
		},
		"Change workshop": {
			give: helper.Bookings(t, 0, 5, 12, 17, 22, 29, 35),
		},
		"Remove workshop": {
			give: helper.Bookings(t, 0, 5, 12, 16, 29, 35),
		},
		"Two workshops same moment": {
			give: helper.Bookings(t, 0, 5, 12, 16, 22, 29, 30),
			wantErr: visitor.WorkshopConflict{
				X: helper.Workshops(t)[29],
				Y: helper.Workshops(t)[30],
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			deps := makeDependencies(t)
			ctx := context.Background()

			alice := helper.Alice(t)
			deps.visitors.Save(ctx, &alice.Visitor)

			ids := map[workshop.ID]bool{}
			for id := range tt.give {
				ids[id] = true
			}
			err := deps.service.UpdateBooking(ctx, alice.Visitor.ID, ids)
			if tt.wantErr != nil && errors.Is(err, tt.wantErr) {
				return
			} else if tt.wantErr != nil {
				t.Fatalf("UpdateBooking failed with %q (expected %q)", err, tt.wantErr)
			} else if err != nil {
				t.Fatalf("UpdateBookings() failed: %v", err)
			}

			got, err := deps.bookings.FindByVisitor(ctx, alice.Visitor.ID)
			if err != nil {
				t.Fatalf("bookings.FindByVisitor(): %v", err)
			}
			if diff := cmp.Diff(tt.give, got); diff != "" {
				t.Errorf("Retrieved bookings don’t match (-want +got):\n%s", diff)
			}
		})
	}
}

func TestCreateMainPayment(t *testing.T) {
	alice := helper.Alice(t)

	tests := map[string]struct {
		giveVisitor         *visitor.Visitor
		giveBasket          *visitor.Basket
		withExistingPayment func(visitor.ID) visitor.Payment
		wantErr             error
	}{
		"No visitor": {
			wantErr: visitor.ErrNotFound,
		},
		"No basket": {
			giveVisitor: &alice.Visitor,
			wantErr:     visitor.ErrNotFound,
		},
		"Good path": {
			giveVisitor: &alice.Visitor,
			giveBasket:  &alice.Basket,
		},
		"Payment already there": {
			giveVisitor: &alice.Visitor,
			giveBasket:  &alice.Basket,
			withExistingPayment: func(vid visitor.ID) visitor.Payment {
				return visitor.Payment{
					VisitorID: vid,
					Amount:    99.99,
					Mode:      visitor.Check,
				}
			},
			wantErr: ErrPaymentAlreadyExists,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			deps := makeDependencies(t)
			ctx := context.Background()

			if tt.giveVisitor != nil {
				if err := deps.visitors.Save(ctx, tt.giveVisitor); err != nil {
					t.Fatalf("Save Alice: %v", err)
				}
			}
			if tt.giveBasket != nil {
				tt.giveBasket.VisitorID = tt.giveVisitor.ID
				if err := deps.baskets.Save(ctx, tt.giveBasket); err != nil {
					t.Fatalf("Save Alice’s basket: %v", err)
				}
			}
			if tt.withExistingPayment != nil {
				pmt := tt.withExistingPayment(tt.giveVisitor.ID)
				if err := deps.payments.Save(ctx, &pmt); err != nil {
					t.Fatalf("Save Alice’s existing payment: %v", err)
				}
			}

			// Create the payment
			err := deps.service.CreateMainPayment(ctx, alice.Visitor.ID)

			// Assertions
			if !errors.Is(err, tt.wantErr) {
				t.Errorf("Create Alice’s main payment: %v (expected %q)", err, tt.wantErr)
			}
			if err != nil {
				return
			}

			_, err = deps.payments.FindByVisitor(ctx, tt.giveVisitor.ID)
			if err != nil {
				t.Fatalf("Find Alice’s payment: %v", err)
			}
		})
	}
}

type StubEmailSender struct {
	calls int
}

func (i *StubEmailSender) SendInvoice(_ visitor.Visitor, _ visitor.Basket,
	_ map[ticket.ID]ticket.Ticket, _ map[workshop.ID]workshop.Workshop) error {
	i.calls++
	return nil
}

func (i *StubEmailSender) SendPaymentConfirmation(_ visitor.Visitor) error {
	return nil
}

type StubTicketRepository struct{}

func (r *StubTicketRepository) All(_ context.Context) (map[ticket.ID]ticket.Ticket, error) {
	return reftickets, nil
}

type StubWorkshopRepository struct {
	t       *testing.T
	allByID func([]workshop.ID) (map[workshop.ID]workshop.Workshop, error)
	all     func() (map[workshop.ID]workshop.Workshop, error)
}

func (r *StubWorkshopRepository) AllByID(_ context.Context, ids []workshop.ID) (map[workshop.ID]workshop.Workshop, error) {
	if r.allByID == nil {
		r.t.Errorf("Unexpected call to WorkshopRepository.AllByID(%v)", ids)
	}
	return r.allByID(ids)
}

func (r *StubWorkshopRepository) All(_ context.Context) (map[workshop.ID]workshop.Workshop, error) {
	if r.all == nil {
		r.t.Errorf("Unexpected call to workshops.All()")
	}
	return r.all()
}

type StubBasketRepository struct {
	visitor.BasketRepository
}

func (r *StubBasketRepository) Save(_ context.Context, bask *visitor.Basket) error {
	return nil
}

type StubBookingRepository struct {
	calls    int
	bookings map[workshop.ID]int
}

func (r *StubBookingRepository) CountByWorkshop(_ context.Context) (map[workshop.ID]int, error) {
	r.calls++
	return r.bookings, nil
}

func (r *StubBookingRepository) FindByVisitor(_ context.Context, _ visitor.ID) (visitor.Bookings, error) {
	panic("Unexpected call to bookings.FindByVisitor()")
}

func (r *StubBookingRepository) Add(_ context.Context, _ visitor.ID, _ workshop.ID) error {
	panic("Unexpected call to bookings.Add()")
}

func (r *StubBookingRepository) Remove(_ context.Context, _ visitor.ID, _ workshop.ID) error {
	panic("Unexpected call to bookings.Remove()")
}

type StubVisitorRepository struct {
	t     *testing.T
	calls int
	find  func(id visitor.ID) (*visitor.Visitor, error)
}

func (r *StubVisitorRepository) Save(_ context.Context, v *visitor.Visitor) error {
	v.ID = visitor.ID(rand.Int())
	r.calls++
	return nil
}

func (r *StubVisitorRepository) All(_ context.Context) (map[visitor.ID]visitor.Visitor, error) {
	panic("Unexpected call to visitors.All()")
}

func (r *StubVisitorRepository) Find(_ context.Context, id visitor.ID) (*visitor.Visitor, error) {
	if r.find == nil {
		r.t.Errorf("Unexpected call to visitors.Find(%v)", id)
	}
	return r.find(id)
}

func (r *StubVisitorRepository) Update(_ context.Context, _ visitor.Visitor) error {
	panic("Unexpected call to visitors.Update")
}

func (r *StubVisitorRepository) Delete(_ context.Context, _ visitor.ID) error {
	panic("Unexpected call to visitors.Delete")
}

func registration(purchases []purchase, workshops []workshop.ID) registerRequest {
	info := personalInfo{
		Surname: "Test surname",
		Name:    "Test name",
	}
	contact := contact{
		Phone: "01 02 03 04 05",
		Email: "test@example.com",
		Address: addressDTO{
			Line1:      "12, rue Bien Testée",
			PostalCode: "59000",
			City:       "Lille",
			Country:    "France",
		},
	}
	return registerRequest{
		Info:        info,
		Contact:     contact,
		Purchases:   purchases,
		Workshops:   workshops,
		PaymentMode: "transfer",
		Rate:        "reduced",
	}
}

type dependencies struct {
	visitors  visitor.Repository
	payments  visitor.PaymentRepository
	tickets   ticket.Repository
	workshops workshop.Repository
	baskets   visitor.BasketRepository
	bookings  visitor.BookingRepository
	service   *service
}

func makeDependencies(t *testing.T) dependencies {
	var (
		visitors  = inmem.NewVisitorRepository()
		payments  = inmem.NewPaymentRepository()
		tickets   = inmem.NewTicketRepository(helper.Tickets(t))
		workshops = inmem.NewWorkshopRepository(helper.Workshops(t))
		baskets   = inmem.NewBasketRepository(visitors, tickets)
		bookings  = inmem.NewBookingRepository(visitors, workshops)
		sender    = email.Sender{
			From:      "test@comboros.com",
			Transport: email.NulTransport{},
			Tmpl:      templates.New("test@comboros.com"),
		}
		srv = NewService(visitors, tickets, workshops, baskets, bookings, payments, sender)
	)
	return dependencies{
		visitors,
		payments,
		tickets,
		workshops,
		baskets,
		bookings,
		srv.(*service),
	}
}
