package registration

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"framagit.org/comboros/combalou/ticket"
	"framagit.org/comboros/combalou/visitor"
	"framagit.org/comboros/combalou/workshop"
	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

func MakeHandler(s Service, logger kitlog.Logger) http.Handler {
	search := kithttp.NewServer(
		makeSearchEndpoint(s),
		decodeSearchRequest,
		encodeSearchResponse,
	)
	register := kithttp.NewServer(
		makeRegisterEndpoint(s),
		decodeRegisterRequest,
		encodeRegisterResponse,
	)
	find := kithttp.NewServer(
		makeFindEndpoint(s),
		decodeFindRequest,
		encodeFindResponse,
	)
	updateVisitor := kithttp.NewServer(
		makeUpdateVisitorEndpoint(s),
		decodeUpdateVisitorRequest,
		encodeUpdateVisitorResponse,
	)
	updateBasket := kithttp.NewServer(
		makeUpdateBasketEndpoint(s),
		decodeUpdateBasketRequest,
		encodeUpdateBasketResponse,
	)
	updateBooking := kithttp.NewServer(
		makeUpdateBookingEndpoint(s),
		decodeUpdateBookingRequest,
		encodeUpdateBookingResponse,
	)
	deleteVisitor := kithttp.NewServer(
		makeDeleteVisitorEndpoint(s),
		decodeDeleteVisitorRequest,
		encodeDeleteVisitorResponse,
	)
	createMainPayment := kithttp.NewServer(
		makeCreateMainPaymentEndpoint(s),
		decodeCreateMainPaymentRequest,
		encodeCreateMainPaymentResponse,
	)
	invoice := kithttp.NewServer(
		makeInvoiceEndpoint(s),
		decodeInvoiceRequest,
		encodeInvoiceResponse,
	)
	workshops := kithttp.NewServer(
		makeWorkshopsEndpoint(s),
		decodeWorkshopsRequest,
		encodeWorkshopsResponse,
	)
	tickets := kithttp.NewServer(
		makeTicketsEndpoint(s),
		decodeTicketsRequest,
		encodeTicketsResponse,
	)

	r := mux.NewRouter()
	r.Handle("/registrations", search).Methods(http.MethodGet)
	r.Handle("/registrations", register).Methods(http.MethodPost)
	r.Handle("/registration/{id}", find).Methods(http.MethodGet)
	r.Handle("/registration/{id}", deleteVisitor).Methods(http.MethodDelete)
	r.Handle("/visitor/{id}", updateVisitor).Methods(http.MethodPut)
	r.Handle("/visitor/{id}/basket", updateBasket).Methods(http.MethodPut)
	r.Handle("/visitor/{id}/booking", updateBooking).Methods(http.MethodPut)
	r.Handle("/visitor/{id}/payment", createMainPayment).Methods(http.MethodPost)
	r.Handle("/visitor/{id}/invoice", invoice).Methods(http.MethodPost)

	r.Handle("/workshops", workshops).Methods(http.MethodGet)
	r.Handle("/tickets", tickets).Methods(http.MethodGet)
	return r
}

// For internal errors, we return a generic message to avoid leaking
// the error itself, as it could potentially include sensitive information.
const (
	_msgRegistrationInternalError = `
Something went wrong on our side!
It is likely your registration hasn’t been taken into account.
Please contact us for more information.
`
	_msgInternalError = `
Something went wrong on our side!
It is likely the operation you asked hasn’t been taken into account.
Please contact us for more information.
`
)

func decodeSearchRequest(_ context.Context, req *http.Request) (interface{}, error) {
	term := req.URL.Query().Get("term")
	return searchRequest(term), nil
}

func encodeSearchResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	r := response.(searchResponse)
	if r.Err == nil && len(r.Results) > 0 {
		return json.NewEncoder(w).Encode(r.Results)
	} else if r.Err == nil {
		// return `[]` (and not `null`)
		return json.NewEncoder(w).Encode([]searchResult{})
	} else if _, ok := r.Err.(ErrValidation); ok {
		http.Error(w, r.Err.Error(), http.StatusBadRequest)
	} else {
		http.Error(w, _msgRegistrationInternalError, http.StatusInternalServerError)
	}
	return nil
}

func decodeRegisterRequest(_ context.Context, req *http.Request) (interface{}, error) {
	var r registerRequest
	err := json.NewDecoder(req.Body).Decode(&r)
	if err != nil {
		return nil, err
	}
	return r, nil
}

// TODO: Define a JSON representation for Bad Requests
func encodeRegisterResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	r := response.(registerResponse)
	if r.Err == nil {
		return json.NewEncoder(w).Encode(r.ID)
	} else if _, ok := r.Err.(ErrValidation); ok {
		http.Error(w, r.Err.Error(), http.StatusBadRequest)
	} else {
		http.Error(w, _msgRegistrationInternalError, http.StatusInternalServerError)
	}
	return nil
}

func decodeFindRequest(_ context.Context, req *http.Request) (interface{}, error) {
	return getVisitorID(req)
}

func encodeFindResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	r := response.(findResponse)
	if r.Err == nil {
		return json.NewEncoder(w).Encode(r.Info)
	} else if errors.Is(r.Err, visitor.ErrNotFound) {
		http.Error(w, r.Err.Error(), http.StatusNotFound)
	} else {
		http.Error(w, _msgRegistrationInternalError, http.StatusInternalServerError)
	}
	return nil
}

func decodeUpdateVisitorRequest(_ context.Context, req *http.Request) (interface{}, error) {
	id, err := getVisitorID(req)
	if err != nil {
		return nil, err
	}
	var v updateVisitorRequest
	err = json.NewDecoder(req.Body).Decode(&v)
	if err != nil {
		return nil, err
	}
	v.ID = visitor.ID(id)
	return v, nil
}

func encodeUpdateVisitorResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	r := response.(updateVisitorResponse)
	if r.Err == nil {
		w.WriteHeader(http.StatusNoContent)
	} else if errors.Is(r.Err, visitor.ErrNotFound) {
		http.Error(w, r.Err.Error(), http.StatusNotFound)
	} else {
		http.Error(w, _msgInternalError, http.StatusInternalServerError)
	}
	return nil
}

func decodeUpdateBasketRequest(_ context.Context, req *http.Request) (interface{}, error) {
	var request updateBasketRequest
	if err := json.NewDecoder(req.Body).Decode(&request.Purchases); err != nil {
		return nil, err
	}
	id, err := getVisitorID(req)
	if err != nil {
		return nil, err
	}
	request.VisitorID = id
	return request, nil
}

func encodeUpdateBasketResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	r := response.(updateBasketResponse)
	// TODO: handle not found
	if r.Err == nil {
		w.WriteHeader(http.StatusNoContent)
	} else if errors.Is(r.Err, visitor.ErrNotFound) {
		http.Error(w, r.Err.Error(), http.StatusNotFound)
	} else {
		http.Error(w, _msgInternalError, http.StatusInternalServerError)
	}
	return nil
}

func decodeUpdateBookingRequest(_ context.Context, req *http.Request) (interface{}, error) {
	vid, err := getVisitorID(req)
	if err != nil {
		return nil, err
	}

	var ids []workshop.ID
	err = json.NewDecoder(req.Body).Decode(&ids)
	if err != nil {
		return nil, err
	}

	books := map[workshop.ID]bool{}
	for _, id := range ids {
		if books[id] {
			return nil, fmt.Errorf("duplicate booking %v", id)
		}
		books[id] = true
	}
	return updateBookingRequest{
		VisitorID: vid,
		Bookings:  books,
	}, nil
}

func encodeUpdateBookingResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	r := response.(updateBookingResponse)
	if r.Err == nil {
		w.WriteHeader(http.StatusNoContent)
	} else if errors.Is(r.Err, visitor.ErrNotFound) {
		http.Error(w, r.Err.Error(), http.StatusNotFound)
	} else {
		http.Error(w, _msgInternalError, http.StatusInternalServerError)
	}
	return nil
}

func decodeDeleteVisitorRequest(_ context.Context, req *http.Request) (interface{}, error) {
	return getVisitorID(req)
}

func encodeDeleteVisitorResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	r := response.(deleteVisitorResponse)
	if r.Err == nil {
		w.WriteHeader(http.StatusNoContent)
	} else if errors.Is(r.Err, visitor.ErrNotFound) {
		http.Error(w, r.Err.Error(), http.StatusNotFound)
	} else {
		http.Error(w, _msgInternalError, http.StatusInternalServerError)
	}
	return nil
}

func decodeCreateMainPaymentRequest(_ context.Context, req *http.Request) (interface{}, error) {
	return getVisitorID(req)
}

func encodeCreateMainPaymentResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	r := response.(createMainPaymentResponse)
	if r.Err == nil {
		w.WriteHeader(http.StatusNoContent)
	} else if errors.Is(r.Err, visitor.ErrNotFound) {
		http.Error(w, r.Err.Error(), http.StatusNotFound)
	} else {
		http.Error(w, _msgInternalError, http.StatusInternalServerError)
	}
	return nil
}

func decodeInvoiceRequest(_ context.Context, req *http.Request) (interface{}, error) {
	return getVisitorID(req)
}

func encodeInvoiceResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	r := response.(invoiceResponse)
	if r.Err == nil {
		w.WriteHeader(http.StatusNoContent)
	} else if errors.Is(r.Err, visitor.ErrNotFound) {
		http.Error(w, r.Err.Error(), http.StatusNotFound)
	} else if errors.Is(r.Err, ErrPaymentAlreadyExists) {
		http.Error(w, r.Err.Error(), http.StatusBadRequest)
	} else {
		http.Error(w, _msgInternalError, http.StatusInternalServerError)
	}
	return nil
}

func decodeWorkshopsRequest(_ context.Context, _ *http.Request) (interface{}, error) {
	return nil, nil
}

func encodeWorkshopsResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	r := response.(workshopsResponse)
	result := make([]workshopDTO, len(r.Workshops))
	for i, w := range r.Workshops {
		w.Gauge = r.Places[w.ID]
		var dto workshopDTO
		dto.FromWorkshop(w)
		result[i] = dto
	}
	return json.NewEncoder(w).Encode(result)
}

func decodeTicketsRequest(_ context.Context, _ *http.Request) (interface{}, error) {
	return nil, nil
}

type TicketDTO struct {
	ID           int     `json:"id"`
	Name         string  `json:"name"`
	Description  string  `json:"description"`
	Price        float64 `json:"price"`
	ReducedPrice float64 `json:"reducedPrice"`
	Category     string  `json:"span"`
	Workshops    int     `json:"workshops"`
}

func encodeTicketsResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	r := response.(ticketsResponse)
	result := make([]TicketDTO, len(r.Tickets))
	for i, t := range r.Tickets {
		result[i] = TicketDTO{
			ID:           int(t.ID),
			Name:         t.Name,
			Description:  t.Description,
			Price:        t.Prices[ticket.Full],
			ReducedPrice: t.Prices[ticket.Reduced],
			Category:     t.Category.String(),
			Workshops:    t.WorkshopCount,
		}
	}
	return json.NewEncoder(w).Encode(result)
}

func getVisitorID(req *http.Request) (visitor.ID, error) {
	str, ok := mux.Vars(req)["id"]
	if !ok {
		return 0, errors.New("id is mandatory")
	}
	id, err := strconv.Atoi(str)
	if err != nil {
		return 0, errors.New("id must be a valid visitor id")
	}
	return visitor.ID(id), nil
}
