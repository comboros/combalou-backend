package registration

// TODO: check each of these DTOs is used at more than one place

import (
	"framagit.org/comboros/combalou/ticket"
	"framagit.org/comboros/combalou/workshop"
)

type personalInfo struct {
	Surname     string `json:"surname" validate:"empty=false"`
	Name        string `json:"name" validate:"empty=false"`
	Responsible string `json:"responsible,omitempty"`
}

type contact struct {
	Phone   string     `json:"phone" validate:"empty=false"`
	Email   string     `json:"email" validate:"empty=false & format=email"`
	Address addressDTO `json:"address"`
}

type addressDTO struct {
	Line1      string `json:"line1" validate:"empty=false"`
	Line2      string `json:"line2,omitempty"`
	PostalCode string `json:"postalCode" validate:"empty=false"`
	City       string `json:"city" validate:"empty=false"`
	Country    string `json:"country" validate:"empty=false"`
}

type purchase struct {
	Ticket   ticket.ID `json:"ticket"`
	Quantity int       `json:"quantity" validator:"gt=0"`
}

type workshopDTO struct {
	ID        workshop.ID `json:"id"`
	Name      string      `json:"name"`
	Band      string      `json:"band"`
	Category  string      `json:"category"`
	Day       string      `json:"day"`
	Moment    string      `json:"moment"`
	Remainder int         `json:"remainder"`
}

func (dto *workshopDTO) FromWorkshop(w workshop.Workshop) {
	*dto = workshopDTO{
		ID:        w.ID,
		Name:      w.Name,
		Band:      w.Band,
		Category:  categoryToString(w.Category),
		Day:       dayFromTime(w.Start),
		Moment:    momentFromTime(w.Start),
		Remainder: w.Gauge,
	}
}
