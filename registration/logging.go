package registration

import (
	"context"
	"time"

	"framagit.org/comboros/combalou/ticket"
	visitor "framagit.org/comboros/combalou/visitor"
	"framagit.org/comboros/combalou/workshop"
	"github.com/go-kit/kit/log"
)

type loggingService struct {
	logger log.Logger
	next   Service
}

func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) Register(ctx context.Context, r registerRequest) (id visitor.ID, err error) {
	defer func(begin time.Time) {
		var level string
		if err == nil {
			level = "info"
		} else if _, ok := err.(ErrValidation); ok {
			level = "warn"
		} else {
			level = "error"
		}
		s.logger.Log(
			"level", level,
			"method", "register",
			"visitor.id", id,
			"err", err,
			"latency", time.Since(begin),
		)
	}(time.Now())
	return s.next.Register(ctx, r)
}

func (s *loggingService) Find(ctx context.Context, id visitor.ID) (_ findRegistrationResponse, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"level", "info",
			"method", "find",
			"visitor.id", id,
			"err", err,
			"latency", time.Since(begin),
		)
	}(time.Now())
	return s.next.Find(ctx, id)
}

func (s *loggingService) Search(ctx context.Context, term string) (_ []searchResult, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"level", "info",
			"method", "search",
			"search.term", term,
			"err", err,
			"latency", time.Since(begin),
		)
	}(time.Now())
	return s.next.Search(ctx, term)
}

func (s *loggingService) UpdateVisitor(ctx context.Context, v visitor.Visitor, r ticket.Rate, st string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"level", "info",
			"method", "update visitor",
			"visitor.id", v.ID,
			"err", err,
			"latency", time.Since(begin),
		)
	}(time.Now())
	return s.next.UpdateVisitor(ctx, v, r, st)
}

func (s *loggingService) UpdateBasket(ctx context.Context, b visitor.Basket) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"level", "info",
			"method", "update basket",
			"visitor.id", b.VisitorID,
			"err", err,
			"latency", time.Since(begin),
		)
	}(time.Now())
	return s.next.UpdateBasket(ctx, b)
}

func (s *loggingService) UpdateBooking(ctx context.Context, id visitor.ID, books visitor.Bookings) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"level", "info",
			"method", "update booking",
			"visitor.id", id,
			"err", err,
			"latency", time.Since(begin),
		)
	}(time.Now())
	return s.next.UpdateBooking(ctx, id, books)
}

func (s *loggingService) Delete(ctx context.Context, id visitor.ID) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"level", "info",
			"method", "delete visitor",
			"visitor.id", id,
			"err", err,
			"latency", time.Since(begin),
		)
	}(time.Now())
	return s.next.Delete(ctx, id)
}

func (s *loggingService) CreateMainPayment(ctx context.Context, vid visitor.ID) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"level", "info",
			"method", "create main payment",
			"err", err,
			"latency", time.Since(begin),
			"visitor.id", vid,
		)
	}(time.Now())
	return s.next.CreateMainPayment(ctx, vid)
}

func (s *loggingService) Invoice(ctx context.Context, vid visitor.ID) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"level", "info",
			"method", "invoice",
			"err", err,
			"latency", time.Since(begin),
			"visitor.id", vid,
		)
	}(time.Now())
	return s.next.Invoice(ctx, vid)
}

func (s *loggingService) Workshops(ctx context.Context) (_ []workshop.Workshop, _ map[workshop.ID]int, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"level", "info",
			"method", "workshops",
			"err", err,
			"latency", time.Since(begin),
		)
	}(time.Now())
	return s.next.Workshops(ctx)
}

func (s *loggingService) Tickets(ctx context.Context) (_ []ticket.Ticket, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"level", "info",
			"method", "tickets",
			"err", err,
			"latency", time.Since(begin),
		)
	}(time.Now())
	return s.next.Tickets(ctx)
}
