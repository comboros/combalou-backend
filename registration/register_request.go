package registration

import (
	"fmt"

	"framagit.org/comboros/combalou/ticket"
	"framagit.org/comboros/combalou/visitor"
	"framagit.org/comboros/combalou/workshop"
)

type registerRequest struct {
	Info        personalInfo  `json:"personal-info"`
	Contact     contact       `json:"contact"`
	Purchases   []purchase    `json:"basket" validate:"empty=false"`
	Workshops   []workshop.ID `json:"workshops"`
	PaymentMode string        `json:"payment-mode" validate:"one_of=transfer,check"`
	Rate        string        `json:"price-type" validate:"one_of=full,reduced"`
}

// Basket transforms a registration’s purchases in an actual visitor.Basket,
// checking that each purchase references an existing ticket.
// TODO: the check should be in the service
func (r registerRequest) ToBasket(tickets map[ticket.ID]ticket.Ticket) (*visitor.Basket, error) {
	rate, err := ticket.StringToRate(r.Rate)
	if err != nil {
		return nil, fmt.Errorf("invalid price type: %v", err)
	}

	purchases := make(map[ticket.ID]int, len(r.Purchases))
	for _, v := range r.Purchases {
		if v.Quantity == 0 {
			continue
		}
		_, ok := tickets[v.Ticket]
		if !ok {
			return nil, fmt.Errorf("ticket %v: %w", v.Ticket, visitor.ErrNotFound)
		}
		purchases[v.Ticket] += v.Quantity
	}

	return &visitor.Basket{Purchases: purchases, Rate: rate}, nil
}

func (r registerRequest) ToVisitor(bookings visitor.Bookings) (*visitor.Visitor, error) {
	i := r.Info
	c := r.Contact
	a := c.Address
	pm, err := visitor.StringToPaymentMode(r.PaymentMode)
	if err != nil {
		return nil, err
	}
	v := visitor.Visitor{
		Surname: i.Surname,
		Name:    i.Name,
		Phone:   c.Phone,
		Email:   c.Email,
		Address: visitor.Address{
			Line1:      a.Line1,
			Line2:      a.Line2,
			PostalCode: a.PostalCode,
			City:       a.City,
			Country:    a.Country,
		},
		Bookings: bookings,
		// Deprecated fields
		Responsible: i.Responsible,
		PaymentMode: pm,
	}
	return &v, nil
}
