package registration

import (
	"context"
	"errors"
	"fmt"
	"sort"
	"strings"
	"time"

	"gopkg.in/dealancer/validate.v2"

	"framagit.org/comboros/combalou/ticket"
	"framagit.org/comboros/combalou/visitor"
	"framagit.org/comboros/combalou/workshop"
)

var (
	ErrEmptyBasket          = errors.New("basket is empty")
	ErrPaymentAlreadyExists = errors.New("payment already exists")
)

type Service interface {
	/*
		Register verifies and saves a registration.

		It splits the registration into a visitor.Visitor, that contains
		personal information (name, address…), a visitor.Basket, that contains
		all bought tickets, and a visitor.Bookings, that represents the booked
		workshops. A few number of properties are then verified, to ensure we
		don’t accept invalid registrations.

		* Basic requirements must be met: mandatory information must be provided,
		  enumerated fields must have a known value, and so on.

		* All booked workshops must exist.

		* Booked workshops must not conflict, e.g. there should not be two
		  workshops taking place at the same time.

		* Booked workshops must still have room left.

		* Basket must be consistent with the number of booked workshops (e.g.
		  the number of booked workshops must match the number of bought
		  workshops).

		If one of this check fails, an ErrValidation is returned.

		Once these verifications are made, the resulting artifacts are
		persisted and a unique identifier for the resulting visitor is
		returned.
	*/
	Register(context.Context, registerRequest) (visitor.ID, error)

	// Find returns a visitor’s registraton.
	Find(context.Context, visitor.ID) (findRegistrationResponse, error)

	// Search finds all visitors matching a term.
	// Results are returned as a slice of Registrations.
	Search(context.Context, string) ([]searchResult, error)

	// UpdateVisitor changes the data associated with a visitor. The
	// following fields will be updated:
	// - Surname
	// - Name
	// - Responsible
	// - Phone
	// - Email
	// - Address
	// - PaymentMode
	// - Rate, if the visitor has a Basket.
	//
	// It is also possible to change the State of a registration:
	// * if it goes from “unpaid” to “paid”, a new payment is added to the
	//   visitor;
	// * if it goes from “paid” to “unpaid”, the existing payment is
	//   deleted.
	// * if it is “inconsistent”, no modification is done.
	//
	// The provided visitor must have an ID. If no existing visitor matches
	// the visitor’s ID, a visitor.ErrNotFound is returned.
	UpdateVisitor(context.Context, visitor.Visitor, ticket.Rate, string) error

	// UpdateBasket changes the purchases associated with a basket.
	// Returns:
	// - visitor.ErrNotFound when the visitor does not exist.
	// - visitor.ErrNotFound when the basket does not exist, even though
	//   this would only happen if data associated with a visitor is
	//   inconsistent.
	// - ErrEmptyBasket when the new basket would be empty.
	UpdateBasket(context.Context, visitor.Basket) error

	// UpdateBooking changes what workshops a visitor has booked.
	UpdateBooking(context.Context, visitor.ID, visitor.Bookings) error

	// Delete removes a visitor, its basket, payment, and bookings.
	Delete(context.Context, visitor.ID) error

	// CreateMainPayment creates a new payment for a visitor, of the same
	// amount of its current basket, using the visitor’s payment mode.
	// A confirmation email is then sent to the visitor.
	// Returns ErrNotFound if the visitor or its basket could not be found.
	CreateMainPayment(context.Context, visitor.ID) error

	// Invoice builds and sends an invoice to a visitor.
	Invoice(context.Context, visitor.ID) error

	// Workshops returns the list of all workshops ordered by start time & name,
	// and a map of the remaining places for workshop.
	Workshops(context.Context) ([]workshop.Workshop, map[workshop.ID]int, error)

	// Tickets returns the entire ticket catalog, sorted by price.
	Tickets(context.Context) ([]ticket.Ticket, error)
}

type EmailSender interface {
	SendInvoice(visitor.Visitor, visitor.Basket, map[ticket.ID]ticket.Ticket, map[workshop.ID]workshop.Workshop) error
	SendPaymentConfirmation(visitor.Visitor) error
}

type service struct {
	visitors  visitor.Repository
	tickets   ticket.Repository
	workshops workshop.Repository
	baskets   visitor.BasketRepository
	bookings  visitor.BookingRepository
	payments  visitor.PaymentRepository
	email     EmailSender
}

func NewService(v visitor.Repository, t ticket.Repository, w workshop.Repository, bsk visitor.BasketRepository, b visitor.BookingRepository, p visitor.PaymentRepository, email EmailSender) Service {
	return &service{
		visitors:  v,
		tickets:   t,
		workshops: w,
		baskets:   bsk,
		bookings:  b,
		payments:  p,
		email:     email,
	}
}

func (s *service) Search(ctx context.Context, term string) ([]searchResult, error) {
	if term == "" {
		return nil, ErrValidation{"term is mandatory"}
	}

	all, err := s.visitors.All(ctx)
	if err != nil {
		return nil, fmt.Errorf("search: %v", err)
	}

	var matches []visitor.Visitor
	for _, v := range all {
		if v.Matches(term) {
			matches = append(matches, v)
		}
	}

	// TODO: move that outside the service
	var results []searchResult
	for _, v := range matches {
		basket, err := s.baskets.FindByVisitor(ctx, v.ID)
		if err != nil {
			return nil, fmt.Errorf("basket of %v: %v", v.ID, err)
		}
		state, err := s.computeRegistrationState(ctx, v, *basket)
		if err != nil {
			return nil, fmt.Errorf("state of %v: %v", v.ID, err)
		}
		results = append(results, newSearchResult(v, state))
	}

	return results, nil
}

func (s *service) Register(ctx context.Context, r registerRequest) (visitor.ID, error) {
	if err := validate.Validate(&r); err != nil {
		return 0, ErrValidation{err.Error()}
	}

	// Build & validate the basket.
	tickets, err := s.tickets.All(ctx)
	if err != nil {
		return 0, err
	}
	basket, err := r.ToBasket(tickets)
	if errors.Is(err, visitor.ErrNotFound) {
		return 0, ErrValidation{err.Error()}
	}
	booked, err := s.workshops.AllByID(ctx, r.Workshops)
	if err != nil {
		return 0, err
	}

	// If a booked workshop is unknown, return an error containing the
	// first unknown workshop id we could find.
	if len(booked) != len(r.Workshops) {
		for _, id := range r.Workshops {
			if _, ok := booked[id]; !ok {
				return 0, ErrValidation{fmt.Sprintf("unknown workshop %v", id)}
			}
		}
		// If all booked workshops are known, then there must be
		// doubles in r.Workshops; we don’t care, it’s handled.
	}

	// Collect the bookings.
	var bookings visitor.Bookings = make(map[workshop.ID]bool, len(r.Workshops))
	for _, id := range r.Workshops {
		bookings[id] = true
	}

	// Ensure the booked workshops don’t intersect.
	if err = bookings.Conflicts(booked); err != nil {
		return 0, err
	}

	// Ensure the selected workshops still have room.
	count, err := s.bookings.CountByWorkshop(ctx)
	if err != nil {
		return 0, fmt.Errorf("could not fetch bookings: %v", err)
	}
	for id, w := range booked {
		if w.Gauge-count[id] < 1 {
			return 0, ErrValidation{fmt.Sprintf("no room left in workshop %v", id)}
		}
	}

	// ensure basket can buy the booked workshops
	// NOTE: the frontend will eventually propose to upgrade the basket ;)
	boughtCount := basket.WorkshopsBought(tickets)
	bookedCount := bookings.WorkshopsBooked()
	if boughtCount != bookedCount {
		return 0, ErrValidation{
			fmt.Sprintf("wrong number of workshops booked: %v (%v bought)", bookedCount, boughtCount),
		}
	}

	// save visitor
	v, err := r.ToVisitor(bookings)
	if err != nil {
		return 0, err
	}
	v.Billed = time.Now() // NOTE: should invoicer set the billed date?
	if err := s.visitors.Save(ctx, v); err != nil {
		return 0, fmt.Errorf("could not save visitor: %v", err)
	}

	// save its basket
	basket.VisitorID = v.ID
	if err := s.baskets.Save(ctx, basket); err != nil {
		return 0, fmt.Errorf("save basket: %w", err)
	}

	// send invoice
	err = s.email.SendInvoice(*v, *basket, tickets, booked)
	if err != nil {
		return 0, fmt.Errorf("could not invoice visitor %v: %v", v.ID, err)
	}
	return v.ID, nil
}

func (s *service) Find(ctx context.Context, id visitor.ID) (findRegistrationResponse, error) {
	zero := findRegistrationResponse{}

	v, err := s.visitors.Find(ctx, id)
	if err != nil {
		return zero, fmt.Errorf("visitor: %w", err)
	}

	basket, err := s.baskets.FindByVisitor(ctx, id)
	if err != nil {
		return zero, fmt.Errorf("basket: %w", err)
	}

	tickets, err := s.tickets.All(ctx)
	if err != nil {
		return zero, fmt.Errorf("tickets: %w", err)
	}

	workshops, err := s.workshops.All(ctx)
	if err != nil {
		return zero, fmt.Errorf("workshops: %w", err)
	}

	state, err := s.computeRegistrationState(ctx, *v, *basket)
	if err != nil {
		return zero, err
	}

	return newFindRegistrationResponse(*v, *basket, state, tickets, workshops), nil
}

// TODO: validate input.
// TODO: remove rate (must be coordinated with a frontend change).
func (s *service) UpdateVisitor(ctx context.Context, v visitor.Visitor, rate ticket.Rate, state string) error {
	if err := s.visitors.Update(ctx, v); err != nil {
		return fmt.Errorf("update visitor: %v", err)
	}

	if err := s.updateBasketRate(ctx, v.ID, rate); err != nil {
		return err
	}
	if err := s.updateRegistrationState(ctx, v.ID, state); err != nil {
		return err
	}

	return nil
}

func (s *service) updateBasketRate(ctx context.Context, id visitor.ID, rate ticket.Rate) error {
	basket, err := s.baskets.FindByVisitor(ctx, id)
	if errors.Is(err, visitor.ErrNotFound) {
		return nil
	} else if err != nil {
		return fmt.Errorf("find basket: %v", err)
	}
	basket.Rate = rate
	if err := s.baskets.Save(ctx, basket); err != nil {
		return fmt.Errorf("save basket: %v", err)
	}
	return nil
}

func (s *service) updateRegistrationState(ctx context.Context, id visitor.ID, nextState string) error {
	if !(nextState == stateUnpaid || nextState == statePaid) {
		return ErrValidation{
			msg: "invalid state " + nextState,
		}
	}

	v, err := s.visitors.Find(ctx, id)
	if err != nil {
		return fmt.Errorf("find visitor: %w", err)
	}
	basket, err := s.baskets.FindByVisitor(ctx, id)
	if err != nil {
		return fmt.Errorf("find basket: %w", err)
	}

	previousState, err := s.computeRegistrationState(ctx, *v, *basket)
	if err != nil {
		return err
	}

	switch {
	case previousState == stateInconsistent:
		return nil

	case previousState == nextState:
		return nil // nothing to change

	case nextState == statePaid:
		tickets, err := s.tickets.All(ctx)
		if err != nil {
			return fmt.Errorf("tickets: %w", err)
		}
		pmt := visitor.Payment{
			VisitorID: v.ID,
			Mode:      v.PaymentMode,
			Amount:    basket.Price(tickets),
		}
		if err := s.payments.Save(ctx, &pmt); err != nil {
			return fmt.Errorf("save payment: %w", err)
		}

	default:
		if err := s.payments.DeleteByVisitor(ctx, v.ID); err != nil {
			return fmt.Errorf("delete payment: %w", err)
		}
	}

	return nil
}

func (s *service) UpdateBasket(ctx context.Context, updated visitor.Basket) error {
	// Filter out nul quantities.
	for tid, qt := range updated.Purchases {
		if qt == 0 {
			delete(updated.Purchases, tid)
		}
	}

	// Reject empty basket.
	if len(updated.Purchases) == 0 {
		return ErrEmptyBasket
	}

	original, err := s.baskets.FindByVisitor(ctx, updated.VisitorID)
	if errors.Is(err, visitor.ErrNotFound) {
		return fmt.Errorf("find original basket: %w", err)
	} else if err != nil {
		return fmt.Errorf("find original basket %v: %v", updated.VisitorID, err)
	}

	// Check all the demanded tickets actually exist.
	tickets, err := s.tickets.All(ctx)
	if err != nil {
		return fmt.Errorf("tickets: %w", err)
	}
	for tid := range updated.Purchases {
		if _, ok := tickets[tid]; !ok {
			return fmt.Errorf("ticket %v: %w", tid, visitor.ErrNotFound)
		}
	}

	// Remove all articles that aren’t present in the updated basket, or
	// are present but in different quantity.
	for tid, qt := range original.Purchases {
		if upqt, ok := updated.Purchases[tid]; !ok || qt != upqt {
			original.CancelPurchase(tickets[tid])
		}
	}

	// Add all articles that aren’t present in the original basket.
	// This will include articles that were present but in a different
	// quantity, since those were removed at the previous step.
	for tid, qt := range updated.Purchases {
		if _, ok := original.Purchases[tid]; !ok {
			original.AddPurchase(tickets[tid], qt)
		}
	}

	if err := s.baskets.Save(ctx, original); err != nil {
		return fmt.Errorf("save basket: %w", err)
	}

	return nil
}

func (s *service) UpdateBooking(ctx context.Context, vid visitor.ID, updated visitor.Bookings) error {
	// Check if the new booking has intersecting workshops.
	workshops, err := s.workshops.All(ctx)
	if err != nil {
		return fmt.Errorf("workshops: %w", err)
	}
	if err := updated.Conflicts(workshops); err != nil {
		return err
	}
	original, err := s.bookings.FindByVisitor(ctx, vid)
	if err != nil {
		return fmt.Errorf("find bookings for %v: %w", vid, err)
	}

	// Separate the workshops that were added from those that were removed.
	var (
		added   []workshop.ID
		removed []workshop.ID
	)
	for wid := range updated {
		if _, ok := original[wid]; !ok {
			added = append(added, wid)
		}
	}
	for wid := range original {
		if _, ok := updated[wid]; !ok {
			removed = append(removed, wid)
		}
	}

	// Persist the changes.
	for _, wid := range added {
		if err := s.bookings.Add(ctx, vid, wid); err != nil {
			return fmt.Errorf("add booking %v: %w", wid, err)
		}
	}
	for _, wid := range removed {
		if err := s.bookings.Remove(ctx, vid, wid); err != nil {
			return fmt.Errorf("remove booking %v: %w", wid, err)
		}
	}

	return nil
}

func (s *service) Delete(ctx context.Context, id visitor.ID) error {
	if err := s.deleteBasket(ctx, id); err != nil {
		return fmt.Errorf("delete basket: %w", err)
	}

	if err := s.payments.DeleteByVisitor(ctx, id); err != nil {
		return fmt.Errorf("delete payment: %w", err)
	}

	if _, err := s.visitors.Find(ctx, id); err != nil {
		return fmt.Errorf("find visitor: %w", err)
	}
	if err := s.visitors.Delete(ctx, id); err != nil {
		return fmt.Errorf("delete visitor: %w", err)
	}
	return nil
}

func (s *service) deleteBasket(ctx context.Context, id visitor.ID) error {
	bask, err := s.baskets.FindByVisitor(ctx, id)
	if errors.Is(err, visitor.ErrNotFound) {
		return nil
	}
	if err != nil {
		return fmt.Errorf("find basket: %w", err)
	}
	if err := s.baskets.Delete(ctx, bask); err != nil {
		return fmt.Errorf("delete basket: %w", err)
	}
	return nil
}

func (s *service) CreateMainPayment(ctx context.Context, vid visitor.ID) error {
	vis, err := s.visitors.Find(ctx, vid)
	if err != nil {
		return fmt.Errorf("find visitor: %w", err)
	}

	_, err = s.payments.FindByVisitor(ctx, vid)
	if err == nil {
		return ErrPaymentAlreadyExists
	} else if !errors.Is(err, visitor.ErrNotFound) {
		return fmt.Errorf("find payment: %w", err)
	}

	bask, err := s.baskets.FindByVisitor(ctx, vid)
	if err != nil {
		return fmt.Errorf("find basket: %w", err)
	}

	tickets, err := s.tickets.All(ctx)
	if err != nil {
		return fmt.Errorf("tickets: %w", err)
	}
	price := bask.Price(tickets)
	pmt := visitor.Payment{
		VisitorID: vid,
		Mode:      vis.PaymentMode,
		Amount:    price,
	}
	if err := s.payments.Save(ctx, &pmt); err != nil {
		return fmt.Errorf("save payment: %w", err)
	}

	if err := s.email.SendPaymentConfirmation(*vis); err != nil {
		return fmt.Errorf("confirm payment: %w", err)
	}

	return nil
}

func (s *service) Invoice(ctx context.Context, vid visitor.ID) error {
	vis, err := s.visitors.Find(ctx, vid)
	if err != nil {
		return fmt.Errorf("find visitor: %w", err)
	}

	basket, err := s.baskets.FindByVisitor(ctx, vid)
	if err != nil {
		return fmt.Errorf("find basket: %w", err)
	}
	tickets, err := s.tickets.All(ctx)
	if err != nil {
		return fmt.Errorf("tickets: %w", err)
	}
	workshops, err := s.workshops.All(ctx)
	if err != nil {
		return fmt.Errorf("workshops: %w", err)
	}
	if err := s.email.SendInvoice(*vis, *basket, tickets, workshops); err != nil {
		return err
	}

	return nil
}

func (s *service) Workshops(ctx context.Context) ([]workshop.Workshop, map[workshop.ID]int, error) {
	workshops, err := s.workshops.All(ctx)
	if err != nil {
		return nil, nil, fmt.Errorf("could not fetch workshops: %v", err)
	}

	// compute the remaining places
	count, err := s.bookings.CountByWorkshop(ctx)
	if err != nil {
		return nil, nil, fmt.Errorf("could not fetch bookings: %v", err)
	}
	places := make(map[workshop.ID]int, len(workshops))
	for id, w := range workshops {
		places[id] = w.Gauge - count[id]
	}

	// collect workshops into a slice
	slice := make([]workshop.Workshop, 0, len(workshops))
	for id := range workshops {
		slice = append(slice, workshops[id])
	}

	// sort it by start time, name.
	sort.Slice(slice, func(i, j int) bool {
		switch {
		case slice[i].Start.Before(slice[j].Start):
			return true
		case slice[i].Start.After(slice[j].Start):
			return false
		default:
			return strings.Compare(slice[i].Name, slice[j].Name) < 0
		}
	})

	return slice, places, nil
}

func (s *service) Tickets(ctx context.Context) ([]ticket.Ticket, error) {
	tickets, err := s.tickets.All(ctx)
	if err != nil {
		return nil, err
	}

	// collect in a slice
	slice := make([]ticket.Ticket, 0, len(tickets))
	for id := range tickets {
		slice = append(slice, tickets[id])
	}

	// sort it by price
	sort.Slice(slice, func(i, j int) bool {
		return slice[i].Prices[ticket.Full] > slice[j].Prices[ticket.Full]
	})

	return slice, nil
}

func (s *service) computeRegistrationState(ctx context.Context, v visitor.Visitor, basket visitor.Basket) (string, error) {
	tickets, err := s.tickets.All(ctx)
	if err != nil {
		return "", fmt.Errorf("tickets: %w", err)
	}

	if v.Bookings.WorkshopsBooked() != basket.WorkshopsBought(tickets) {
		return stateInconsistent, nil
	}

	_, err = s.payments.FindByVisitor(ctx, v.ID)
	if errors.Is(err, visitor.ErrNotFound) {
		return stateUnpaid, nil
	} else if err != nil {
		return "", fmt.Errorf("payment: %w", err)
	}

	return statePaid, nil
}

// TODO: ErrValidation should wrap its cause
type ErrValidation struct {
	msg string
}

func (err ErrValidation) Error() string {
	return err.msg
}
