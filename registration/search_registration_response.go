package registration

import (
	"strings"
	"time"

	"framagit.org/comboros/combalou/visitor"
	"framagit.org/comboros/combalou/workshop"
)

type searchResult struct {
	// Basic
	ID          visitor.ID `json:"id"`
	Name        string     `json:"name"`
	Surname     string     `json:"surname"`
	Responsible string     `json:"responsible"`

	// Address
	Line1      string `json:"line1"`
	Line2      string `json:"line2"`
	PostalCode string `json:"postalCode"`
	City       string `json:"city"`
	Country    string `json:"country"`
	Phone      string `json:"phone"`
	Email      string `json:"email"`

	// Money-related
	Billed      int64  `json:"lastBilling"`
	Rate        string `json:"priceType"`
	PaymentMode string `json:"paymentType"`
	State       string `json:"state"`
}

func newSearchResult(v visitor.Visitor, state string) searchResult {
	return searchResult{
		ID:          v.ID,
		Name:        v.Name,
		Surname:     v.Surname,
		Responsible: v.Responsible,
		Line1:       v.Address.Line1,
		Line2:       v.Address.Line2,
		PostalCode:  v.Address.PostalCode,
		City:        v.Address.City,
		Country:     v.Address.Country,
		Phone:       v.Phone,
		Email:       v.Email,
		Billed:      v.Billed.Unix(),
		Rate:        "full", // TODO: remove this from the DTO
		PaymentMode: v.PaymentMode.String(),
		State:       state,
	}
}

func dayFromTime(t time.Time) string {
	return strings.ToLower(t.Weekday().String())
}

func momentFromTime(t time.Time) string {
	if t.Hour() < 12 {
		return "morning"
	}
	return "afternoon"
}

func categoryToString(c workshop.Category) string {
	switch c {
	case workshop.Danse:
		return "danse"
	case workshop.Music:
		return "instrument"
	default:
		return "autre"
	}
}
