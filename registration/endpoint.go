package registration

import (
	"context"

	"framagit.org/comboros/combalou/ticket"
	"framagit.org/comboros/combalou/visitor"
	"framagit.org/comboros/combalou/workshop"
	"github.com/go-kit/kit/endpoint"
)

type searchRequest string

type searchResponse struct {
	Results []searchResult
	Err     error
}

func makeSearchEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, req interface{}) (interface{}, error) {
		term := req.(searchRequest)
		results, err := s.Search(ctx, string(term))
		return searchResponse{Results: results, Err: err}, nil
	}
}

type registerResponse struct {
	ID  visitor.ID
	Err error
}

func makeRegisterEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, req interface{}) (interface{}, error) {
		id, err := s.Register(ctx, req.(registerRequest))
		return registerResponse{ID: id, Err: err}, nil
	}
}

type findResponse struct {
	Info findRegistrationResponse
	Err  error
}

func makeFindEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, req interface{}) (interface{}, error) {
		r, err := s.Find(ctx, req.(visitor.ID))
		return findResponse{Info: r, Err: err}, nil
	}
}

type updateVisitorRequest struct {
	ID           visitor.ID `json:"id"`
	Surname      string     `json:"surname"`
	Name         string     `json:"name"`
	Responsible  string     `json:"responsible"`
	AddressLine1 string     `json:"addressLine1"`
	AddressLine2 string     `json:"addressLine2"`
	PostalCode   string     `json:"postalCode"`
	City         string     `json:"city"`
	Country      string     `json:"country"`
	Phone        string     `json:"phone"`
	Email        string     `json:"email"`
	PaymentMode  string     `json:"paymentType"` // historical name
	Rate         string     `json:"priceType"`   // historical name
	State        string     `json:"state"`       // registration state
}

type updateVisitorResponse struct {
	Err error
}

func makeUpdateVisitorEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateVisitorRequest)
		address := visitor.Address{
			Line1:      req.AddressLine1,
			Line2:      req.AddressLine2,
			PostalCode: req.PostalCode,
			City:       req.City,
			Country:    req.Country,
		}
		paymentMode, err := visitor.StringToPaymentMode(req.PaymentMode)
		if err != nil {
			return nil, err
		}
		rate, err := ticket.StringToRate(req.Rate)
		if err != nil {
			return nil, err
		}
		v := visitor.Visitor{
			ID:          req.ID,
			Surname:     req.Surname,
			Name:        req.Name,
			Responsible: req.Responsible,
			Phone:       req.Phone,
			Email:       req.Email,
			Address:     address,
			PaymentMode: paymentMode,
		}
		err = s.UpdateVisitor(ctx, v, rate, req.State)
		return updateVisitorResponse{Err: err}, nil
	}
}

type updateBasketRequest struct {
	VisitorID visitor.ID
	Purchases []struct {
		Ticket   ticket.ID `json:"ticket"`
		Quantity int       `json:"quantity"`
	}
}

type updateBasketResponse struct {
	Err error
}

func makeUpdateBasketEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateBasketRequest)
		purchases := make(map[ticket.ID]int)
		for _, p := range req.Purchases {
			purchases[p.Ticket] = p.Quantity
		}
		basket := visitor.Basket{
			VisitorID: req.VisitorID,
			Purchases: purchases,
		}
		err := s.UpdateBasket(ctx, basket)
		return updateBasketResponse{Err: err}, nil
	}
}

type updateBookingRequest struct {
	VisitorID visitor.ID
	Bookings  map[workshop.ID]bool
}

type updateBookingResponse struct {
	Err error
}

func makeUpdateBookingEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateBookingRequest)
		err := s.UpdateBooking(ctx, req.VisitorID, req.Bookings)
		return updateBookingResponse{
			Err: err,
		}, nil
	}
}

type deleteVisitorResponse struct {
	Err error
}

func makeDeleteVisitorEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		id := request.(visitor.ID)
		err := s.Delete(ctx, id)
		return deleteVisitorResponse{
			Err: err,
		}, nil
	}
}

type createMainPaymentResponse struct {
	Err error
}

func makeCreateMainPaymentEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		id := request.(visitor.ID)
		err := s.CreateMainPayment(ctx, id)
		return createMainPaymentResponse{
			Err: err,
		}, nil
	}
}

type invoiceResponse struct {
	Err error
}

func makeInvoiceEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		id := request.(visitor.ID)
		err := s.Invoice(ctx, id)
		return invoiceResponse{
			Err: err,
		}, nil
	}
}

type workshopsResponse struct {
	Workshops []workshop.Workshop
	Places    map[workshop.ID]int
}

func makeWorkshopsEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, _ interface{}) (interface{}, error) {
		w, p, err := s.Workshops(ctx)
		return workshopsResponse{Workshops: w, Places: p}, err
	}
}

type ticketsResponse struct {
	Tickets []ticket.Ticket
}

func makeTicketsEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		t, err := s.Tickets(ctx)
		return ticketsResponse{Tickets: t}, err
	}
}
