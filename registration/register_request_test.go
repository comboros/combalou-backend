package registration

import (
	"errors"
	"reflect"
	"testing"

	"framagit.org/comboros/combalou/helper"
	"framagit.org/comboros/combalou/ticket"
	"framagit.org/comboros/combalou/visitor"
	"framagit.org/comboros/combalou/workshop"
	"gopkg.in/dealancer/validate.v2"
)

func TestValidateRegisterRequest(t *testing.T) {
	cases := map[string]struct {
		input      registerRequest
		successful bool
	}{
		"surname required": {
			registerRequest{
				Info: personalInfo{
					Name: "Deconninck",
				},
				Contact: contact{
					Phone: "01 47 20 00 01",
					Email: "béber@example.com",
					Address: addressDTO{
						Line1:      "10 pl. de la Liberté",
						PostalCode: "86000",
						City:       "Poitiers",
						Country:    "France",
					},
				},
				Purchases: []purchase{
					{ticket.ID(15), 2},
					{ticket.ID(7), 1},
				},
				Workshops:   []workshop.ID{3, 2, 1},
				PaymentMode: "check",
				Rate:        "full",
			},
			false,
		},

		"name required": {
			registerRequest{
				Info: personalInfo{
					Surname: "Albert",
				},
				Contact: contact{
					Phone: "01 47 20 00 01",
					Email: "béber@example.com",
					Address: addressDTO{
						Line1:      "10 pl. de la Liberté",
						PostalCode: "86000",
						City:       "Poitiers",
						Country:    "France",
					},
				},
				Purchases: []purchase{
					{ticket.ID(15), 2},
					{ticket.ID(7), 1},
				},
				Workshops:   []workshop.ID{3, 2, 1},
				PaymentMode: "check",
				Rate:        "full",
			},
			false,
		},

		"phone required": {
			registerRequest{
				Info: personalInfo{
					Surname: "Albert",
					Name:    "Deconninck",
				},
				Contact: contact{
					Email: "béber@example.com",
					Address: addressDTO{
						Line1:      "10 pl. de la Liberté",
						PostalCode: "86000",
						City:       "Poitiers",
						Country:    "France",
					},
				},
				Purchases: []purchase{
					{ticket.ID(15), 2},
					{ticket.ID(7), 1},
				},
				Workshops:   []workshop.ID{3, 2, 1},
				PaymentMode: "check",
				Rate:        "full",
			},
			false,
		},

		"email required": {
			registerRequest{
				Info: personalInfo{
					Surname: "Albert",
					Name:    "Deconninck",
				},
				Contact: contact{
					Phone: "01 47 20 00 01",
					Address: addressDTO{
						Line1:      "10 pl. de la Liberté",
						PostalCode: "86000",
						City:       "Poitiers",
						Country:    "France",
					},
				},
				Purchases: []purchase{
					{ticket.ID(15), 2},
					{ticket.ID(7), 1},
				},
				Workshops:   []workshop.ID{3, 2, 1},
				PaymentMode: "check",
				Rate:        "full",
			},
			false,
		},

		"address line 1 required": {
			registerRequest{
				Info: personalInfo{
					Surname: "Albert",
					Name:    "Deconninck",
				},
				Contact: contact{
					Phone: "01 47 20 00 01",
					Email: "béber@example.com",
					Address: addressDTO{
						PostalCode: "86000",
						City:       "Poitiers",
						Country:    "France",
					},
				},
				Purchases: []purchase{
					{ticket.ID(15), 2},
					{ticket.ID(7), 1},
				},
				Workshops:   []workshop.ID{3, 2, 1},
				PaymentMode: "check",
				Rate:        "full",
			},
			false,
		},

		"postal code required": {
			registerRequest{
				Info: personalInfo{
					Surname: "Albert",
					Name:    "Deconninck",
				},
				Contact: contact{
					Phone: "01 47 20 00 01",
					Email: "béber@example.com",
					Address: addressDTO{
						Line1:   "10 pl. de la Liberté",
						City:    "Poitiers",
						Country: "France",
					},
				},
				Purchases: []purchase{
					{ticket.ID(15), 2},
					{ticket.ID(7), 1},
				},
				Workshops:   []workshop.ID{3, 2, 1},
				PaymentMode: "check",
				Rate:        "full",
			},
			false,
		},

		"city required": {
			registerRequest{
				Info: personalInfo{
					Surname: "Albert",
					Name:    "Deconninck",
				},
				Contact: contact{
					Phone: "01 47 20 00 01",
					Email: "béber@example.com",
					Address: addressDTO{
						Line1:      "10 pl. de la Liberté",
						PostalCode: "86000",
						Country:    "France",
					},
				},
				Purchases: []purchase{
					{ticket.ID(15), 2},
					{ticket.ID(7), 1},
				},
				Workshops:   []workshop.ID{3, 2, 1},
				PaymentMode: "check",
				Rate:        "full",
			},
			false,
		},

		"country required": {
			registerRequest{
				Info: personalInfo{
					Surname: "Albert",
					Name:    "Deconninck",
				},
				Contact: contact{
					Phone: "01 47 20 00 01",
					Email: "béber@example.com",
					Address: addressDTO{
						Line1:      "10 pl. de la Liberté",
						PostalCode: "86000",
						City:       "Poitiers",
					},
				},
				Purchases: []purchase{
					{ticket.ID(15), 2},
					{ticket.ID(7), 1},
				},
				Workshops:   []workshop.ID{3, 2, 1},
				PaymentMode: "check",
				Rate:        "full",
			},
			false,
		},

		"purchases required": {
			registerRequest{
				Info: personalInfo{
					Surname: "Albert",
					Name:    "Deconninck",
				},
				Contact: contact{
					Phone: "01 47 20 00 01",
					Email: "béber@example.com",
					Address: addressDTO{
						Line1:      "10 pl. de la Liberté",
						PostalCode: "86000",
						City:       "Poitiers",
						Country:    "France",
					},
				},
				Purchases:   []purchase{},
				Workshops:   []workshop.ID{3, 2, 1},
				PaymentMode: "check",
				Rate:        "full",
			},
			false,
		},

		"payment mode required": {
			registerRequest{
				Info: personalInfo{
					Surname: "Albert",
					Name:    "Deconninck",
				},
				Contact: contact{
					Phone: "01 47 20 00 01",
					Email: "béber@example.com",
					Address: addressDTO{
						Line1:      "10 pl. de la Liberté",
						PostalCode: "86000",
						City:       "Poitiers",
						Country:    "France",
					},
				},
				Purchases: []purchase{
					{ticket.ID(15), 2},
					{ticket.ID(7), 1},
				},
				Workshops: []workshop.ID{3, 2, 1},
				Rate:      "full",
			},
			false,
		},

		"rate required": {
			registerRequest{
				Info: personalInfo{
					Surname: "Albert",
					Name:    "Deconninck",
				},
				Contact: contact{
					Phone: "01 47 20 00 01",
					Email: "béber@example.com",
					Address: addressDTO{
						Line1:      "10 pl. de la Liberté",
						PostalCode: "86000",
						City:       "Poitiers",
						Country:    "France",
					},
				},
				Purchases: []purchase{
					{ticket.ID(15), 2},
					{ticket.ID(7), 1},
				},
				Workshops:   []workshop.ID{3, 2, 1},
				PaymentMode: "check",
			},
			false,
		},

		"all good": {
			registerRequest{
				Info: personalInfo{
					Surname: "Albert",
					Name:    "Deconninck",
				},
				Contact: contact{
					Phone: "01 47 20 00 01",
					Email: "béber@example.com",
					Address: addressDTO{
						Line1:      "10 pl. de la Liberté",
						PostalCode: "86000",
						City:       "Poitiers",
						Country:    "France",
					},
				},
				Purchases: []purchase{
					{ticket.ID(15), 2},
					{ticket.ID(7), 1},
				},
				Workshops:   []workshop.ID{3, 2, 1},
				PaymentMode: "check",
				Rate:        "full",
			},
			true,
		},
	}

	for name, tc := range cases {
		t.Run(name, func(t *testing.T) {
			err := validate.Validate(&tc.input)
			if tc.successful && err != nil {
				t.Errorf("should be successful, got %v", err)
			} else if !tc.successful && err == nil {
				t.Error("should not be successful")
			}
		})
	}
}

func TestRegisterRequestToBasket(t *testing.T) {
	const rate = ticket.Full
	tickets := helper.TicketsByID(t)
	cases := map[string]struct {
		purchases []purchase
		want      visitor.Basket
		err       error
	}{
		"all good": {
			[]purchase{
				purchase{ticket.ID(0), 2},
				purchase{ticket.ID(3), 1},
			},
			visitor.Basket{
				Rate:      rate,
				Purchases: map[ticket.ID]int{0: 2, 3: 1},
			},
			nil,
		},
		"unknown ticket": {
			[]purchase{purchase{ticket.ID(42), 1}},
			visitor.Basket{},
			visitor.ErrNotFound,
		},
		"double ticket": {
			[]purchase{
				purchase{ticket.ID(0), 2},
				purchase{ticket.ID(3), 1},
				purchase{ticket.ID(0), 1},
			},
			visitor.Basket{
				Rate:      rate,
				Purchases: map[ticket.ID]int{0: 3, 3: 1},
			},
			nil,
		},
		"zeroed quantity": {
			[]purchase{
				purchase{ticket.ID(0), 2},
				purchase{ticket.ID(3), 0},
			},
			visitor.Basket{
				Rate:      rate,
				Purchases: map[ticket.ID]int{0: 2},
			},
			nil,
		},
	}

	for name, tc := range cases {
		t.Run(name, func(t *testing.T) {
			r := registerRequest{
				Purchases: tc.purchases,
				Rate:      "full",
			}
			got, err := r.ToBasket(tickets)
			switch {
			case err != nil && tc.err == nil:
				t.Errorf("expected to success, but got error instead: %v", err)
			case err != nil && tc.err != nil && !errors.Is(err, tc.err):
				t.Errorf("got error %v that don’t match %s", err, tc.err)
			case err == nil:
				expected := tc.want.Purchases
				actual := got.Purchases
				if !reflect.DeepEqual(expected, actual) {
					t.Errorf("got %+v want %+v", *got, tc.want)
				}
			}
		})
	}
}

func TestRegisterRequestToVisitor(t *testing.T) {
	r := registerRequest{
		Info: personalInfo{
			Surname: "Roger",
			Name:    "Devlamenque",
		},
		Contact: contact{
			Phone: "08 36 65 65 65",
			Email: "devl@example.com",
			Address: addressDTO{
				Line1:      "138 rue des Chats Siamois",
				PostalCode: "59800",
				City:       "Lille",
				Country:    "France",
			},
		},
		Purchases:   []purchase{},
		Workshops:   []workshop.ID{},
		PaymentMode: "check",
		Rate:        "full",
	}
	want := visitor.Visitor{
		Surname: "Roger",
		Name:    "Devlamenque",
		Phone:   "08 36 65 65 65",
		Email:   "devl@example.com",
		Address: visitor.Address{
			Line1:      "138 rue des Chats Siamois",
			PostalCode: "59800",
			City:       "Lille",
			Country:    "France",
		},
		Bookings:    visitor.Bookings{},
		Responsible: "",
		PaymentMode: visitor.Check,
	}
	got, err := r.ToVisitor(visitor.Bookings{})
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}
	if !reflect.DeepEqual(got, &want) {
		t.Errorf("got %+v want %+v", *got, want)
	}
}
