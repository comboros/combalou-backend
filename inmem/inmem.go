package inmem

import (
	"context"
	"fmt"

	"framagit.org/comboros/combalou/ticket"
	"framagit.org/comboros/combalou/visitor"
	"framagit.org/comboros/combalou/workshop"
)

type ticketRepository struct {
	tickets map[ticket.ID]ticket.Ticket
	lastID  ticket.ID
}

func NewTicketRepository(tickets []ticket.Ticket) ticket.Repository {
	repo := ticketRepository{
		tickets: make(map[ticket.ID]ticket.Ticket),
	}
	for i := range tickets {
		repo.insert(tickets[i])
	}
	return &repo
}

func (r *ticketRepository) All(_ context.Context) (map[ticket.ID]ticket.Ticket, error) {
	return r.tickets, nil
}

func (r *ticketRepository) insert(t ticket.Ticket) ticket.ID {
	id := ticket.ID(r.lastID)
	r.lastID++
	r.tickets[id] = t
	return id
}

type workshopRepository struct {
	workshops map[workshop.ID]workshop.Workshop
	lastID    workshop.ID
}

func NewWorkshopRepository(workshops []workshop.Workshop) workshop.Repository {
	repo := workshopRepository{
		workshops: make(map[workshop.ID]workshop.Workshop),
	}
	for i := range workshops {
		repo.insert(workshops[i])
	}
	return &repo
}

func (r *workshopRepository) All(_ context.Context) (map[workshop.ID]workshop.Workshop, error) {
	return r.workshops, nil
}

func (r *workshopRepository) AllByID(_ context.Context, ids []workshop.ID) (map[workshop.ID]workshop.Workshop, error) {
	result := make(map[workshop.ID]workshop.Workshop)
	for _, id := range ids {
		if w, ok := r.workshops[id]; ok {
			result[id] = w
		}
	}
	return result, nil
}

func (r *workshopRepository) insert(w workshop.Workshop) workshop.ID {
	id := workshop.ID(r.lastID)
	r.lastID++
	r.workshops[id] = w
	return id
}

type visitorRepository struct {
	visitors map[visitor.ID]visitor.Visitor
	lastID   visitor.ID
}

func NewVisitorRepository() visitor.Repository {
	return &visitorRepository{
		visitors: make(map[visitor.ID]visitor.Visitor),
	}
}

func (r *visitorRepository) Save(_ context.Context, v *visitor.Visitor) error {
	id := r.lastID
	v.ID = id
	r.visitors[id] = *v
	r.lastID++
	return nil
}

// All returns the actual visitor store
func (r *visitorRepository) All(_ context.Context) (map[visitor.ID]visitor.Visitor, error) {
	return r.visitors, nil
}

func (r *visitorRepository) Find(_ context.Context, id visitor.ID) (*visitor.Visitor, error) {
	if v, ok := r.visitors[id]; ok {
		return &v, nil
	}
	return nil, visitor.ErrNotFound
}

func (r *visitorRepository) Update(_ context.Context, v visitor.Visitor) error {
	w, ok := r.visitors[v.ID]
	if !ok {
		return visitor.ErrNotFound
	}
	w.Surname = v.Surname
	w.Name = v.Name
	w.Phone = v.Phone
	w.Email = v.Email
	w.Responsible = v.Responsible
	w.PaymentMode = v.PaymentMode
	w.Address = v.Address
	r.visitors[v.ID] = w
	return nil
}

func (r *visitorRepository) Delete(_ context.Context, id visitor.ID) error {
	delete(r.visitors, id)
	return nil
}

type bookingRepository struct {
	visitors  visitor.Repository
	workshops workshop.Repository
}

func NewBookingRepository(v visitor.Repository, w workshop.Repository) visitor.BookingRepository {
	return &bookingRepository{v, w}
}

func (r *bookingRepository) CountByWorkshop(ctx context.Context) (map[workshop.ID]int, error) {
	count := make(map[workshop.ID]int)
	visitors, _ := r.visitors.All(ctx) // NOTE: ignoring errors for inmem store
	for _, v := range visitors {
		if len(v.Bookings) > 0 {
			for id := range v.Bookings {
				count[id] += 1
			}
		}
	}
	return count, nil
}

func (r *bookingRepository) FindByVisitor(ctx context.Context, id visitor.ID) (visitor.Bookings, error) {
	visitors, _ := r.visitors.All(ctx) // NOTE: ignoring errors for inmem store
	v, ok := visitors[id]
	if !ok {
		return nil, nil // don’t err here, it’s perfectly okay not to have booked anything
	}
	return v.Bookings, nil
}

func (r *bookingRepository) Add(ctx context.Context, visID visitor.ID, workID workshop.ID) error {
	v, err := r.visitors.Find(ctx, visID)
	if err != nil {
		return fmt.Errorf("find visitor %v: %w", visID, err)
	}
	w, err := r.workshops.AllByID(ctx, []workshop.ID{workID})
	if err != nil {
		return fmt.Errorf("find workshop %v: %w", workID, err)
	}
	if len(w) == 0 {
		return fmt.Errorf("find workshop %v: %w", workID, visitor.ErrNotFound)
	}
	v.Bookings[workID] = true
	return nil
}

func (r *bookingRepository) Remove(ctx context.Context, visID visitor.ID, workID workshop.ID) error {
	v, err := r.visitors.Find(ctx, visID)
	if err != nil {
		return fmt.Errorf("find visitor %v: %w", visID, err)
	}
	delete(v.Bookings, workID)
	return nil
}

type basketRepository struct {
	baskets  map[visitor.BasketID]visitor.Basket
	visitors visitor.Repository
	tickets  ticket.Repository

	lastID visitor.BasketID
}

func NewBasketRepository(visitors visitor.Repository, tickets ticket.Repository) visitor.BasketRepository {
	return &basketRepository{
		baskets:  make(map[visitor.BasketID]visitor.Basket),
		visitors: visitors,
		tickets:  tickets,
		lastID:   1,
	}
}

func (r *basketRepository) FindByVisitor(ctx context.Context, vid visitor.ID) (*visitor.Basket, error) {
	for _, b := range r.baskets {
		if b.VisitorID == vid {
			return &b, nil
		}
	}
	return nil, visitor.ErrNotFound
}

func (r *basketRepository) Find(ctx context.Context, bid visitor.BasketID) (visitor.Basket, error) {
	b, ok := r.baskets[bid]
	if !ok {
		// FIXME: this is ugly
		return visitor.Basket{}, fmt.Errorf("basket: %w", visitor.ErrNotFound)
	}
	return b, nil
}

func (r *basketRepository) Save(ctx context.Context, bask *visitor.Basket) error {
	if bask.ID == 0 {
		bask.ID = r.lastID
		r.lastID++
	}

	r.baskets[bask.ID] = *bask

	return nil
}

func (r *basketRepository) Delete(ctx context.Context, bask *visitor.Basket) error {
	delete(r.baskets, bask.ID)
	bask.ID = 0
	return nil
}

type paymentRepository struct {
	payments map[visitor.PaymentID]visitor.Payment
	lastID   visitor.PaymentID
}

func NewPaymentRepository() visitor.PaymentRepository {
	return &paymentRepository{
		payments: make(map[visitor.PaymentID]visitor.Payment),
		lastID:   1,
	}
}

func (r *paymentRepository) Save(_ context.Context, p *visitor.Payment) error {
	if p.ID == 0 {
		p.ID = r.lastID
		r.lastID++
	}

	r.payments[p.ID] = *p

	return nil
}

func (r *paymentRepository) DeleteByVisitor(_ context.Context, vid visitor.ID) error {
	for id, pmt := range r.payments {
		if pmt.VisitorID == vid {
			delete(r.payments, id)
			return nil
		}
	}
	return visitor.ErrNotFound
}

func (r *paymentRepository) FindByVisitor(_ context.Context, vid visitor.ID) (*visitor.Payment, error) {
	for _, p := range r.payments {
		if p.VisitorID == vid {
			return &p, nil
		}
	}
	return nil, visitor.ErrNotFound
}
