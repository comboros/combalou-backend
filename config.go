package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/pelletier/go-toml"
)

type Config struct {
	env   string
	paths struct {
		tickets   string
		workshops string
	}
	server struct {
		port string
	}
	smtp struct {
		host     string
		port     string
		from     string
		user     string
		password string
	}
	persistence struct {
		postgres bool
	}
}

func LoadConfig() *Config {
	flags := loadFlags()
	if flags.config == nil || *flags.config == "" {
		return flags.toConfig()
	}

	cfg := loadToml(*flags.config)
	cfg.merge(flags)
	return cfg
}

// Overwrites cfg with non-zero values.
func (cfg *Config) merge(values *flagConfig) {
	if *values.env != "" {
		cfg.env = *values.env
	}
	if *values.paths.tickets != "" {
		cfg.paths.tickets = *values.paths.tickets
	}
	if *values.paths.workshops != "" {
		cfg.paths.workshops = *values.paths.workshops
	}
	if *values.server.port != "" {
		cfg.server.port = *values.server.port
	}
	if *values.smtp.host != "" {
		cfg.smtp.host = *values.smtp.host
	}
	if *values.smtp.port != "" {
		cfg.smtp.port = *values.smtp.port
	}
	if *values.smtp.from != "" {
		cfg.smtp.from = *values.smtp.from
	}
	if *values.smtp.user != "" {
		cfg.smtp.user = *values.smtp.user
	}
	if *values.smtp.password != "" {
		cfg.smtp.password = *values.smtp.password
	}
	if *values.persistence.postgres {
		cfg.persistence.postgres = *values.persistence.postgres
	}
}

type flagConfig struct {
	env    *string
	config *string
	paths  struct {
		tickets   *string
		workshops *string
	}
	server struct {
		port *string
	}
	smtp struct {
		host     *string
		port     *string
		from     *string
		user     *string
		password *string
	}
	persistence struct {
		postgres *bool
	}
}

func (flags flagConfig) toConfig() *Config {
	var cfg Config

	cfg.env = *flags.env
	cfg.paths.tickets = *flags.paths.tickets
	cfg.paths.workshops = *flags.paths.workshops
	cfg.server.port = *flags.server.port
	cfg.smtp.host = *flags.smtp.host
	cfg.smtp.port = *flags.smtp.port
	cfg.smtp.from = *flags.smtp.from
	cfg.smtp.user = *flags.smtp.user
	cfg.smtp.password = *flags.smtp.password
	cfg.persistence.postgres = *flags.persistence.postgres

	return &cfg
}

func loadFlags() *flagConfig {
	var cfg flagConfig

	cfg.env = flag.String("env", "local", "environment (local,prod)")
	cfg.config = flag.String("config", "", "configuration file (TOML)")

	cfg.paths.tickets = flag.String("tickets", "data/tickets.csv", "ticket catalog (CSV file)")
	cfg.paths.workshops = flag.String("workshops", "data/workshops.csv", "workshop catalog (CSV file)")

	cfg.server.port = flag.String("port", "8080", "listening port")

	cfg.smtp.host = flag.String("smtp-host", "localhost", "host of the SMTP server")
	cfg.smtp.port = flag.String("smtp-port", "25", "port of the SMTP server")
	cfg.smtp.from = flag.String("smtp-from", "robot@localhost", "address that sends the emails")
	cfg.smtp.user = flag.String("smtp-user", "", "username used to log in the SMTP server")
	cfg.smtp.password = flag.String("smtp-password", "", "password used to log in the SMTP server")

	cfg.persistence.postgres = flag.Bool("postgres", false, "use PostgreSQL instead of the in-memory database")

	flag.Parse()

	return &cfg
}

func loadToml(path string) *Config {
	file, err := os.Open(path)
	if err != nil {
		fmt.Fprintf(os.Stderr, "read %s: %s", path, err)
		os.Exit(2)
	}

	var cfg Config
	if err := toml.NewDecoder(file).Decode(&cfg); err != nil {
		fmt.Fprintf(os.Stderr, "parse %s: %s", path, err)
		os.Exit(2)
	}
	return &cfg
}
