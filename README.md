# Combalou (backend)

Combalou is the registration manager developped for [Comboros](http://comboros.com).

The frontend is available in [the old repository](https://framagit.org/comboros/comboros-form-2018/).

## Getting started

### Requirements

- Go v1.13+
- PostgreSQL v11.5+

#### PostgreSQL configuration

Combalou uses the Postgres role `comboros`, that should be granted all rights on the `combalou` schema.

The following shell commands should do the trick:

```
$ createuser comboros
$ createdb combalou -O comboros
```

### Installing

To compile the project, simply run `go build` in the `combalou/` directory.
It will download all Go dependencies, compile and link the program, and produce an executable named `combalou`.

To run the project, simply run `./combalou`.

## Running the tests

### Unit tests

Simply run `go test ./...`, it should display something like that:

```sh
$ go test ./...
?       framagit.org/comboros/combalou  [no test files]
?       framagit.org/comboros/combalou/inmem    [no test files]
ok      framagit.org/comboros/combalou/invoice  (cached)
ok      framagit.org/comboros/combalou/postgres (cached)
ok      framagit.org/comboros/combalou/registration     (cached)
?       framagit.org/comboros/combalou/ticket   [no test files]
?       framagit.org/comboros/combalou/visitor  [no test files]
ok      framagit.org/comboros/combalou/workshop (cached)
```

### Integration tests

Shut down your own PostgreSQL instance (probably just a matter of `sudo systemctl stop postgresql`)
then start the docker stack and run the tests:

```sh
$ docker-compose up -d
$ go test -tags it framagit.org/comboros/combalou/postgres
$ docker-compose down
```

You can also run unit tests & integration tests altogether with `go test -tags it ./...` (it’s quicker to type).

### Acceptance tests

Acceptance tests ensure the backend is compatible with the [old PHP backend](https://framagit.org/comboros/comboros-form-2018/tree/master/src/php).
Incidentally, they also ensure it is compatible with the [frontend](https://framagit.org/comboros/comboros-form-2018/).
To run the acceptance tests, start the backend in a first terminal with `./combalou`,
then run the tests with the following command:

```sh
$ cd v1test
$ go test -tags acc -api rest
```

- `-tags` chooses between unit tests (the default) and acceptance tests.
- `-api` chooses between the legacy HTTP interface (URLs ending with `.php`, which is the default) and the new one (proper RESTy URLs).

### Unit test coverage

Go comes with a wonderful tooling for test coverage.
If you only want some raw statistics, you can simply add `-cover` to the test command.
For instance:

```sh
$ go test -cover ./...
?       framagit.org/comboros/combalou  [no test files]
?       framagit.org/comboros/combalou/inmem    [no test files]
ok      framagit.org/comboros/combalou/invoice  0.034s  coverage: 43.8% of statements
ok      framagit.org/comboros/combalou/postgres 0.013s  coverage: 10.3% of statements
ok      framagit.org/comboros/combalou/registration     0.034s  coverage: 38.2% of statements
?       framagit.org/comboros/combalou/ticket   [no test files]
?       framagit.org/comboros/combalou/visitor  [no test files]
ok      framagit.org/comboros/combalou/workshop 0.034s  coverage: 33.3% of statements
```

These statistics are, however, hardly useful.
To get something better, try the following:

```sh
$ export BROWSER=firefox # Ensure this environment variable is set to your browser
$ go test -coverprofile=coverage.out
$ go tool cover -html=coverage.out
```

It will open your browser to a per-file coverage analysis that will allow you to witness that business code is well-tested,
whereas repositories aren’t, as this would need an integration test.

## Deploying

TODO.

## Contributing

Merge requests for fixes and minor enhancements are welcome.
For bigger changes, please open an issue first to make your proposal.

### Style guide

We follow the standard Go recommendations:
- [Effective Go](https://golang.org/doc/effective_go.html).
- [Code Review Comments](https://github.com/golang/go/wiki/CodeReviewComments).

Also, we’re starting to follow the [Uber style guide](https://github.com/uber-go/guide/blob/master/style.md);
all new contributions should follow this guide, but please don’t migrate existing code that is out of the scope of your merge request.
If you really want to migrate code, open a specific merge request :wink:
