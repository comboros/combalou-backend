package ticket

import (
	"context"
	"fmt"
)

type Repository interface {
	All(context.Context) (map[ID]Ticket, error)
}

type Ticket struct {
	ID            ID
	Name          string
	Description   string
	Prices        map[Rate]float64
	Category      Category
	WorkshopCount int
}

type ID int

type Rate int

const (
	Full Rate = iota
	Reduced
)

func StringToRate(s string) (Rate, error) {
	switch s {
	case "full":
		return Full, nil
	case "reduced":
		return Reduced, nil
	default:
		return 0, fmt.Errorf("invalid rate %v", s)
	}
}

func (t Rate) String() string {
	switch t {
	case Reduced:
		return "reduced"
	default:
		return "full"
	}
}

type Category int

const (
	Unitary Category = iota
	Daily
	Weekly
)

func StringToCategory(s string) (Category, error) {
	switch s {
	case "unitary":
		return Unitary, nil
	case "daily":
		return Daily, nil
	case "weekly":
		return Weekly, nil
	default:
		return 0, fmt.Errorf("invalid category: %s", s)
	}
}

func (c Category) String() string {
	switch c {
	case Daily:
		return "daily"
	case Weekly:
		return "weekly"
	default:
		return "unitary"
	}
}
