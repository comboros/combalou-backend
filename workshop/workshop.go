package workshop

import (
	"context"
	"fmt"
	"time"
)

type Repository interface {
	All(context.Context) (map[ID]Workshop, error)
	AllByID(context.Context, []ID) (map[ID]Workshop, error)
}

type Workshop struct {
	ID       ID
	Name     string
	Band     string
	Start    time.Time
	Duration time.Duration
	Category Category
	Gauge    int
}

type ID int

// Conflicts returns true if two workshops, treated as time intervals, have
// a non-empty intersection. Time intervals are left-closed, right-opened, such
// that a workshop starting at the end of another won’t conflict.
func (w Workshop) Conflicts(x Workshop) bool {
	wend := w.Start.Add(w.Duration)
	xend := x.Start.Add(x.Duration)
	// the only way two workshops won’t conflict is if one is completely before the other
	before := wend.Equal(x.Start) || wend.Before(x.Start)
	after := w.Start.Equal(xend) || w.Start.After(xend)
	return !before && !after
}

type Category int

const (
	Danse Category = iota
	Music
	Craft
	Other
)

func StringToCategory(s string) (Category, error) {
	switch s {
	case "danse":
		return Danse, nil
	case "music":
		return Music, nil
	case "craft":
		return Craft, nil
	default:
		return 0, fmt.Errorf("invalid category: %s", s)
	}
}

func (c Category) String() string {
	switch c {
	case Danse:
		return "danse"
	case Music:
		return "music"
	case Craft:
		return "craft"
	default:
		return "other"
	}
}
