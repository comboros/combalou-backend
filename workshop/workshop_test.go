package workshop

import (
	"testing"
	"time"
)

func TestWorkshopConflicts(t *testing.T) {
	cases := map[string]struct {
		x      spec
		y      spec
		result bool
	}{
		"Completely disjoint": {
			spec{"01 Aug 10 10:00 UTC", "01 Aug 10 12:00 UTC"},
			spec{"01 Aug 10 15:00 UTC", "01 Aug 10 19:00 UTC"},
			false,
		},
		"Touching, but still disjoint": {
			spec{"02 Aug 10 13:00 UTC", "02 Aug 10 15:00 UTC"},
			spec{"02 Aug 10 15:00 UTC", "02 Aug 10 17:00 UTC"},
			false,
		},
		"Conflicting": {
			spec{"03 Aug 10 13:00 UTC", "03 Aug 10 17:00 UTC"},
			spec{"03 Aug 10 15:00 UTC", "03 Aug 10 19:00 UTC"},
			true,
		},
		"Same time slot": {
			spec{"04 Aug 10 13:00 UTC", "04 Aug 10 17:00 UTC"},
			spec{"04 Aug 10 13:00 UTC", "04 Aug 10 17:00 UTC"},
			true,
		},
		"First contains second": {
			spec{"05 Aug 10 08:00 UTC", "05 Aug 10 17:00 UTC"},
			spec{"05 Aug 10 10:00 UTC", "05 Aug 10 12:00 UTC"},
			true,
		},
		"Second contains first": {
			spec{"06 Aug 10 10:00 UTC", "06 Aug 10 12:00 UTC"},
			spec{"06 Aug 10 08:00 UTC", "06 Aug 10 17:00 UTC"},
			true,
		},
	}

	for name, tc := range cases {
		t.Run(name, func(t *testing.T) {
			x := tc.x.Workshop(t)
			y := tc.y.Workshop(t)
			result := x.Conflicts(y)
			if result != tc.result {
				t.Errorf("got %v, want %v", result, tc.result)
			}
		})
	}
}

type spec struct{ start, end string }

func (s *spec) Workshop(t *testing.T) Workshop {
	parse := func(str string) time.Time {
		x, err := time.Parse(time.RFC822, str)
		if err != nil {
			t.Fatalf("wrong date format in %s: %v", str, err)
		}
		return x
	}

	start := parse(s.start)
	end := parse(s.end)
	return Workshop{Start: start, Duration: end.Sub(start)}
}
