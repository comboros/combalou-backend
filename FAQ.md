Frequently Asked Questions (FAQ)
================================

How are workshops loaded?
-------------------------

Workshops are loaded at each boot from the `data/workshops.csv` file.
They aren’t overwritten, meaning any local change would be kept,
but any missing workshop is restored,
meaning any local workshop deletion would be reverted at each boot.

How are tickets loaded?
-----------------------

Tickets are loaded at each boot from the `data/tickets.csv` file.
As workshops, they aren’t overwritten but any missing ticket is re-created.

How are timezones handled?
---------------------------

Right now, the server’s timezone is the one that counts.
It means that it *must* be set in the same timezone as the festival.

The workshops are loaded from a csv file that does not include timezone information.
Hence, the server timezone is used as the default timezone.
If the server timezone is wrong, then the workshops will be loaded with erroneous start times.

How does Combalou ensures the database has the right schema?
------------------------------------------------------------

Combalou uses the table `schema_migration` to keep track of the state of the database schema:
at each boot, Combalou checks this table to devise if the database schema must be upgraded and,
if so, does the upgrade itself.

