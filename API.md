# HTTP Interface: v1

This interface is inherited from the previous backend.
We ought to keep retro-compatibility to ensure we can reuse the same frontend,
with few modifications.
All URLs have changed, though, since the first backend was written in vanilla PHP.

## Customer-facing
### Registrations
#### POST `/registrations`
- Creates a new registration.
- Sends an invoice to the customer.
- Books the selected workshops, if any.

### Tickets
#### GET `/tickets`
- Returns the ticket catalog.

### Workshops
#### GET `/workshops`
- Returns the workshop list.
- Compute, for each workshop, the number of remaining places.

## Internal use only

#### GET `/registrations?term=foo`
- Returns an excerpt of all registrations matching `term`.

#### GET `/registration/{id}`
- Returns all known information for registration matching `id`.
- If there’s a payment for the visitor, then the registration has the state *paid*.
- Otherwise the number of workshops booked and the number of workshops bought are inconsistent, the registration has the state *inconsistent*.

NOTE: This behavior is expected to change soon to something more detailed.

#### DELETE `/visitor/{id}`
- Deletes a visitor. All related information is deleted, including its basket,
  payments, and workshop bookings.

#### PUT `/visitor/{id}`
- Updates a visitor.

#### PUT `/visitor/{visitorID}/basket`
- Updates a visitor’s basket.

#### PUT `/visitor/{id}/bookings`
- Updates a registration’s booked workshops.

#### POST `/visitor/{id}/invoice`
- If the registration is *unpaid*, then:
    - send an invoice to the customer;
    - updates the registration’s “last invoicing” field to now.
- Otherwise, a `400 Bad Request` is returned.

#### POST `/visitor/{id}/payment`
- If the registration is *unpaid*, then:
    - a new payment of the same amount than the visitor’s basket is created;
    - a payment confirmation is sent to the customer.
- Otherwise, a `400 Bad Request` is returned.

