package templates

import (
	"path/filepath"
	"strconv"
	"strings"
	"text/template"
	"time"

	"framagit.org/comboros/combalou/internal/projectpath"
	"framagit.org/comboros/combalou/ticket"
	"framagit.org/comboros/combalou/visitor"
	"framagit.org/comboros/combalou/workshop"
)

type Tmpl struct {
	from                string
	confirmRegistration *template.Template
	confirmPayment      *template.Template
}

func New(from string) Tmpl {
	p := func(s string) string {
		return filepath.Join(projectpath.Root, "templates/", s)
	}
	var (
		confirmRegistration = template.Must(template.ParseFiles(
			p("confirm-registration.tmpl"),
			p("payment-check.tmpl"),
			p("payment-transfer.tmpl"),
		))
		confirmPayment = template.Must(template.ParseFiles(
			p("confirm-payment.tmpl"),
		))
	)
	return Tmpl{
		from:                from,
		confirmRegistration: confirmRegistration,
		confirmPayment:      confirmPayment,
	}
}

func (t Tmpl) ConfirmRegistration(v visitor.Visitor, basket visitor.Basket,
	tickets map[ticket.ID]ticket.Ticket, workshops map[workshop.ID]workshop.Workshop) (string, error) {
	data := confirmRegistrationData{
		Sender:   t.from,
		Visitor:  convertVisitor(v),
		Basket:   convertBasket(tickets, basket),
		Bookings: convertBookings(workshops, v.Bookings),
	}
	var b strings.Builder
	err := t.confirmRegistration.Execute(&b, &data)
	return b.String(), err
}

func (t Tmpl) ConfirmPayment(v visitor.Visitor) (string, error) {
	data := confirmPaymentData{
		Sender:  t.from,
		Visitor: convertVisitor(v),
	}
	var b strings.Builder
	err := t.confirmPayment.Execute(&b, &data)
	return b.String(), err
}

type confirmRegistrationData struct {
	Sender   string
	Visitor  visitorData
	Basket   basketData
	Bookings []workshopData
}

type confirmPaymentData struct {
	Sender  string
	Visitor visitorData
}

type visitorData struct {
	Surname, Name string
	Phone, Email  string
	Address       visitor.Address
	Adult         bool
	Responsible   string
	PaymentMode   string
}

func convertVisitor(v visitor.Visitor) visitorData {
	return visitorData{
		Surname:     v.Surname,
		Name:        v.Name,
		Phone:       v.Phone,
		Email:       v.Email,
		Address:     v.Address,
		Adult:       v.Responsible == "",
		Responsible: v.Responsible,
		PaymentMode: v.PaymentMode.String(),
	}
}

type basketData struct {
	Total     string
	Purchases []purchaseData
}

func convertBasket(tickets map[ticket.ID]ticket.Ticket, b visitor.Basket) basketData {
	var total float64
	purchases := make([]purchaseData, 0, len(b.Purchases))
	for tid, qt := range b.Purchases {
		ticket := tickets[tid]
		price := float64(qt) * ticket.Prices[b.Rate]
		total += price
		purchases = append(purchases, purchaseData{
			Quantity: qt,
			Name:     ticket.Name,
			Price:    formatPrice(price),
		})
	}
	return basketData{
		Total:     formatPrice(total),
		Purchases: purchases,
	}
}

type purchaseData struct {
	Quantity int
	Name     string
	Price    string
}

type workshopData struct {
	Slot, Name, Band string
}

func convertBookings(workshops map[workshop.ID]workshop.Workshop, b visitor.Bookings) []workshopData {
	result := make([]workshopData, 0, len(b))
	for id := range b {
		w := workshops[id]
		result = append(result, workshopData{
			Slot: formatSlot(w.Start),
			Name: w.Name,
			Band: w.Band,
		})
	}
	return result
}

func formatPrice(price float64) string {
	return strconv.FormatFloat(price, 'f', 2, 64)
}

// TODO: localize
func formatSlot(t time.Time) string {
	return _weekdays[t.Weekday()] + " " + formatHalfday(t)
}

var _weekdays = map[time.Weekday]string{
	time.Sunday:    "Dimanche",
	time.Monday:    "Lundi",
	time.Tuesday:   "Mardi",
	time.Wednesday: "Mercredi",
	time.Thursday:  "Jeudi",
	time.Friday:    "Vendred",
	time.Saturday:  "Samedi",
}

func formatHalfday(t time.Time) string {
	if t.Hour() < 12 {
		return "matin"
	}
	return "après-midi"
}
